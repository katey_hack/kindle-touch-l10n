#!/usr/bin/env python

import locale, os, re

class L10nLine(object):
    """
    Line with Kindle Touch Framework localization unit.
    
    Localization unit contains:
    * optional locale of localization unit (by default locale is en_US)
    * qualified Java class name (but without common prefix 'com.amazon')
    * line of Java .properties file for this class
    """

    # Examples of valid localization lines:
    # * @en_GB@ebook.util.resources.StringUtilResources#ellipses=\u2026
    # * ebook.util.resources.StringUtilResources#ellipses=\u2026
    #
    # Locale name could only contain alphabetic characters and signs '-', '_'.
    # Java class name could only contain alphanumeric characters and signs '_',
    # '$'. Namespaces and class name are delimited by dot.
    RE = re.compile("""^(@(?P<locale>[A-Za-z_-]+)@)?
                        (?P<class>[\w\$\.]+)\#(?P<property_line>.+)""", re.X)

    COM_AMAZON = ['com', 'amazon']
    DEFAULT_LOCALE = 'en_US'

    def locale(self, line):
        """Return locale (if it's contained in given line) or default locale."""
        match = self.RE.match(line)
        if match:
            return match.group('locale') or self.DEFAULT_LOCALE

    def qualified_class_name(self, line):
        """
        Return qualified Java class name (if it's contained in given line) 
        or None.
        """
        match = self.RE.match(line)
        if match:
            return match.group('class')

    def property_line(self, line):
        """
        Return line of Java .properties (if it's contained in given line) 
        or None.
        """
        match = self.RE.match(line)
        if match:
            return match.group('property_line')

    def packages_hier(self, line):
        """
        Return package hierarchy of Java class name (if it's contained in
        given line) or None.
        
        Package hierarchy is a part of qualified Java class name without the
        class name itself.
        """
        class_name = self.qualified_class_name(line)
        if class_name:
            return self.COM_AMAZON + class_name.split('.')[:-1]

    def class_name(self, line):
        """Return Java class name (if it's contained in given line) or None."""
        class_name = self.qualified_class_name(line)
        if class_name:
            return class_name.split('.')[-1]

    def construct(self, packages, class_name, property_line, locale=None):
        """Return line with localization unit from given parameters."""
        # Make locale part of localization unit.
        if locale is not None and locale != self.DEFAULT_LOCALE:
            locale = '@{0}@'.format(locale)
        else:
            locale = ''
        # Make list of packages and class name, forming qualified class name 
        # and then strip the 'com.amazon' prefix if it is existed.
        qualified_class = packages + [class_name]
        for pkg_name in self.COM_AMAZON:
            if qualified_class[0] == pkg_name:
                qualified_class.pop(0)
            else:
                break
        return '{0}{1}#{2}'.format(locale,
                                   '.'.join(qualified_class),
                                   property_line)

def extract_l10n(l10n_file, dest_dir):
    """
    Create Java .properties files from file with localization unit lines.
    """
    with open(l10n_file) as f:
        l10n_line = L10nLine()
        created_files = {}

        for line in f:
            # Construct path to properties file where data should be extracted
            # to.
            # 
            # Line '@de@ebook.util.StringUtilResources#<...>'
            # should be dumped to file 
            # '<dest>/com/amazon/ebook/util/StringUtilResources_de.properties'
            data_dir = os.path.join(dest_dir, *l10n_line.packages_hier(line))
            data_file_name = \
                '{0}_{1}.properties'.format(l10n_line.class_name(line),
                                            l10n_line.locale(line))
            data_file = os.path.join(data_dir, data_file_name)

            # Create all missing directories in path to data file.
            if not os.path.exists(data_dir):
                os.makedirs(data_dir)

            # Deduce whether data file should be opened for appending or
            # be (re)created.
            if data_file in created_files:
                data_file_mode = 'a+'
            else:
                data_file_mode = 'w'
                created_files[data_file] = True

            # Write data.
            with open(data_file, data_file_mode) as data:
                data.write(l10n_line.property_line(line))
                data.write('\n')
        
def construct_l10n(data_dir, target_file):
    """
    Create file with localization unit lines from directory hierarchy with
    Java .properties files.
    """
    def find_data_files(start_dir):
        """Recursively search for any file."""
        for path_to_subdir, dirs, files in os.walk(start_dir):
            for data_file_name in files: yield path_to_subdir, data_file_name

    def path_components(path):
        """Make list from file path splitting it by path separators."""
        components = []
        while True:
            path, path_tail = os.path.split(path)
            if not len(path) or not len(path_tail):
                return components
            components.insert(0, path_tail)

    with open(target_file, 'w') as f:
        l10n_line = L10nLine()

        for data_file_dir, data_file_name in find_data_files(data_dir):
            # Strip common prefix from path to data file.
            meaningful_part_of_path = data_file_dir.replace(data_dir, '', 1)
            # Make list of packages where found class is belonged to, deducing
            # it from relative path to data file directory.
            packages = path_components(meaningful_part_of_path)
            # Find locale of data file. It is defined by suffix of file name,
            # delimited by '_'. Examples: 
            # * SomeClass_de.properties (locale is 'de'),
            # * AnotherClass_en_GB.properties (locale is 'en_GB')
            file_name_components = \
                os.path.splitext(data_file_name)[0].split('_')
            l10n_locale = file_name_components.pop()
            while True:
                maybe_full_l10n_locale = \
                    "_".join([file_name_components[-1], l10n_locale])
                if maybe_full_l10n_locale.lower() in locale.locale_alias:
                    l10n_locale = maybe_full_l10n_locale
                    file_name_components.pop()
                else:
                    break
            # Make Java class name.
            class_name = '_'.join(file_name_components)

            # Append lines from data file into target file, while transforming
            # them into lines with localization units.
            data_file = os.path.join(data_file_dir, data_file_name)
            with open(data_file) as data:
                for line in data:
                    f.write(l10n_line.construct(packages,
                                                class_name,
                                                line.rstrip(),
                                                l10n_locale))
                    f.write('\n')

def _main():
    import sys
    from optparse import OptionParser
    parser = OptionParser()
    parser.set_usage("""Usage: {0} <command> <options>

Commands:
  extract   - extract strings from source file in ixtab's format into separate
              Java .properties files in target directory
  construct - construct target file in ixtab's format from separate Java
              .properties files from source directory""".format(__file__))
    parser.add_option('-f', '--file',
                      action='store', type='string', dest='filename')
    parser.add_option('-d', '--dir',
                      action='store', type='string', dest='directory')
    (options, args) = parser.parse_args(sys.argv)
    command = args[1] if len(args) > 1 else None

    if command == 'extract':
        extract_l10n(options.filename, options.directory)
    elif command == 'construct':
        construct_l10n(options.directory, options.filename)
    else:
        parser.print_help()

if '__main__' == __name__:
    _main()

