import locale, os, sys, re
from optparse import OptionParser

parser = OptionParser()
parser.add_option('-s', '--src-dir',
                  metavar='PATH',
                  dest='src_dir',
                  default='src',
                  help="path to the src dir of Git repository "
                       "('src' by default)")
parser.add_option('--fw-version',
                  metavar="VERSION_STRING",
                  dest='fw_version',
                  help='name of subdirectory of src dir to scan')
parser.add_option('--default-source-lang',
                  metavar="LOCALE_CODE",
                  dest='default_source_lang',
                  default='en',
                  help="source_lang value for files with names without locale "
                       "code suffix ('en' by default)")

if len(sys.argv) <= 1:
    parser.print_help()
    sys.exit()

(options, args) = parser.parse_args()

if options.fw_version is None:
    parser.error('firmware version must be defined')

framework_rc_dir = os.path.join(options.src_dir,
                                options.fw_version,
                                'framework')

if not os.path.exists(framework_rc_dir):
    print("Framework resources directory "
          "didn't found at path: {0}".format(framework_rc_dir))
    sys.exit()

resource_fw_version = options.fw_version.replace('.', '-')

def split_locale_code(filename):
    # We aren't interested in file extension.
    name, ext = os.path.splitext(filename)
    # File name is consisted of Java class name and optional locale code suffix,
    # delimited by underscore ('_').
    #
    # List of possible locale codes at given file name is made by splitting file
    # name by underscore and ajoining sublists back. So:
    # * 'AClassName' will be turned into ['aclassname'],
    # * 'AClassName_en'    --       into ['aclassname_en', 'en'],
    # * 'AClassName_en_US' --       into ['aclassname_en_us', 'en_us', 'us']
    #
    # Name is downcased because all locales defined by standard are contained
    # in locale.locale_alias dictionary as downcased keys.
    name_parts = name.lower().split('_')
    ajoined_name_parts = map(lambda i: '_'.join(name_parts[i:]),
                             range(len(name_parts)))
    for maybe_locale_code in ajoined_name_parts:
        if maybe_locale_code in locale.locale_alias:
            pos = len(maybe_locale_code)
            return name[:-pos-1], name[-pos:]
    return name, None

def is_source_file(filename):
    """
    Source file has no locale code suffix in it's name.
    But there is an exception: SimpleTextListFormatResources_en.properties
    """
    if 'SimpleTextListFormatResources_en.properties' == filename:
        return True
    class_name, locale_code = split_locale_code(filename)
    return locale_code is None

def extract_java_package_path_components(dirpath, filename):
    path = dirpath.replace(framework_rc_dir, '', 1)
    path_components = []
    while True:
        path, a_dir = os.path.split(path)
        if len(a_dir):
            path_components.insert(0, a_dir)
        else:
            break
    class_name = split_locale_code(filename)[0]
    path_components.append(class_name)
    return path_components

def convert_to_slug(dirpath, filename):
    path_components = extract_java_package_path_components(dirpath, filename)
    class_name_part = re.sub('Resources$', '', path_components.pop())
    return '-'.join(map(lambda v: v[0], path_components) + [class_name_part])

used_slugs = {}
for dirpath, dirnames, filenames in os.walk(framework_rc_dir):
    for fname in filenames:
        if is_source_file(fname):
            slug = convert_to_slug(dirpath, fname)
            if len(slug) >= 50:
                print("Generated slug is too long: {0}".format(slug))
                raise RuntimeError('fix my convert_to_slug to make generated '
                                   'slug of no more than 50 characters')
            if slug in used_slugs:
                print("Generated slug is repeated: {0}".format(slug))
                raise RuntimeError('fix my convert_to_slug to produce '
                                   'more unique slugs')
            used_slugs[slug] = True
            java_package_path = \
                '/'.join(extract_java_package_path_components(dirpath, fname))
            source_lang = split_locale_code(fname)[1]
            if source_lang is None:
                source_lang = options.default_source_lang
                source_file_path = java_package_path
            else:
                source_file_path = java_package_path + '_' + source_lang
            print """\
[kindle-touch-framework.{0}--{1}]
file_filter = src/{2}/framework/{3}_<lang>.properties
source_file = src/{2}/framework/{4}.properties
source_lang = {5}
type = MOZILLAPROPERTIES
""".format(resource_fw_version,
           slug,
           options.fw_version,
           java_package_path,
           source_file_path,
           source_lang)
