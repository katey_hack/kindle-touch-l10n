import base64, json, re, sys, urllib2
from getpass import getpass
from ConfigParser import ConfigParser
from optparse import OptionParser

parser = OptionParser()
parser.add_option('-f', '--orginal-version',
                  metavar='VERSION',
                  dest='original_version',
                  help='Original value of firmware version tag to update')
parser.add_option('-t', '--new-version',
                  metavar='VERSION',
                  dest='new_version',
                  help='New value of firmware version tag')
parser.add_option('-c', '--tx-config',
                  metavar='PATH',
                  dest='tx_config',
                  default='.tx/config',
                  help='path to the Transifex command-line client '
                        "configuration file ('.tx/config' by default)")
parser.add_option('-u', '--user',
                  dest='user',
                  help='username of account on Transifex server')
parser.add_option('-p', '--password',
                  dest='password',
                  help='password of account on Transifex server')
parser.add_option('--dry-run',
                  dest='dry_run',
                  action='store_true',
                  help="Skip phase of sending update data to server.")

(options, args) = parser.parse_args()

config = ConfigParser()
if not len(config.read(options.tx_config)):
    print("Configuration file hadn't found at {0}".format(options.tx_config))
    sys.exit()

if not config.has_section('main') or not config.has_option('main', 'host'):
    print("Configuration file doesn't contains Transifex server address")
    sys.exit()
host = config.get('main', 'host')
if host.endswith('/'):
    host = host[:-1]

if options.user is None:
    options.user = raw_input('Username: ')
if options.password is None:
    options.password = getpass('Password: ')
auth_string = base64.encodestring('{0}:{1}'.format(options.user,
                                                   options.password))[:-1]
auth_header = ('Authorization', 'Basic {0}'.format(auth_string))

version_tag_templates = {
    'slug': lambda v: '{0}--'.format(v.replace('.', '-')),
    'name' : lambda v: '[{0}] '.format(v)
}
version_tag = {}
for rc_attr, tag_tpl in version_tag_templates.iteritems():
    version_tag[rc_attr] = {}
    version_tag[rc_attr]['original_re'] = \
        re.compile('^{0}'.format(re.escape(tag_tpl(options.original_version))))
    version_tag[rc_attr]['new_value'] = tag_tpl(options.new_version)

def sub_version_tag(rc, rc_attr):
    return \
    version_tag[rc_attr]['original_re'].sub(version_tag[rc_attr]['new_value'],
                                            rc[rc_attr])

rc_sections = filter(lambda s: '.' in s, config.sections())
for section_name in rc_sections:
    project_slug, rc_slug = section_name.split('.')
    api_rc_endpoint = '{0}/api/2/project/{1}/resource/{2}/'.format(host,
                                                                   project_slug,
                                                                   rc_slug)
    try:
        req_get = urllib2.Request(api_rc_endpoint)
        req_get.add_header(*auth_header)
        rc = json.load(urllib2.urlopen(req_get))
        print('Resource {0}'.format(rc['slug']))
        update_dict = {}
        for rc_attr_name in version_tag.keys():
            new_rc_attr_val = sub_version_tag(rc, rc_attr_name)
            if new_rc_attr_val == rc[rc_attr_name]:
                print("  {0} ({1}) hasn't original version tag. Skip it's " \
                      "updating.".format(rc_attr_name, rc[rc_attr_name]))
            else:
                print("  {0} ({1}) will be updated.".format(rc_attr_name,
                                                            rc[rc_attr_name]))
                update_dict[rc_attr_name] = new_rc_attr_val
        if len(update_dict):
            if options.dry_run:
                print("  dry run: new value(s): {0}".format(update_dict))
            else:
                opener = urllib2.build_opener(urllib2.HTTPHandler)
                req_put = urllib2.Request(api_rc_endpoint,
                                          data=json.dumps(update_dict))
                req_put.add_header(*auth_header)
                req_put.add_header('Content-Type', 'application/json')
                req_put.get_method = lambda: 'PUT'
                opener.open(req_put)
    except urllib2.HTTPError as e:
        print("Access to Transifex server has failed: {0}".format(e))

