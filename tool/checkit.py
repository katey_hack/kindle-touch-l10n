#!/usr/bin/env python

'''
What this tool does is:
   * compares "model" language and source language and finds all properties that are identical (if "model" language property is missing or
emtpty it is considered identical to source language)
   * compares one or several other languages with source language and points out properties that are identical in model<->source but not identical in target<->source

For usage tips run "python checkit.py -h"

The tool doesn't mind your working dir, you may run it from whatever you like, just don't move it and don't rename src/ directory.

Typical way to check stuff is to run "python checkit.py -m de" assuming "de" localization is fine and then browsing the reports into report/ directory.

Remember to do fresh "tx pull -a" before running the tool so you'd get fresh results


Authored by Vasilij Litvinov a.k.a. JustAMan

'''

import os, glob, collections, sys, re
from optparse import OptionParser

def getFrameworkVersions(path):
    try:
        return os.walk(path).next()[1]
    except:
        return []

def loadProperyFile(path):
    data = []
    with open(path, 'r') as f:
        for line in f.xreadlines():
            pair = line.strip().split('=', 1)
            if len(pair) == 2:
                data.append(pair)
    return dict(data)

def getAllProperties(path):
    jsDirs = ('waf', 'pillow')
    javaDirs = ('framework', )
    javaProp = re.compile(r'(?P<name>[^_]*?)_(?P<lng>.*)\.properties')
    javaResult = collections.defaultdict(lambda: collections.defaultdict(dict))
    for part in javaDirs:
        for root, dirs, files in os.walk(os.path.join(path, part)):
            shortName = os.path.relpath(root, path)
            for f in files:
                pp = javaProp.match(f)
                try:
                    name, lng = pp.group('name'), pp.group('lng')
                    javaResult[shortName][name][lng] = loadProperyFile(os.path.join(root, f))
                except:
                    pass
    # now gather JS ones
    jsProp = re.compile(r'(?P<name>.*?)\.properties')
    jsResult = collections.defaultdict(lambda: collections.defaultdict(dict))
    for part in jsDirs:
        root = os.path.join(path, part)
        for lang in os.walk(root).next()[1]:
            if lang in ['metadata']:
                continue
            for subRoot, dirs, files in os.walk(os.path.join(root, lang)):
                shortName = re.sub(r'([/\\])(%s)([/\\])' % lang, r'\1<lang>\3', os.path.relpath(subRoot, path))
                for f in files:
                    pp = jsProp.match(f)
                    try:
                        name = pp.group('name')
                        jsResult[shortName][name][lang] = loadProperyFile(os.path.join(subRoot, f))
                    except:
                        pass
                
    return javaResult, jsResult

def getAllLangs(properties):
    result = set()
    for propFiles in properties.itervalues():
        for lngDict in propFiles.itervalues():
            result |= set(lngDict.keys())
    return result

def getEqualKeys(src, target):
    result = set()
    for k in src:
        v = target.get(k, '')
        if v == '' or v == src[k]:
            result.add(k)
    return result

def getShouldBeEqualStuff(l10n, srcLng, modLng):
    shouldBeEqual = {}
    for folder, folderValue in l10n.iteritems():
        if folderValue:
            tmp = {}
            for fileName, fileValue in folderValue.iteritems():
                if fileValue and srcLng in fileValue:
                    eq = getEqualKeys(fileValue[srcLng], fileValue.get(modLng, {}))
                    if eq:
                        tmp[fileName] = eq
            if tmp:
                shouldBeEqual[folder] = tmp
    return shouldBeEqual

def perform_report(options, l10n, js_l10n, langs, reportPath, srcPath):
    print 'Generating report...',
    
    shouldBeEqual = getShouldBeEqualStuff(l10n, options.srcLng, options.modLng)
    
    report = {}
    for lang in options.target:
        tmpLang = {}
        for folder, folderValue in shouldBeEqual.iteritems():
            tmpFolder = {}
            for fileName, equalProps in folderValue.iteritems():
                tmpEq = getEqualKeys(l10n[folder][fileName][options.srcLng], l10n[folder][fileName].get(lang, {}))
                bad = equalProps - tmpEq
                if bad:
                    tmpFolder[fileName] = bad
            if tmpFolder:
                tmpLang[folder] = tmpFolder
        if tmpLang:
            report[lang] = tmpLang
    
    print 'done'
    print 'Saving report...',
    
    for lang, data in report.iteritems():
        if not data: 
            continue
        with open(os.path.join(reportPath, '%s.html' % lang), 'w') as out:
            out.write('''<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <table border=1>
            <tr><td>Filename</td><td>Property</td><td>%s value</td><td>%s value</td></tr>''' % (lang, options.srcLng))
            for folder, folderData in data.iteritems():
                if not folderData:
                    continue
                out.write('''
            <tr><td colspan=4 align=center><b>%s</b></td></tr>''' % folder)
                for fileName, badProps in folderData.iteritems():
                    if not badProps:
                        continue
                    firstProp = badProps.pop()
                    out.write('''
            <tr><td rowspan=%d>%s_%s.properties</td><td>%s</td><td>%s</td><td>%s</td></tr>''' % \
                    (len(badProps) + 1, fileName, lang, firstProp,
                     l10n[folder][fileName][lang][firstProp],
                     l10n[folder][fileName][options.srcLng][firstProp]))
                    for item in badProps:
                        out.write('''
            <tr><td>%s</td><td>%s</td><td>%s</td></tr>''' % \
                        (item,
                         l10n[folder][fileName][lang][item],
                         l10n[folder][fileName][options.srcLng][item]))
                    # end of for item in badProps
                # end of for fileName, badProps in folderData.iteritems()
            # end of for folder, folderData in data.iteritems()
            out.write('''
        </table>
    </body>
</html>''')
    # end of for lang, data in report.iteritems()
    print 'done'
    print 'Generating index...',
    with open(os.path.join(reportPath, 'index.html'), 'w') as out:
        out.write('''<html>
    <body>
        <ul>''')
        for lang in options.target:
            data = report.get(lang, None)
            if not data:
                out.write('''
            <li><b>%s</b> fine</li>''' % lang)
                continue
            out.write('''
            <li><a href="%s">%s</a> - potential problems</li>''' % ('%s.html' % lang, lang))
        # end of for lang, data in report.iteritems()
        out.write('''
        </ul>
    </body>
</html>''')
        out.close()
    print 'done'
    return 0

def perform_blacklist(options, l10n, js_l10n, langs, reportPath, srcPath):
    print 'Generating blacklist stuff...',
    shouldBeEqual = getShouldBeEqualStuff(l10n, options.srcLng, options.modLng)
    print 'done\n'
    
    print 'Generating blacklist code...',
    with open(os.path.join(reportPath, 'blacklist-%s.txt' % options.modLng), 'w') as out:
        out.write('// this is auto-generated file to be included into .java tool for generating Kindle Touch localizations\n' + \
                  '// examine its output prior to using the results!\n\n\n')
        for folder in sorted(shouldBeEqual.keys()):
            folderName = folder.split(os.sep)
            while folderName and folderName[0] != 'com':
                del folderName[0]
            folderName = '.'.join(folderName)
            if options.fancyBlacklist:
                out.write('\n// %s\n' % folderName)
            folderData = shouldBeEqual[folder]
            for fileName in sorted(folderData.keys()):
                if options.fancyBlacklist:
                    out.write('\t// %s.properties\n' % fileName)
                for property in sorted(list(folderData[fileName])):
                    if options.fancyBlacklist:
                        out.write('\t\t// [%s] %s=%s\n' % (options.srcLng, property, l10n[folder][fileName][options.srcLng][property]))
                        out.write('\t\tset.add(new ResourceEntry("%s", "%s", "%s"));\n' % (folderName, fileName, property))
                    else:
                        out.write('// %s.%s#%s=%s\n' % (folderName, fileName, property, l10n[folder][fileName][options.srcLng][property]))
                        out.write('set.add(new ResourceEntry("%s", "%s", "%s"));\n' % (folderName, fileName, property))

    print 'done\n'

    return 0

def perform_combine(options, l10n, js_l10n, langs, reportPath, srcPath):
    print 'Generating blacklist stuff...',
    javaShouldBeEqual = getShouldBeEqualStuff(l10n, options.srcLng, options.modLng)
    print 'done\n'
    
    print 'Combining...',
    for lang in options.target:
        for data, shouldBeEqual, name, doSub in ((l10n, javaShouldBeEqual, 'java', False), (js_l10n, {}, 'js', True)):
            with open(os.path.join(reportPath, '%s-%s.lng' % (name, lang)), 'w') as out:
                out.write('''# this is auto-combined file for <%s> language

# lines with hash in the beginning are ignored - they are comments
# empty lines are ignored, too
# you are discouraged from changing line positions or anything except actual translations

# format is the following:
# F filename - filename that been combined here; 
#       may be followed by comments and zero to several property lines
# P propertyname - property name extracted from the file; 
#       MUST be followed by source value line then translation line;
#       please edit ONLY translation line; 
#       translation line might be empty, WOULD NOT be ignored, would be set to source value
# ! propertyname - currently blacklisted property;
#       follows the same rules as P property but might be ignored when compiling ending locale package


''' % lang)
                for folder in sorted(data.keys()):
                    folderData = data[folder]
                    for fileName in sorted(folderData.keys()):
                        fileData = folderData[fileName]
                        fullName = os.path.join(folder, fileName)
                        if doSub:
                            fullName = re.sub(r'([/\\])(<lang>)([/\\])', r'\1%s\3' % lang, fullName)
                        out.write('\n#' + '-' * 60 + '\nF %s\n#' % fullName + '-' * 60 + '\n')
                        try:
                            blacks = shouldBeEqual[folder][fileName]
                        except:
                            blacks = set()
                        for propertyName in sorted(fileData[options.srcLng].keys()):
                            out.write('%s %s\n' % ('!' if propertyName in blacks else 'P', propertyName))
                            for l in (options.srcLng, lang, ''):
                                out.write('%s\n' % fileData.get(l, {}).get(propertyName, ''))

    print 'done\n'
    return 0

def perform_expand(options, l10n, js_l10n, langs, reportPath, srcPath):
    print 'Reading combined files...',
    loadedJava, loadedJS = [collections.defaultdict(lambda: \
                collections.defaultdict(dict)) for i in xrange(2)]
    for lang in options.target:
        for loaded, name in ((loadedJava, 'java'), (loadedJS, 'js')):
            with open(os.path.join(reportPath, '%s-%s.lng' % (name, lang)), 'r') as inpF:
                inp = [x.replace('\n', '').replace('\r', '') for x in inpF.xreadlines()]
                pos, max = 0, len(inp)
                while pos < max:
                    line = inp[pos]
                    pos += 1
                    if not line or line[0] == '#':
                        continue
                    if line[0] == 'F':
                        fileName = line[2:]
                    elif line[0] in ['P', '!']:
                        propertyName = line[2:]
                        srcLine, transLine = inp[pos:pos+2]
                        pos += 2
                        loaded[fileName][lang][propertyName] = transLine
    print 'done\n'
    
    print 'Writing loaded stuff...',
    for lang in options.target:
        for loaded, nameTmpl, loc, doSub in ((loadedJava, '%%s_%s.properties' % lang, l10n, False), (loadedJS, '%s.properties', js_l10n, True)):
            for fileName, fileData in loaded.iteritems():
                folder, baseName = os.path.split(fileName)
                if doSub:
                    folder = re.sub(r'([/\\])(%s)([/\\])' % lang, r'\1<lang>\3', folder)
                srcData = loc[folder][baseName][options.srcLng]
                with open(nameTmpl % os.path.join(srcPath, fileName), 'w') as out:
                    for propertyName in sorted(srcData.keys()):
                        out.write('%s=%s\n' % (propertyName, fileData.get(lang, {}).get(propertyName, '')))
                    out.write('\n')
    print 'done\n'
    return 0

def noActionFound(options, *args, **kw):
    print "No action <%s> found. That's probably a bug in the tool, contact its author!" % options.action
    return 1

def main():
    rootPath = os.path.join(os.path.split(__file__)[0], '..')
    srcPath = os.path.join(rootPath, 'src')
    allFrameworkVersions = getFrameworkVersions(srcPath)

    parser = OptionParser()
    parser.add_option('-f', '--framework-version', dest='frameworkVersion', action='store', help='framework version to check', metavar='VERSION', default='5.0.3')
    parser.add_option('-s', '--source-lng', dest='srcLng', action='store', help='source language', metavar='LANG', default='en_US')
    parser.add_option('-m', '--model-lng', dest='modLng', action='store', help='model language (the one which translation is verified to be working)', metavar='LANG', default='de')
    parser.add_option('-t', '--target-lng', dest='target', action='append', help='language to check (you may specify several languages or omit the option to check all of them)', metavar='LANG', default=[])
    parser.add_option('-o', '--output-dir', dest='output', action='store', help='directory where to put reports', metavar='PATH', default='report')
    parser.add_option('-a', '--action', dest='action', action='store', help='action to execute', type='choice', choices=['report', 'blacklist', 'combine', 'expand'], metavar='ACTION', default='report')
    parser.add_option('--nfb', '--no-fancy-blacklist', dest='fancyBlacklist', action='store_false', help='turn off fancy comments and tabs in blacklist mode', default=True)
    options, args = parser.parse_args()
    
    if options.frameworkVersion not in allFrameworkVersions:
        parser.error('Wrong framework version <%s>. Available are: %s' % (options.frameworkVersion, ', '.join(allFrameworkVersions)))
    srcPath = os.path.join(srcPath, options.frameworkVersion)

    reportPath = os.path.join(rootPath, options.output, options.frameworkVersion)
    if not os.path.exists(reportPath):
        try:
            os.makedirs(reportPath)
        except Exception, e:
            print 'Cannot create output directory "%s". %s' % (reportPath, str(e))
            return 1
    elif not os.path.isdir(reportPath):
        print 'Specified output "%s" is not a directory. Quitting' % reportPath
        return 1
    
    print 'Reading l10n, please wait...',
    l10n, js_l10n = getAllProperties(srcPath)
    langs = getAllLangs(l10n) | getAllLangs(js_l10n)
    langsStr = ', '.join(sorted(list(langs)))
    langNotKnown = '%s language <%s> not known. Known languages are: %s'
    print 'done\n'
    
    if options.srcLng not in langs:
        parser.error(langNotKnown % ('Source', options.srcLng, langsStr))
    if options.modLng not in langs:
        parser.error(langNotKnown % ('Model', options.modLng, langsStr))
    
    if options.target:
        for lang in options.target:
            if lang not in langs:
                parser.error(langNotKnown % ('Target', lang, langsStr))
    else:
        options.target = langs - set([options.srcLng, options.modLng])
    
    print ('Parameters:\n    * ' + ': %s\n    * '.join([x.ljust(20) for x in ['framework version', 
                                                                              'source language', 
                                                                              'model language', 
                                                                              'target language' + ('s' if len(options.target) > 1 else ''), 
                                                                              'output directory',
                                                                              'action']]) + ': %s\n') % \
        (options.frameworkVersion, options.srcLng, options.modLng, ', '.join(sorted(options.target)), reportPath, options.action)

    method = globals().get('perform_' + options.action, noActionFound)
    return method(options, l10n, js_l10n, langs, reportPath, srcPath)

if __name__ == '__main__':
    sys.exit(main())
