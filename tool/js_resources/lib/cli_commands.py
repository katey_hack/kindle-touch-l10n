import codecs, errno, json, locale, os, re, sys

from .resource import (ConstructorCall, ResourcesStoreBuilder)
from .format.properties import PropertiesFormat
from .format.javascript import extract_resources

from vendor import deep, jsparser

class ResourcesManifest(object):
    def __init__(self, path):
        self.path = path
        self.content = None

    def read(self):
        if self.content is None:
            with open(self.path) as manifest:
                self.content = manifest.read().splitlines()
        return self.content

    def __iter__(self):
        self.read()
        for line in self.content:
            if len(line.strip()):
                yield line

class ExtractCommand(object):
    class Dest(object):
        def __init__(self, dest_dir, locale):
            self.dest_dir = dest_dir
            self.locale = locale

        def dir(self, rel_path):
            return os.path.join(self.dest_dir, self.locale,
                                os.path.dirname(rel_path))

        def filename(self, rel_path):
            name = os.path.splitext(os.path.basename(rel_path))[0]
            return '{0}.properties'.format(name)

        def file(self, rel_path):
            return os.path.join(self.dir(rel_path), self.filename(rel_path))

        def metadata_filename(self, rel_path):
            name = os.path.splitext(os.path.basename(rel_path))[0]
            return '{0}.metadata.json'.format(name)

        def metadata_file(self, rel_path):
            return os.path.join(self.dir(rel_path),
                                self.metadata_filename(rel_path))

    def _utf8_open(self, path, mode):
        return codecs.open(path, mode, encoding='utf8')

    def execute(self, args):
        if not os.path.exists(args.dest_dir):
            print("Destination directory doesn't exists")
            sys.exit(1)
        if args.manifest in ('pillow', 'waf'):
            args.manifest = os.path.join(os.path.dirname(__file__),
                                         '..', 'metadata',
                                         '{0}.manifest'.format(args.manifest))
        manifest = ResourcesManifest(args.manifest)
        dest = self.Dest(args.dest_dir, args.locale)
        for relative_path in manifest:
            sys.stdout.write('{0}... '.format(relative_path))
            js_file = os.path.join(args.source_dir, relative_path)
            if not os.path.exists(js_file):
                print("doesn't exists.")
                continue
            parse_result = jsparser.parse(file(js_file).read(), js_file)
            resources = extract_resources(parse_result)
            try:
                os.makedirs(dest.dir(relative_path))
            except OSError as e:
                if errno.EEXIST == e.errno:
                    pass
                else: raise
            properties_format = PropertiesFormat(resources)
            with self._utf8_open(dest.file(relative_path), 'w') as properties:
                properties.write(
                    '\n'.join(map(lambda v: "{0}={1}".format(*map(str, v)), 
                                  properties_format.output())))
            self._store_metadata(properties_format.metadata(),
                                 dest.metadata_file(relative_path))
            print('extracted.')

    def _store_metadata(self, metadata, metadata_file_path):
        existing_metadata = None
        if os.path.exists(metadata_file_path):
            with self._utf8_open(metadata_file_path, 'r') as metadata_file:
                try:
                    existing_metadata = json.load(metadata_file)
                except ValueError:
                    pass
        if deep.diff(metadata, existing_metadata):
            with self._utf8_open(metadata_file_path, 'w') as metadata_file:
                json.dump(metadata, metadata_file, indent=2)

class ResourcesJsonEncoder(json.JSONEncoder):
    _CONSTRUCTOR_CALL_META = {
      'prefix': '__constructor_call__',
      'placeholder_format': '{0}--({1})',
      're': '(?P<ctor_name>[\w_]+)--\((?P<ctor_arg>.+)\)',
    }
    _CONSTRUCTOR_CALL_PLACEHOLDER = \
        u'{c[prefix]}{c[placeholder_format]}'.format(c=_CONSTRUCTOR_CALL_META)
    _CONSTRUCTOR_CALL_RE = \
        re.compile('^"{c[prefix]}{c[re]}"$'.format(c=_CONSTRUCTOR_CALL_META))

    def default(self, o):
        if isinstance(o, ConstructorCall):
            return self._CONSTRUCTOR_CALL_PLACEHOLDER.format(
                o.constructor_name.decode('utf8'),
                o.arg.decode('utf8'))
        return super(self.__class__, self).default(o)

    def iterencode(self, o):
        for chunk in super(self.__class__, self).iterencode(o):
            matched = self._CONSTRUCTOR_CALL_RE.match(chunk)
            if matched:
                yield u'new {0}("{1}")'.format(*matched.groups())
            else:
                yield chunk

class CompileCommand(object):
    def execute(self, args):
        if not os.path.exists(args.dest_dir):
            print("Destination directory doesn't exists")
            sys.exit(1)
        if '*' == args.locale or \
                (isinstance(args.locale, list) and '*' in args.locale):
            args.locale = os.listdir(args.source_dir)
            try:
                args.locale.remove(args.source_locale)
            except ValueError:
                pass
            args.locale = filter(lambda l: l.lower() in locale.locale_alias,
                                 args.locale)
        source_locale_dir = os.path.join(args.source_dir, args.source_locale)
        for dirpath, dirnames, filenames in os.walk(source_locale_dir):
            for fname in filenames:
                if not fname.endswith('.properties'):
                    continue
                base_file = os.path.join(dirpath, fname)
                relative_path = dirpath.replace(source_locale_dir, '', 1)
                if relative_path.startswith(os.sep):
                    relative_path = relative_path[1:]
                elif os.path.altsep is not None \
                        and relative_path.startswith(os.altsep):
                    relative_path = relative_path[1:]
                for a_locale in args.locale:
                    dest_locale = a_locale.lower().replace('_', '-')
                    compiled_file = os.path.join(args.source_dir,
                                                 a_locale,
                                                 relative_path,
                                                 fname)
                    metadata_fname = \
                        os.path.splitext(fname)[0] + '.metadata.json'
                    metadata_file = os.path.join(args.source_dir,
                                                 'metadata',
                                                 relative_path,
                                                 metadata_fname)
                    properties = PropertiesFormat(ResourcesStoreBuilder())
                    resources = properties.read(compiled_file,
                                                metadata_file,
                                                base=base_file)
                    dest_dirpath = os.path.join(args.dest_dir,
                                                'locales',
                                                dest_locale,
                                                relative_path)
                    try:
                        os.makedirs(dest_dirpath)
                    except OSError as e:
                        if errno.EEXIST == e.errno:
                            pass
                        else: raise
                    dest_fname = os.path.splitext(fname)[0] + '.js'
                    dest_file = os.path.join(dest_dirpath, dest_fname)
                    with codecs.open(dest_file, 'w', encoding='utf8') as js:
                        var = resources.resources_variable
                        js.write('{0}{1} = '.format(
                            'var ' if var.is_local else '',
                            var.identifier))
                        json.dump(resources.store, js, indent=2,
                                  ensure_ascii=False, encoding="utf8",
                                  cls=ResourcesJsonEncoder)
                        js.write(';\n')
