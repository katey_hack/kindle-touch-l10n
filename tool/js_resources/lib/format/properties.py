import codecs, json, os, re
from contextlib import contextmanager

try:
    from collections import OrderedDict
except ImportError:
    from vendor.backport.OrderedDict import OrderedDict

from ..resource import (ConstructorCall,
                        ArrayLiteralContext,
                        ObjectLiteralContext,
                        Resource,
                        ResourcesVariable)

class PropertiesFormat(object):
    """Converter of resources' store to .properties format.
    
    Key of resource is made by joining id of resource with dot.
    """

    class PathStore(object):
        def __init__(self, path=None):
            self.path = [] if path is None else path

        @contextmanager
        def add(self, context, path_component):
            self.path.append((context, path_component))
            yield self.__class__(self.path[:])
            self.path.pop()

        def __str__(self):
            compiled_path = []
            for context, path_component in self.path:
                if isinstance(context, dict):
                    compiled_path.append(path_component)
                elif isinstance(context, list):
                    prev = compiled_path.pop()
                    compiled_path.append("{0}[{1}]".format(prev,
                                                           path_component))
                else: # default variant
                    compiled_path.append(path_component)
            return '.'.join(compiled_path)

    class PathIterator(object):
        def __init__(self, path):
            self.path = path

        def __iter__(self):
            path_components = self.path.split('.')
            path_endpoint = path_components.pop()
            for component in path_components:
                for key, inner_ctx in self._iter_path_component(component):
                    yield key, inner_ctx
            for key, ctx in self._iter_path_component(path_endpoint, True):
                yield key, ctx

        def _iter_path_component(self, path_component, path_endpoint=False):
            inner_ctx = ObjectLiteralContext() if not path_endpoint else None
            matched = re.match('^(?P<key>.+)\[(?P<idx>\d+)]$', path_component)
            if matched:
                yield matched.group('key'), ArrayLiteralContext()
                yield matched.group('idx'), inner_ctx
            else:
                yield path_component, inner_ctx

    def __init__(self, resources_store_builder):
        self.resources = resources_store_builder

    def read(self, properties_file, metadata_file, base=None):
        with codecs.open(metadata_file, encoding='utf8') as md:
            metadata = json.load(md)
        self.resources.resources_variable = \
            ResourcesVariable(metadata['variable']['identifier'],
                              metadata['variable']['local'])
        props = OrderedDict()
        line_re = re.compile('^(?P<key>[^=]+)=(?P<val>.+)$')
        if base is not None:
            with codecs.open(base, encoding='utf8') as base_file:
                for line in base_file.readlines():
                    matched = line_re.match(line.strip())
                    if matched:
                        key, val = matched.groups()
                        props[key] = val
        with codecs.open(properties_file, encoding='utf8') as properties:
            for line in properties.readlines():
                matched = line_re.match(line.strip())
                if matched:
                    key, val = matched.groups()
                    if base is None or (key in props and len(val)):
                        props[key] = val
        for path, val in props.iteritems():
            for key, inner_ctx in self.PathIterator(path):
                if inner_ctx is not None:
                    context = Resource(inner_ctx, key=key)
                    self.resources.enter_into_new_context(context)
                else:
                    try:
                        ctor_name = metadata['properties'][path]['constructor']
                        ctor_call = ConstructorCall(ctor_name, val)
                        resource = Resource(ctor_call, key=key)
                    except KeyError:
                        resource = Resource(val, key=key)
                    self.resources.add_resource(resource)
            self.resources.reset_current_context()
        return self.resources

    def metadata(self):
        metadata = {
            'variable': {
                'identifier': self.resources.resources_variable.identifier,
                'local': self.resources.resources_variable.is_local
            }
        }

        properties = {}
        for path, ctx in self.resources.store.iter_context(self.PathStore()):
            if isinstance(ctx, ConstructorCall):
                properties[str(path)] = {'constructor': ctx.constructor_name}
        if len(properties.keys()):
            metadata['properties'] = properties

        return metadata

    def output(self):
        return list(self.resources.store.iter_context(self.PathStore()))
