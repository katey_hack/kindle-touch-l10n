from ..resource import (ConstructorCall,
                        ArrayLiteralContext,
                        ObjectLiteralContext,
                        Resource,
                        ResourcesStoreBuilder,
                        ResourcesVariable)

def build_resources_store(resources_obj_node, store_builder):
    """Build resources store with help of ResourcesStoreBuilder.

    :param resources_obj_node: OBJECT_INIT node of parsed JavaScript object
                               literal with resources.
    :type resources_obj_node: jsparser.Node
    :param store_builder: Builder of resources store.
    :type store_builder: ResourcesStoreBuilder
    """
    for property_init_node in resources_obj_node:
        # Name of JavaScript object property.
        name = property_init_node[0].value
        # jsparser.Node instance with value of object property.
        value_node = property_init_node[1]
        if 'STRING' == value_node.type:
            # Property contains plain string, just add it to resources store.
            store_builder.add_resource(Resource(value_node.value, key=name))
        elif 'NEW_WITH_ARGS' == value_node.type:
            # Property contains JavaScript constructor call, add it to
            # resources store with proper metadata.
            #
            # This is ad-hoc hack for dealing with translatable string as 
            # single argument of constructor call.
            ctor_name = value_node[0].value
            ctor_arg = value_node[1][0].value
            ctor_call = ConstructorCall(ctor_name, ctor_arg)
            store_builder.add_resource(Resource(ctor_call, key=name))
        elif 'OBJECT_INIT' == value_node.type:
            # Property contains another JavaScript object. Descend into it.
            obj_context = Resource(ObjectLiteralContext(), key=name)
            with store_builder.descend(obj_context) as obj_builder:
                build_resources_store(value_node, obj_builder)
        elif 'ARRAY_INIT' == value_node.type:
            # Property contains JavaScript array. Descend into it, iterate over
            # it's contents with descending into context of it's memebers.
            #
            # This is ad-hoc hack for dealing with array whose memebers are
            # JavaScript objects with translatable strings.
            ary_context = Resource(ArrayLiteralContext(), key=name)
            with store_builder.descend(ary_context) as ary_builder:
                for element in value_node:
                    obj_context = Resource(ObjectLiteralContext())
                    with ary_builder.descend(obj_context) as obj_builder:
                        build_resources_store(element, obj_builder)

def find_resources(script_node):
    if len(script_node.varDecls):
        var_node = script_node.varDecls[0]
        resources_node = var_node.initializer
        resources_variable = ResourcesVariable(var_node.value)
    else:
        # Ad-hoc hack for browser resources where top-level assignment is made
        # without 'var' keyword.
        var_node = script_node[0].expression
        resources_node = var_node[1]

        var_identifier_node = var_node[0]
        if 'IDENTIFIER' == var_identifier_node.type:
            var_identifier = var_identifier_node.value
        elif 'DOT' == var_identifier_node.type:
            var_identifier = '.'.join(map(lambda v: v.value, 
                                          var_identifier_node))

        resources_variable = ResourcesVariable(var_identifier, is_local=False)

    return resources_variable, resources_node

def extract_resources(script_node, resources_store=None):
    resources_variable, resources_node = find_resources(script_node)
    resources_store_builder = ResourcesStoreBuilder(resources_variable)

    if 'GROUP' == resources_node.type:
        # Ad-hoc hack for adviewer resources where assigned object literal is
        # wrapped into function call.
        #
        # Recurse into 'SCRIPT' node representing code inside called function.
        function_body = resources_node[0][0].body
        wrapped_variable, resources_node = find_resources(function_body)
        obj_context = Resource(ObjectLiteralContext(), 
                               wrapped_variable.identifier)
        resources_store_builder.enter_into_new_context(obj_context)

    if 'OBJECT_INIT' == resources_node.type:
        build_resources_store(resources_node, resources_store_builder)

    return resources_store_builder
