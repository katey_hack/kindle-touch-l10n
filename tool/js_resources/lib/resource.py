from contextlib import contextmanager

try:
    from collections import OrderedDict
except ImportError:
    from vendor.backport.OrderedDict import OrderedDict

from .html_sanitizer import sanitize_html

class ResourcesVariable(object):
    """Resources' variable.
    
    Resources are stored in JavaScript object that is assigned to variable.
    Name of this variable and other attributes are stored here.
    """

    def __init__(self, identifier, is_local=True):
        #: Identifier of variable (i.e. it's name)
        self.identifier = identifier
        #: Attribute of variable localness. If variable has been defined with
        #: 'var' keyword, it is local. Otherwise, it's not local.
        self.is_local = is_local

    def __str__(self):
        return '{0}{1}'.format('var ' if self.is_local else '', self.identifier)

class ConstructorCall(object):
    """Metadata for string wrapped in constructor call.
    
    Constructor call in JavaScript is represented with statement 'new Ctor()'.
    """

    def __init__(self, constructor_name, constructor_arg):
        #: Name of called constructor.
        self.constructor_name = constructor_name
        #: String argument of called constructor.
        self.arg = sanitize_html(constructor_arg)

    def __str__(self):
        return self.arg

    def __repr__(self):
        return 'new {0}("{1}")'.format(self.constructor_name, self.arg)

class Resource(object):
    """Resource is a key/value pair of JavaScript object literal member or
    a value of JavaScript array literal element.
    """

    def __init__(self, value, key=None):
        #: Value of resource.
        self.value = sanitize_html(value)
        #: Key of resource (if resource is a member of object literal, otherwise
        #: it's None).
        self.key = key

    def __repr__(self):
        if self.key is None:
            return "Resource({0})".format(self.value)
        else:
            return "Resource({0}: {1})".format(self.key, self.value)

class ContextIterator(object):
    """Iteration over values of resources' context with building of path to
    value.

    Resource context is a JavaScript collection contained resource: object
    literal, array literal etc.

    Path to value is a list of tuples respresenting contexts contained resource.
    Tuple contain context itself and id of value in context (property name
    for object literal context, index for array literal context, ...)
    """

    def __init__(self, context, path_store):
        #: Context for iteration over it's values. It should implement method
        #: iter_context_values(path_store).
        self.context = context
        #: Object for storing path to context value.
        self.path_store = path_store

    def __iter__(self):
        for path, val in self.context.iter_context_values(self.path_store):
            # If value is a another context, it is support iteration over it's
            # values. So check for it and proceed to iteration if it's true.
            if hasattr(val, 'iter_context'):
                for subpath, subval in val.iter_context(path):
                    yield subpath, subval
            else:
                yield path, val

class ObjectLiteralContext(OrderedDict):
    """Representation of object literal from JavaScript resources code.
    
    From Python side it's an ordinary dictionary, but with some methods helping
    for it's filling out of parsed JavaScript resources code.
    """

    def add_resource(self, resource):
        """Add resource contained in JavaScript object.
        
        :type resource: Resource
        :raise RuntimeError: when key of resource is None
        """
        if resource.key is None:
            raise RuntimeError('Adding of {0} with undefined key into object '\
                               'context'.format(repr(resource)))
        self[resource.key] = resource.value

    def enter_into(self, new_context):
        """Enter into context of added resource.

        :type new_context: Resource
        :return: value of added resource
        """
        if new_context.key in self:
            return self[new_context.key]
        self.add_resource(new_context)
        return new_context.value

    def iter_context(self, path_store):
        """Iteration over context's values.
        
        :param path_store: Object for storing of path to context value. Must
                           implement method add(context, path_component).
        """
        return ContextIterator(self, path_store)

    def iter_context_values(self, path_store):
        """Implementation of real iteration over context's values.

        :param path_store: Object for storing of path to context value. Must
                           implement method add(context, path_component).
        """
        for key, val in self.iteritems():
            with path_store.add(self, key) as descended_path_store:
                yield descended_path_store, val

class ArrayLiteralContext(list):
    """Representation of array literal from JavaScript resources code.
    
    From Python side it's an ordinary list, but with some methods helping for
    it's filling out of parsed JavaScript resources code.
    """
    def add_resource(self, resource):
        """Add resource contained in JavaScript object.
        
        :type resource: Resource
        """
        try: 
            idx = int(resource.key)
        except (TypeError, ValueError):
            idx = None
        if idx is None or idx == len(self):
            self.append(resource.value)
        else:
            self[idx] = resource.value

    def enter_into(self, new_context):
        """Enter into context of added resource.

        :type new_context: Resource
        :return: value of added resource
        """
        try:
            idx = int(new_context.key)
            return self[idx]
        except (TypeError, ValueError, IndexError):
            pass
        self.add_resource(new_context)
        return new_context.value

    def iter_context(self, path_store):
        """Iteration over context's values.
        
        :param path_store: Object for storing of path to context value. Must
                           implement method add(context, path_component).
        """
        return ContextIterator(self, path_store)

    def iter_context_values(self, path_store):
        """Implementation of real iteration over context's values.

        :param path_store: Object for storing of path to context value. Must
                           implement method add(context, path_component).
        """
        for idx, val in enumerate(self):
            with path_store.add(self, idx) as descended_path_store:
                yield descended_path_store, val

class ResourcesStoreBuilder(object):
    """Builder of resources store.

    Resources store is an ObjectLiteralContext instance, representing object
    literal assigned to resources variable.
    """

    def __init__(self, resources_variable=None):
        #: ResourcesVariable instance.
        self.resources_variable = resources_variable
        #: Resources store to build up.
        self.store = ObjectLiteralContext()
        self.reset_current_context()

    def add_resource(self, resource):
        """Add resource to current context.
        
        :type resource: Resource
        """
        self._current_context.add_resource(resource)

    def enter_into_new_context(self, new_context):
        """Enter into context of added resource.

        :type new_context: Resource
        """
        new_context = self._current_context.enter_into(new_context)
        self._context_history.append(self._current_context)
        self._current_context = new_context

    def exit_from_current_context(self):
        """Exit from current context to outer context.

        :return: current context or None if current context is initial
                 ObjectLiteralContext.
        """
        if not len(self._context_history):
            return None
        exited_context = self._current_context
        self._current_context = self._context_history.pop()
        return exited_context

    def reset_current_context(self):
        self._current_context = self.store
        self._context_history = []

    @contextmanager
    def descend(self, new_context):
        """Descend into context of added resource. 
        
        :type new_context: Resource
        """
        self.enter_into_new_context(new_context)
        yield self
        self.exit_from_current_context()
