import collections, re, xml

from vendor import html5lib

from vendor.html5lib.sanitizer import HTMLSanitizer as Html5libSanitizer
from vendor.html5lib.filters._base import Filter as Html5libBaseFilter

class RequestedEntitiesLoggingWrapper(collections.MutableMapping):
    """Dictionary with remembering of key of last requested value.

    Entities dictionary is a mapping {entity_name: unicode_character}.

    Instance of this class is intended to save entity_name of last
    unicode_character requested from wrapped entities mapping.
    """

    def __init__(self, entities):
        #: Entities dictionary.
        self._entities = entities
        # Key of last requested value (or None if no value was requested yet or
        # remembered key was already returned to interested consumer).
        self._remembered_key = None

    def pop_remembered_key(self):
        """Return remembered key of last requested value and then forget this
         key.

        If no value was requested yet since creating of instance (or key was
        forgot earlier and no value was requested yet), None is returned.
        """
        popped_key = self._remembered_key
        self._remembered_key = None
        return popped_key

    def __getitem__(self, key):
        self._remembered_key = key
        return self._entities.__getitem__(key)

    def __setitem__(self, key, value):
        self._entities.__setitem__(key, value)

    def __delitem__(self, key):
        self._entites.__delitem__(key)

    def __len__(self):
        return self._entities.__len__()

    def __contains__(self, item):
        return self._entities.__contains__(item)

    def __iter__(self):
        return self._entities.__iter__(self)

# Dirty hack. Look at HtmlSanitizer.consumeEntity comments.
_logged_entities = RequestedEntitiesLoggingWrapper(html5lib.tokenizer.entities)
html5lib.tokenizer.entities = _logged_entities

class EntityTokenData(unicode):
    """Store of entity token data (unicode character) with additional saving of
    entity name.
    
    It is just unicode string with additional attribute.
    """

    # Arbitary unique name of attribuite.
    __slots__ = ('_x_ktl10n_entity_name')

    def __new__(cls, entity_data, entity_name):
        wrapped_entity_data = unicode.__new__(cls, entity_data)
        wrapped_entity_data._x_ktl10n_entity_name = entity_name
        return wrapped_entity_data

class HtmlSanitizer(Html5libSanitizer):
    #: Allowed HTML elements (tags).
    allowed_elements = ('p', 'br', 'a')

    #: Allowed attributes of HTML elements (tags).
    allowed_attributes = ()

    # CSS properties allowed in value of `style` attribute.
    allowed_css_properties = ()

    #: CSS keywords allowed in value of `style` attribute.
    allowed_css_keywords = ()

    #: Allowed SVG properties.
    allowed_svg_properties = ()

    #: Protocols allowed in value of attribute.
    allowed_protocols = ()

    #: Order-sensitive mapping required for modifying of starting sequence of
    #: sanitized tag.
    _sanitized_tag_prefix = (('</', '</sanitized'), ('<', '<sanitized'))

    def sanitize_token(self, token):
        # accommodate filters which use token_type differently
        # (required for using of this sanitizer as filter).
        old_token_type = token["type"]
        if old_token_type in html5lib.constants.tokenTypes.keys():
            old_token_type = html5lib.constants.tokenTypes[old_token_type]

        # Sanitize token.
        token = super(self.__class__, self).sanitize_token(token)        
        if token:
            if self._is_sanitized_token(old_token_type, token):
                # Modify starting sequence of sanitized tag.
                for prefix, sub in self._sanitized_tag_prefix:
                    if token['data'].startswith(prefix):
                        token['data'] = token['data'].replace(prefix, sub, 1)
                        break
            return token

    def _is_sanitized_token(self, old_token_type, token):
        # Tag token before sanitizing has type 'SmthTag', and after sanitizing
        # it is converted to token with type of text data ('Characters').
        #
        # Token of type 'SmthTag' has 'name' property, while token of text data
        # hasn't 'name' property. 
        return (old_token_type in (html5lib.constants.tokenTypes["StartTag"],
                                   html5lib.constants.tokenTypes["EndTag"], 
                                   html5lib.constants.tokenTypes["EmptyTag"])
                and token['type'] != old_token_type
                and 'name' not in token)

    def consumeEntity(self, allowedChar=None, fromAttribute=False):
        # This method originally consumes possible HTML entity from input
        # stream and (if valid entity was found) converts it into Unicode
        # character. It is irrevocable converting.
        #
        # We want to additionaly save entity by itself and pass it through
        # whole pipeline up to tree walking (where entity will be outputted).
        #
        # There is no easy way to intercept this method's work in the middle of
        # it's execution, so we need to:
        # * set some hook to see if any entity was converted
        # * (if any entity was converted) attach entity to some token attribute
        #   which guaranteedly will pass through whole pipeline.
        #
        # The hook is set on entities to Unicode characters mapping at
        # 'html5lib.tokenizer.entities' (see somewhere upper at code).
        # Original entity name is attached to 'data' attribute of token.
        #
        # TODO: handle numerical entities and entities in attributes.
        super(self.__class__, self).consumeEntity(allowedChar, fromAttribute)
        entity = _logged_entities.pop_remembered_key()
        if entity is not None:
            # Strip possible leading '&' and trailing ';'.
            entity_name = re.sub(r'^&?(.+);$', r'\1', entity)
            # Wrap original 'data' (Unicode character) into the same object
            # but with additional attribute contained entity name.
            entity_data = self.tokenQueue[-1]['data']
            wrapped_entity_data = EntityTokenData(entity_data, entity_name)
            self.tokenQueue[-1]['data'] = wrapped_entity_data

class SanitizedHtmlTreeWalker(html5lib.treewalkers.getTreeWalker('dom')):
    def getNodeDetails(self, node):
        # If entity name is attached to text node, return details of entity.
        if xml.dom.Node.TEXT_NODE == node.nodeType \
                and hasattr(node.nodeValue, '_x_ktl10n_entity_name'):
            return (html5lib.treewalkers._base.ENTITY,
                    node.nodeValue._x_ktl10n_entity_name)
        else:
            return super(self.__class__, self).getNodeDetails(node)

class StripToplevelHtmlTags(Html5libBaseFilter):
    """Strip top-level HTML tags from HTML parsing tree."""

    _toplevel_tags = ('html', 'head', 'body')

    def __iter__(self):
        for token in super(self.__class__, self).__iter__():
            if token['type'] not in ('StartTag', 'EndTag'):
                yield token
            elif token['name'] not in self._toplevel_tags:
                yield token

def sanitize_html(value):
    if not isinstance(value, basestring) or not len(value):
        return value

    parser = html5lib.HTMLParser(
        tokenizer=HtmlSanitizer,
        tree=html5lib.treebuilders.getTreeBuilder('dom'))
    walker = SanitizedHtmlTreeWalker
    serializer = html5lib.serializer.htmlserializer.HTMLSerializer(
        escape_lt_in_attrs=True,
        escape_rcdata=True,
        inject_meta_charset=False,
        resolve_entities=False,
        omit_optional_tags=False)

    tree = parser.parseFragment(value, container='body', encoding='utf8')
    stream = StripToplevelHtmlTags(walker(tree))
    return serializer.render(stream, encoding='utf8')

if '__main__' == __name__:
    import sys
    print(sanitize_html(' '.join(sys.argv[1:])))
