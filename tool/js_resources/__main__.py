"""
Tool for extracting translatable resources from JavaScript files used in
Pillow and WAF subsystems of Kindle Touch GUI.

This tool is meant to be run with Python 2.6 or Python 2.7.
"""

import sys

if sys.version_info[0] != 2 or sys.version_info[1] < 6:
    print('This tool is written for Python 2.6.x or Python 2.7.x only!')
    sys.exit(1)

import itertools, textwrap

from lib.cli_commands import ExtractCommand, CompileCommand

try:
    import argparse
except ImportError:
    from vendor.backport import argparse

class ArgparseHelpFormatter(argparse.ArgumentDefaultsHelpFormatter):
    def _fill_text(self, text, width, indent):
        lines = map(lambda l: l.strip(), text.strip().splitlines())
        content_groups = itertools.groupby(lines, lambda l: len(l) == 0)
        def transform_text(is_empty, lines):
            if is_empty:
                return '\n' * len(list(lines))
            else:
                return textwrap.fill(' '.join(lines), width,
                                     initial_indent=indent,
                                     subsequent_indent=indent)
        return ''.join(itertools.starmap(transform_text, content_groups))

    def _get_help_string(self, action):
        if action.default is None:
            return action.help
        else:
            return super(self.__class__, self)._get_help_string(action)

tool_description="""
    Tool for extracting/compiling resources (translatable strings) from/into
    JavaScript files used in Pillow and WAF subsystems of Kindle Touch.
"""
parser = argparse.ArgumentParser(description=tool_description,
                                 formatter_class=ArgparseHelpFormatter)
commands = parser.add_subparsers(title='subcommands',
    help='Mode of work')
command_extract = commands.add_parser('extract',
    formatter_class=ArgparseHelpFormatter,
    help='Extract resources from JavaScript files into .properties')
command_compile = commands.add_parser('compile',
    formatter_class=ArgparseHelpFormatter,
    help='Compile resources from .properties into JavaScript files')
command_help = commands.add_parser('help',
    formatter_class=ArgparseHelpFormatter,
    help='Show help on subcommand')

command_extract.set_defaults(cmd=ExtractCommand())
command_extract.add_argument('-m', '--manifest',
    action='store',
    dest='manifest',
    required=True,
    metavar='PATH/ALIAS',
    help="Manifest of JavaScript files to consider in resources' "
         "extracting")
command_extract.add_argument('-s', '--source-dir',
    action='store',
    dest='source_dir',
    required=True,
    metavar='PATH',
    help='Source directory of JavaScript files')
command_extract.add_argument('-d', '--dest-dir',
    action='store',
    dest='dest_dir',
    required=True,
    metavar='PATH',
    help='Destination directory of .properties files')
command_extract.add_argument('-l', '--locale',
    action='store',
    dest='locale',
    default='en_US',
    metavar='LOCALE_CODE',
    help='Locale code of extracted resources')

command_compile.set_defaults(cmd=CompileCommand())
command_compile.add_argument('-s', '--source-dir',
    action='store',
    dest='source_dir',
    required=True,
    metavar='PATH',
    help='Source directory of .properties files')
command_compile.add_argument('-d', '--dest-dir',
    action='store',
    dest='dest_dir',
    required=True,
    metavar='PATH',
    help='Destination directory of JavaScript files')
command_compile.add_argument('-b', '--source-locale',
    action='store',
    dest='source_locale',
    default='en_US',
    metavar='LOCALE_CODE',
    help='Locale code of source files to merge with at compiling')
command_compile.add_argument('-l', '--locale',
    action='store',
    nargs='+',
    dest='locale',
    default='*',
    metavar='LOCALE_CODE',
    help='Locale codes of .properties files to consider in compiling')

class CommandHelpAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        commands.choices[values].print_help()
        parser.exit()

command_help.add_argument('command',
    action=CommandHelpAction,
    nargs='?',
    choices=commands.choices.keys(),
    default='help',
    help='Subcommand for showing help on')

if 1 == len(sys.argv):
    parser.print_help()
    parser.exit()

args = parser.parse_args(sys.argv[1:])
if hasattr(args, 'cmd'):
    args.cmd.execute(args)

