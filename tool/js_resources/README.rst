Overview
--------

js_resources is a tool for dealing with resources (translatable strings) in
JavaScript files used in Pillow and WAF subsystems of Kindle Touch.

js_resources could:

* extract resources from Javascript files into files with format alike Java
  properties (but with content encoded in UTF-8),
* compile files of Java properties format back into JavaScript files.

The advantages of extracting/compiling instead of just using JavaScript files
are:

* there are a bit less of Amazon's copyrighted data in public access (at least,
  no plain JavaScript code),
* Java properties format is more common in software translation sphere, so file
  in Java properties-alike format could be uploaded to some online translation
  service (like transifex.net) for collaborative translating.

(Transifex.net supports Java properties-alike format with content encoded in
UTF-8 as MOZILLAPROPERTIES type of resource.)

Requirements
------------

Python 2.6 or Python 2.7 is required for working with tool. Python 3.x isn't
supported.

Invocation
----------

`python tool/js_resources <options>`

Yes, run THE directory by itself. Or `tool/js_resources/__main__.py`.

Extracting
~~~~~~~~~~

usage: js_resources extract [-h] -m PATH/ALIAS -s PATH -d PATH
                            [-l LOCALE_CODE]

Arguments:

  -h, --help
                        show this help message and exit

  -m PATH/ALIAS, --manifest PATH/ALIAS
                        Manifest of JavaScript files to consider in resources'
                        extracting

  -s PATH, --source-dir PATH
                        Source directory of JavaScript files

  -d PATH, --dest-dir PATH
                        Destination directory of .properties files

  -l LOCALE_CODE, --locale LOCALE_CODE
                        Locale code of extracted resources (default: en_US)

Currently, there are 2 pre-defined manifests with aliases *pillow* and *waf*.

For using of *pillow* manifest source directory should be the [copied] directory
`/usr/share/webkit-1.0/pillow` from Kindle Touch.

For using of *waf* manifest source directory should be the [copied] directory
`/var/local/waf` from Kindle Touch.

Compiling
~~~~~~~~~

usage: js_resources compile [-h] -s PATH -d PATH [-m LOCALE_CODE]
                            [-l LOCALE_CODE]

Arguments:

  -h, --help            
                        show this help message and exit

  -s PATH, --source-dir PATH
                        Source directory of .properties files

  -d PATH, --dest-dir PATH
                        Destination directory of JavaScript files

  -b LOCALE_CODE, --source-locale LOCALE_CODE
                        Locale code of source files to merge with at compiling
                        (default: en_US)

  -l LOCALE_CODE [LOCALE_CODE ...], --locale LOCALE_CODE [LOCALE_CODE ...]
                        Locale codes of .properties files to consider in
                        compiling (default: \*)

Directory named *locales* is created in destination directory. *locales* should
be copied to the same directory which was the source of original JavaScript
files.

Example: resources were extracted from `/var/local/waf` as source directory.
Therefore, resulting *locales* directory should be copied to `/var/local/waf`
(on Kindle Touch).

Reference
---------

Translatable resources of Pillow are located in separate JavaScript files at
directory */usr/share/webkit-1.0/pillow/strings* on Kindle Touch.

Translatable resources of WAF app are located in separate JavaScript file at
directory of this app (*/var/local/waf/<app_name>/*) on Kindle Touch. There are
no common scheme of placement of WAF resources for concrete WAF app. Some
examples: for adviewer app it's *scripts/resources.js*, for browser app it's
*js/strings.js*.

Structure of these JavaScript files is mostly common. There is one variable
assignment with value of object literal. Keys of object are resources names and
values are translatable strings. Some values could be object literals by itself
which keys are resources names and values are either translatable strings or
object literals etc.

Example:

    var AppStringTable = {
        helloMessage:   "Hello",
        byeMessage:     "Bye",
        statusMessages: {
          ok:    "OK",
          error: "Error"
        }
    };

Some JavaScript files contains specific code (though, in the end it is evaluated
to common scheme with one assignment of object literal). And some values of
object literal members are specific too.

Here is the list of exceptions from common scheme:

* */usr/share/webkit-1.0/pillow/strings/media_player_bar_strings.js*

  It contains value that is costructor of new object, like:

      var StringTable = { message: new Template("Hello, <% name %>") };

* */var/local/waf/adviewer/scripts/resources.js*

  Assigned object literal is wrapped into function call, like:

      var AppResources = (function() { 
        var messages = { hello: "Hello" };
        return {
          messages: messages
        };
      } ());

* */var/local/waf/browser/js/strings.js*

  There is no local scope variable assignment (that with 'var' keyword), but
  rather assignment to a new member of previously created object, like:

      wafapp.messages = { ... };

* */var/local/waf/browser/js/strings.js*

  One of value contained in assigned object literal is array, like

      wafapp.messages = { 
        locations: [
          {
            name: 'First location',
            path: '/some/path'
          },
          {
            name: 'Second location',
            path: '/some/path/too'
          }
        ]
      };

These exceptions are taken in account in js_resources tool.

