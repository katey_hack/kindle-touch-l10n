package ixtab.ktlocale;

import ixtab.ktlocale.modification.ArrayJoinerMapModificationStrategy;
import ixtab.ktlocale.modification.BlacklistedEntryRemoverMapModificationStrategy;
import ixtab.ktlocale.modification.EmptyValuesRemoverMapModificationStrategy;
import ixtab.ktlocale.modification.IncorrectEscapeFixerMapModificationStrategy;
import ixtab.ktlocale.modification.MapModificationStrategy;
import ixtab.ktlocale.modification.MessageFormatCreatorMapModificationStrategy;
import ixtab.ktlocale.modification.OffButtonsFixerMapModificationStrategy;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;

import contrib.IterableEnumeration;
import contrib.SortedProperties;

public class KTResourceBundle extends ListResourceBundle {

	public static class Descriptor {
		public final String packageName;
		public final String className;
		public final Locale locale;

		public Descriptor(String packageName, String className, Locale locale) {
			super();
			this.packageName = packageName;
			if (locale != null) {
				this.className = className;
				this.locale = locale;
			} else {
				if (className.contains("_")) {
					this.locale = parseLocale(className.substring(className
							.indexOf("_") + 1));
					this.className = className.substring(0,
							className.indexOf("_"));
				} else {
					this.className = className;
					this.locale = null;
				}
			}
		}

		public String toString() {
			return packageName + "." + suggestClassName();
		}

		public String suggestClassName() {
			if (locale == null) {
				return className;
			} else {
				return className + "_" + locale;
			}
		}
	}

	private final Object[][] contents;
	public final Descriptor descriptor;
	private final MapModificationStrategy[] modificationStrategies;

	public KTResourceBundle(ResourceBundle source,
			MapModificationStrategy... modificationStrategies) {
		this(source, getSourcePackageName(source), getSourceClassName(source),
				source.getLocale(), modificationStrategies);
	}

	private static String getSourceClassName(ResourceBundle source) {
		if (source instanceof KTResourceBundle) {
			return ((KTResourceBundle) source).descriptor.className;
		}
		return source.getClass().getSimpleName();
	}

	private static String getSourcePackageName(ResourceBundle source) {
		if (source instanceof KTResourceBundle) {
			return ((KTResourceBundle) source).descriptor.packageName;
		}
		return source.getClass().getPackage().getName();
	}

	public KTResourceBundle(ResourceBundle source, String packageName,
			String className, Locale locale,
			MapModificationStrategy... modificationStrategies) {
		this.modificationStrategies = modificationStrategies;
		descriptor = new Descriptor(packageName, className, locale);
		contents = extractContentsFrom(packageName, className, source);
	}

	public KTResourceBundle(Object[][] contents, String packageName,
			String className, Locale locale) {
		modificationStrategies = new MapModificationStrategy[0];
		this.contents = contents;
		descriptor = new Descriptor(packageName, className, locale);
	}

	private Object[][] extractContentsFrom(String packageName, String className, ResourceBundle source) {
		Enumeration<String> keys = source.getKeys();
		SortedMap<String, Object> map = new TreeMap<String, Object>();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			Object value = source.getObject(key);
			map.put(key, value);
		}
		for (MapModificationStrategy strategy : modificationStrategies) {
			strategy.updateResourceBundleMap(packageName, className, map);
		}
		return arrayFromSortedMap(map);
	}

	private Object[][] arrayFromSortedMap(SortedMap<String, Object> map) {
		Object[][] result = new Object[map.size()][];
		int i = 0;
		for (String key : map.keySet()) {
			result[i++] = new Object[] { key, map.get(key) };
		}
		return result;
	}

	@Override
	public Object[][] getContents() {
		return contents;
	}

	@Override
	public Locale getLocale() {
		return descriptor.locale;
	}

	public static KTResourceBundle fromDescriptor(BundleDescriptor descriptor) {
		try {
			FileInputStream fis = new FileInputStream(descriptor.file);
			BufferedInputStream bis = new BufferedInputStream(fis);
			InputStreamReader r = new InputStreamReader(bis, "UTF-8");
			PropertyResourceBundle input = new PropertyResourceBundle(r);
			return new KTResourceBundle(input, descriptor.packageName,
					descriptor.bundleName, parseLocale(descriptor.locale),
					new BlacklistedEntryRemoverMapModificationStrategy(),
					new IncorrectEscapeFixerMapModificationStrategy(),
					new MessageFormatCreatorMapModificationStrategy(),
					new ArrayJoinerMapModificationStrategy(),
					// this should be after array joining: there are indeed "" values in some message formats!
					new EmptyValuesRemoverMapModificationStrategy(),
					new OffButtonsFixerMapModificationStrategy());
		} catch (IOException io) {
			throw new RuntimeException(io);
		}
	}

	public static Locale parseLocale(String language) {
		String country = null;
		if (language.contains("_")) {
			country = language.substring(language.indexOf("_") + 1);
			language = language.substring(0, language.indexOf("_"));
		}
		if (country != null) {
			return new Locale(language, country);
		}
		return new Locale(language);
	}

	public String toString() {
		return descriptor.toString();
	}

	public Properties toProperties() {
		Properties props = new SortedProperties();
		for (String key : IterableEnumeration.make(getKeys())) {
			props.put(key, getObject(key));
		}
		return props;
	}
}
