#!/bin/sh

lipc-send-event com.lab126.blanket.ota otaSplashProgress -i 0
mntroot rw || exit 1

DIR=`pwd`
cd /

if [ `localedef --list-archive|grep -c <LOC_SYSTEM>.utf8` != 1 ]; then
  tar xzvf $DIR/glibc-locale.tgz
fi
lipc-send-event com.lab126.blanket.ota otaSplashProgress -i 50

tar xzvf $DIR/locale.tgz
lipc-send-event com.lab126.blanket.ota otaSplashProgress -i 100