#!/bin/sh

lipc-send-event com.lab126.blanket.ota otaSplashProgress -i 0
mntroot rw || exit 1

# remove locale-specific files
rm -rf /usr/share/locale/<LOC_SYSTEM>
rm -f /opt/amazon/ebook/lib/locale-<LOC_FULL>.jar
rm -f /opt/amazon/ebook/config/locales/<LOC_FULL>.properties
test -e /usr/lib/locale/<LOC_SYSTEM>.utf8 && rm -rf /usr/lib/locale/<LOC_SYSTEM>.utf8
lipc-send-event com.lab126.blanket.ota otaSplashProgress -i 25

# _RUN_DELETE_COMMANDS_ #

lipc-send-event com.lab126.blanket.ota otaSplashProgress -i 50

# reset locale to english if uninstalling current locale
if [ `grep -c =<LOC_SYSTEM> /var/local/system/locale` != 0 ]; then
  echo "LANG=en_US.utf8" > /var/local/system/locale
  echo "LC_ALL=en_US.utf8" >> /var/local/system/locale

  sed -i -e 's/locale=.*/locale=en-US/' /var/local/java/prefs/com.amazon.ebook.framework/prefs
fi
lipc-send-event com.lab126.blanket.ota otaSplashProgress -i 100
