package ixtab.ktlocale;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public final class Workspace {

	public static final String DIR_JAVA = "framework";
	public static final String DIR_BLANKET = "locale";
	public static final String DIR_WAF = "waf";
	public static final String DIR_PILLOW = "pillow";

	private final RuntimeException invalidWorkspace;
	public final File baseDir;
	public final File javaDir;
	public final File blanketDir;
	public final File pillowDir;
	public final File wafDir;

	public Workspace(File baseDirectory) {
		this.baseDir = baseDirectory;
		invalidWorkspace = new IllegalArgumentException(
				baseDirectory
						+ " is not a valid translation base directory. A valid directory must contain \""
						+ DIR_JAVA + "\",\"" + DIR_BLANKET + "\",\"" + DIR_PILLOW + "\", and \"" + DIR_WAF
						+ "\" subdirectories");
		
		javaDir = getDirectory(DIR_JAVA);
		blanketDir = getDirectory(DIR_BLANKET);
		pillowDir = getDirectory(DIR_PILLOW);
		wafDir = getDirectory(DIR_WAF);
		verifyIsValidWorkspace();
	}

	private void verifyIsValidWorkspace() {
		boolean ok = javaDir.isDirectory();
		ok &= blanketDir.isDirectory();
		ok &= pillowDir.isDirectory();
		ok &= wafDir.isDirectory();
		
		if (!ok) {
			throw invalidWorkspace;
		}
	}

	private File getDirectory(String dirname) {
		File f;
		try {
			f = new File(baseDir.getCanonicalPath() + File.separator
					+ dirname);
			return f;
		} catch (IOException e) {
			throw invalidWorkspace;
		}
	}

	public List<String> listAvailableLocales() {
		List<String> locales = new ArrayList<String>();
		for (File dir: blanketDir.listFiles()) {
			if (dir.isDirectory() && !dir.getName().startsWith("en")) {
				locales.add(dir.getName());
			}
		}
		Collections.sort(locales);
		return locales;
	}

	public List<File> findBlanketFiles(Locale locale) throws IOException {
		List<File> files = new ArrayList<File>();
		File directory = new File(blanketDir.getCanonicalPath()+File.separator+locale.toString());
		addBlanketFilesRecursively(files, directory);
		return files;
	}

	private void addBlanketFilesRecursively(List<File> files, File directory) {
		for (File file: directory.listFiles()) {
			if (file.isDirectory()) {
				addBlanketFilesRecursively(files, file);
			} else if (file.getName().endsWith(".po")) {
				files.add(file);
			}
		}
	}
}
