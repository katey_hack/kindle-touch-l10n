package ixtab.ktlocale.cldr;

import ixtab.ktlocale.NonStandardLanguageTerritories;
import ixtab.ktlocale.util.LocaleUtil;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import contrib.SortedProperties;


public class CLDRLocale {
	public static final int LANGUAGE_ONLY = 0;
	public static final int LANGUAGE_AND_TERRITORY = 1;
	public static final CLDRLocale ENGLISH;
	
	public static final String KEY_LOCALE = "locale";
	public static final String KEY_DISPLAY_NAME = "display.name";
	public static final String KEY_LOCALES_SUPPORTED = "locales.supported";
	public static final String KEY_DISPLAY_NAME_LOC_PREFIX = "display.name.";
	public static final String KEY_POSIX_ID = "posix.id";
	

	
	private static final DocumentBuilder BUILDER;
	private static final XPath XPATH;
	private static final XPathExpression XPATH_IDENTITY_LANGUAGE;
	private static final XPathExpression XPATH_IDENTITY_TERRITORY;
	private static final XPathExpression XPATH_VALID_SUBLOCALES;
	private static final MessageFormat XPATH_LOOKUP_LANGUAGE = new MessageFormat("/ldml/localeDisplayNames/languages/language[@type=\"{0}\"]/text()", Locale.US);
	private static final MessageFormat XPATH_LOOKUP_TERRITORY = new MessageFormat("/ldml/localeDisplayNames/territories/territory[@type=\"{0}\"]/text()", Locale.US);

	
	public final Locale locale;
	private final Document xml;
	private final List<Document> additionalXml = new ArrayList<Document>();
	private final List<String> additionalNames = new ArrayList<String>();
	
	
	static {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		docFactory.setNamespaceAware(true);
		BUILDER = createBuilder(docFactory);
		XPathFactory factory = XPathFactory.newInstance();
		XPATH = factory.newXPath();
		XPATH_IDENTITY_LANGUAGE = compile("/ldml/identity/language/@type");
		XPATH_IDENTITY_TERRITORY = compile("/ldml/identity/territory/@type");
		XPATH_VALID_SUBLOCALES = compile("/ldml/collations/@validSubLocales");
		ENGLISH = new CLDRLocale(Locale.ENGLISH, null);
	}

	private static DocumentBuilder createBuilder(DocumentBuilderFactory docFactory) {
		try {
			return docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			return null;
		}
	}
	
	private static XPathExpression compile(String expression) {
		try {
			return XPATH.compile(expression);
		} catch (XPathExpressionException e) {
			return null;
		}
	}

	public static CLDRLocale[] load(Locale locale) {
		Locale languageOnly, languageAndTerritory;
		if (locale.getCountry().equals("")) {
			languageOnly = locale;
			languageAndTerritory = getLocaleWithTerritory(languageOnly);
		} else {
			languageAndTerritory = locale;
			languageOnly = new Locale(languageAndTerritory.getLanguage());
		}
		
		try {
			CLDRLocale parent = new CLDRLocale(languageOnly, null);
			return new CLDRLocale[] {parent, new CLDRLocale(languageAndTerritory, parent)};
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	public static Locale getLocaleWithTerritory(Locale localeWithoutTerritory) {
		try {
			Locale custom = NonStandardLanguageTerritories.get(localeWithoutTerritory);
			if (custom != null) {
				return custom;
			}
			
			Document xml = BUILDER.parse(CLDRLoader.getCLDRFileAsStream(localeWithoutTerritory.toString()));
			String validSubLocales = XPATH_VALID_SUBLOCALES.evaluate(xml);
			String defaultSubLocale = findDefaultSubLocale(localeWithoutTerritory.getLanguage(), validSubLocales);
			if (defaultSubLocale != null) {
				return LocaleUtil.localeFromString(defaultSubLocale);
			}
			
			/* Fallback: for some obscure reason, valid sublocales are not set for some languages. (e.g. it => it_IT).
			 * If the english version contains a territory matching the uppercased locale, we assume
			 * this to be valid.
			 */
			Document english = BUILDER.parse(CLDRLoader.getCLDRFileAsStream(ENGLISH.locale.toString()));
			String candidateTerritory = localeWithoutTerritory.toString().toUpperCase();
			String englishTerritory = evaluateXPath(english, XPATH_LOOKUP_TERRITORY, candidateTerritory);
			if (englishTerritory != null && englishTerritory.trim().length() > 0) {
				return LocaleUtil.localeFromString(localeWithoutTerritory.toString()+ "_" +candidateTerritory);
			}
			System.err.print("No default territory could automatically be determined for locale \""+localeWithoutTerritory+"\". ");
			System.err.println("You have to add this locale to ixtab.ktlocale.NonStandardLanguageTerritories.");
			return null;
		} catch (Exception e) {
			System.err.println(e);
			return null;
		}
	}
	
	private static String findDefaultSubLocale(String lang,
			String valid) {
		String heuristic = lang + "_" + lang.toUpperCase();
		int total = 0;
		String last = null;
		StringTokenizer tokens = new StringTokenizer(valid);
		while (tokens.hasMoreTokens()) {
			last = tokens.nextToken();
			++total;
			if (last.equals(heuristic)) {
				return last;
			}
		}
		if (total == 1) {
			// only one valid territory for that language.
			return last;
		}
		return null;
	}

	private CLDRLocale(Locale locale, CLDRLocale parent) {
		try {
			this.locale = locale;
			/* this is required for zh_CN, for example. For reasons unknown to me,
			 * the valid sublocale is named "zh_Hans_CN", but at least referred to
			 * in "zh.xml".
			 */
			InputStream xmlFileStream = CLDRLoader.getCLDRFileAsStream(locale, parent);
			this.xml = getXmlFromStream(xmlFileStream);
			sanityCheck(locale.getLanguage(), XPATH_IDENTITY_LANGUAGE);
			sanityCheck(locale.getCountry(), XPATH_IDENTITY_TERRITORY);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	private Document getXmlFromStream(InputStream xmlFileStream)
			throws SAXException, IOException {
		return BUILDER.parse(xmlFileStream);
	}
	
	void addAdditionalData(String name, InputStream stream) {
		if (stream != null) {
			try {
				additionalNames.add(name);
				additionalXml.add(0, getXmlFromStream(stream));
			} catch (SAXException e) {
			} catch (IOException e) {
			}
		}
	}
	
	private void sanityCheck(String expected,
			XPathExpression expression) {
		if (expected.equals("")) {
			return;
		}
		try {
			Object actual = expression.evaluate(xml, XPathConstants.STRING);
			if (!expected.equals(actual)) {
				throw new IllegalStateException("Expected "+expected+", but got "+actual);
			}
		} catch (XPathExpressionException e) {
		}
	}

	private void addDescriptions(Locale lookup, List<String[]> descriptions) {
		List<Document> documents = new ArrayList<Document>();
		documents.add(xml);
		documents.addAll(additionalXml);
		for (Document doc: documents) {
			String[] result = new String[2];
			result[0] = determineLanguage(doc, lookup.getLanguage());
			if (!lookup.getCountry().equals("")) {
				result[1] = evaluateXPath(doc, XPATH_LOOKUP_TERRITORY, lookup.getCountry());
			}
			descriptions.add(result);
		}
	}

	private String determineLanguage(Document doc, String language) {
		String result = evaluateXPath(doc, XPATH_LOOKUP_LANGUAGE, language);
		// (probably) only relevant for chinese
		if (locale.toString().startsWith(language)) {
			for (String altLanguage: additionalNames) {
				String altResult = evaluateXPath(doc, XPATH_LOOKUP_LANGUAGE, altLanguage);
				if (altResult != null) {
					//System.err.println(language+" ALT/"+altLanguage+": " + altResult);
					result = altResult; //break;
				}
			}
		}

		return result;
	}

	private static String evaluateXPath(Document xml, MessageFormat format,
			String content) {
		String expr = format.format(new Object[] {content});
		try {
			String result = XPATH.compile(expr).evaluate(xml);
			if (result != null && result.trim().equals("")) {
				result = null;
			}
			return result;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return null;
		}
	};

	public static String describe(Locale described, CLDRLocale... describing) {
		/* as "base" locales must (seemingly) always be associated with a territory,
		 * we also turn base locales into their default territory locale.
		 */
		if (described.getCountry().equals("")) {
			described = CLDRLocale.load(described)[CLDRLocale.LANGUAGE_AND_TERRITORY].locale;
		}
		List<String[]> descriptions = new ArrayList<String[]>();
		for (int i=0; i < describing.length; ++i) {
			describing[i].addDescriptions(described, descriptions);
		}
		return makeDescription(describing[describing.length-1].locale, descriptions.toArray(new String[descriptions.size()][]));
	}
	
	private static String makeDescription(Locale locale, String[]... langAndTerritory) {
		String[] finalInput = new String[2];
		for (int i=langAndTerritory.length-1; i >= 0; --i) {
			if (finalInput[0] == null) {
				finalInput[0] =  langAndTerritory[i][0];
			}
			if (finalInput[1] == null) {
				finalInput[1] =  langAndTerritory[i][1];
			}
		}
		if (finalInput[0] != null && finalInput[0].length() > 1) {
			finalInput[0] = finalInput[0].substring(0, 1).toUpperCase(locale) + finalInput[0].substring(1);
		}
		if (finalInput[1] != null) {
			return finalInput[0].trim() + " ("+finalInput[1]+")";
		} else {
			return finalInput[0];
		}
	}

	public static Properties getProperties(Locale target) {
		CLDRLocale[] both = CLDRLocale.load(target);
		CLDRLocale authoritative = both[target.getCountry().equals("") ? LANGUAGE_ONLY : LANGUAGE_AND_TERRITORY];
		String supported = both[LANGUAGE_AND_TERRITORY].locale.toString().replace('_', '-');
		Properties props = new SortedProperties(new LocalePropertiesKeyComparator(supported));
		props.put(KEY_LOCALE, both[0].locale.toString());
		props.put(KEY_DISPLAY_NAME, describe(authoritative.locale, ENGLISH));
		props.put(KEY_LOCALES_SUPPORTED, supported);
		props.put(KEY_DISPLAY_NAME_LOC_PREFIX+supported, describe(authoritative.locale, both));
		props.put(KEY_POSIX_ID,"en_US.UTF-8");
		return props;
	}
	
	public static class LocalePropertiesKeyComparator implements Comparator<String> {
		private String[] keys = new String[] {
				KEY_LOCALE, KEY_DISPLAY_NAME, KEY_LOCALES_SUPPORTED, null, KEY_POSIX_ID
		};
		
		public LocalePropertiesKeyComparator(String supported) {
			for (int i=0; i < keys.length; ++i) {
				if (keys[i] == null) {
					keys[i] = KEY_DISPLAY_NAME_LOC_PREFIX + supported;
					break;
				}
			}
		}
		
		@Override
		public int compare(String o1, String o2) {
			int i1 = getArrayPosition(o1);
			int i2 = getArrayPosition(o2);
			return Integer.valueOf(i1).compareTo(Integer.valueOf(i2));
		}

		private int getArrayPosition(String o1) {
			for (int i=0; i < keys.length; ++i) {
				if (keys[i].equals(o1)) {
					return i;
				}
			}
			return -1;
		}
		
	}

	public List<String> getPotentialSubLocales(Locale wanted) {
		List<String> candidates = new ArrayList<String>();
		try {
			String validSubLocales = XPATH_VALID_SUBLOCALES.evaluate(xml);
			StringTokenizer tokens = new StringTokenizer(validSubLocales);
			while (tokens.hasMoreTokens()) {
				String candidate = tokens.nextToken();
				if (isPotentiallyCorrectSubLocale(candidate, wanted)) {
					candidates.add(candidate);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return candidates;
	}

	private boolean isPotentiallyCorrectSubLocale(String candidate,
			Locale wanted) {
		return candidate.startsWith(wanted.getLanguage()+"_") && candidate.endsWith("_"+wanted.getCountry());
	}
}
