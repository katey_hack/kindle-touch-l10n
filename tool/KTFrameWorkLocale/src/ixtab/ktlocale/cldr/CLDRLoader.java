package ixtab.ktlocale.cldr;

import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class CLDRLoader {
	public static URL getCLDRFileAsURL(String locale) {
		return CLDRLoader.class.getResource(locale+".xml");
	}
	
	public static InputStream getCLDRFileAsStream(Locale locale, CLDRLocale parent) {
		InputStream result = getCLDRFileAsStream(locale.toString());
		if (result == null) {
			if (parent != null) {
				List<String> fallback = parent.getPotentialSubLocales(locale);
				if (fallback.size() == 1) {
					String fullFileName = fallback.get(0);
					addAlternativesToParent(parent, fullFileName);
					return getCLDRFileAsStream(fullFileName);
				}
				else {
					if (fallback.size() > 1) {
						System.err.println("While looking for fallback definitions for "+locale+", multiple alternatives were found: "+fallback);
						System.err.println("I don't know what to do.");
					}
				}
			}
			System.err.println("No xml definition for "+locale+" could be found. This will result in a total fuckup in a few moments.");
		}
		return result;
	}
	
	public static InputStream getCLDRFileAsStream(String locale) {
		return CLDRLoader.class.getResourceAsStream(locale+".xml");
	}
	
	private static void addAlternativesToParent(CLDRLocale parent,
			String name) {
		int underscore = name.lastIndexOf('_');
		if (underscore == -1) {
			return;
		}
		name = name.substring(0, underscore);
		if (parent.locale.toString().equals(name)) return;
		parent.addAdditionalData(name, getCLDRFileAsStream(name));
		// recurse until we reach parent locale name, or no more underscores left
		addAlternativesToParent(parent, name);
	}


}
