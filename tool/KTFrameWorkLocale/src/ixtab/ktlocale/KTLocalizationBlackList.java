package ixtab.ktlocale;

import java.util.HashSet;
import java.util.Set;


/**
 * A very crude way of blacklisting various patterns from localization.
 * These files have been blacklisted (=explicitly excluded from localizations)
 * because they only contain strings where it doesn't seem to make sense
 * to translate them.
 * 
 * These patterns have been identified manually, i.e. by
 * looking through the "translatable" resources and manually flagging
 * the ones which obviously make no sense to translate.
 * 
 * For now, behavior is hardcoded in this class, but it may well make
 * sense to make the patterns more configurable (e.g. by outsourcing
 * them into a file).
 * 
 * Blacklist patterns match any bundle which starts with the pattern.
 * I.e., the pattern "resource" matches "resource", "resource_de", "resource_ru" etc.
 * As a very special case, if the pattern ends with "$", then an exact match
 * is required. I.e., the pattern "resource$" ONLY matches "resource", but not "resource_de"
 * @author ixtab
 *
 */
public class KTLocalizationBlackList {

	public static class ResourceEntry {
		public final String packageName;
		public final String className;
		public final String entryName;
		public ResourceEntry(String packageName, String className,
				String entryName) {
			super();
			this.packageName = packageName;
			this.className = className;
			this.entryName = entryName;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((className == null) ? 0 : className.hashCode());
			result = prime * result
					+ ((entryName == null) ? 0 : entryName.hashCode());
			result = prime * result
					+ ((packageName == null) ? 0 : packageName.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ResourceEntry other = (ResourceEntry) obj;
			if (className == null) {
				if (other.className != null)
					return false;
			} else if (!className.equals(other.className))
				return false;
			if (entryName == null) {
				if (other.entryName != null)
					return false;
			} else if (!entryName.equals(other.entryName))
				return false;
			if (packageName == null) {
				if (other.packageName != null)
					return false;
			} else if (!packageName.equals(other.packageName))
				return false;
			return true;
		}
	}
	
	private static String[] BLACKLISTED_CLASSNAMES = new String[] {
					"com.amazon.ebook.util.resources.LcidMappings",
					"com.amazon.ebook.util.resources.MccCodeMappings",
					"com.amazon.kindle.settings.resources.DiagnosticResources",
					"com.amazon.ebook.util.text.stopword.resources.StopWordData",
					"com.amazon.ebook.booklet.reader.resources.DefaultDictionaries",
					"com.amazon.ebook.util.dict.resources.SimpleDictLookupFormatResources",
					"com.amazon.ebook.util.resources.CliticUtilResources",
					"com.amazon.ebook.util.resources.SimpleTextListFormatResources$",
					

	};
	
	private static Set<ResourceEntry> BLACKLISTED_ENTRIES = getBlackListedEntries();
	
	private static Set<ResourceEntry> getBlackListedEntries() {
		Set<ResourceEntry> set = new HashSet<ResourceEntry>();
		
		/*
		 * The below entries have been determined using a combination of manual effort
		 * and automatic output of tool/checkit.py. They might include "false positives",
		 * and they might have missed "false negatives".
		 */

		// com.amazon.agui.swing.resources.ComponentResources#ContentItemView.tag_separator=,
		set.add(new ResourceEntry("com.amazon.agui.swing.resources", "ComponentResources", "ContentItemView.tag_separator"));
		// com.amazon.agui.swing.resources.ComponentResources#DetailElementView.value.separator=\\:
		set.add(new ResourceEntry("com.amazon.agui.swing.resources", "ComponentResources", "DetailElementView.value.separator"));
		// com.amazon.agui.swing.resources.ComponentResources#Header.number.template=({0,number,integer})
		set.add(new ResourceEntry("com.amazon.agui.swing.resources", "ComponentResources", "Header.number.template"));
		// com.amazon.agui.swing.resources.ComponentResources#HeaderBar.primary.format={0} {1}
		set.add(new ResourceEntry("com.amazon.agui.swing.resources", "ComponentResources", "HeaderBar.primary.format"));
		// com.amazon.agui.swing.resources.ComponentResources#HeaderBar.separator=·
		set.add(new ResourceEntry("com.amazon.agui.swing.resources", "ComponentResources", "HeaderBar.separator"));
		// com.amazon.agui.swing.resources.ComponentResources#PendingContentItemView.stateTemplate={0} - {1,number,percent}
		set.add(new ResourceEntry("com.amazon.agui.swing.resources", "ComponentResources", "PendingContentItemView.stateTemplate"));
		// com.amazon.agui.swing.resources.ComponentResources#action.switch_app.ad.target=com.amazon.kindle.booklet.ad
		set.add(new ResourceEntry("com.amazon.agui.swing.resources", "ComponentResources", "action.switch_app.ad.target"));
		// com.amazon.agui.swing.resources.ComponentResources#action.switch_app.home.target=com.lab126.booklet.home
		set.add(new ResourceEntry("com.amazon.agui.swing.resources", "ComponentResources", "action.switch_app.home.target"));
		// com.amazon.agui.swing.resources.ComponentResources#action.switch_app.reader.target=com.lab126.booklet.reader
		set.add(new ResourceEntry("com.amazon.agui.swing.resources", "ComponentResources", "action.switch_app.reader.target"));
		// com.amazon.agui.swing.resources.ComponentResources#action.switch_app.settings.target=com.lab126.booklet.settings
		set.add(new ResourceEntry("com.amazon.agui.swing.resources", "ComponentResources", "action.switch_app.settings.target"));
		// com.amazon.agui.swing.resources.ComponentResources#action.switch_app.store.target=com.lab126.store
		set.add(new ResourceEntry("com.amazon.agui.swing.resources", "ComponentResources", "action.switch_app.store.target"));
		// com.amazon.ebook.booklet.pdfreader.impl.resources.PDFReaderImplResources#pdfreader.pdfnavigator.startReadng.pageOne=1
		set.add(new ResourceEntry("com.amazon.ebook.booklet.pdfreader.impl.resources", "PDFReaderImplResources", "pdfreader.pdfnavigator.startReadng.pageOne"));
		// com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources.BlogHomeResources#event.gesture.next.vertical=north
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources", "BlogHomeResources", "event.gesture.next.vertical"));
		// com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources.BlogHomeResources#event.gesture.next=west
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources", "BlogHomeResources", "event.gesture.next"));
		// com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources.BlogHomeResources#event.gesture.previous.vertical=south
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources", "BlogHomeResources", "event.gesture.previous.vertical"));
		// com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources.BlogHomeResources#event.gesture.previous=east
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources", "BlogHomeResources", "event.gesture.previous"));
		// com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources.BlogHomeResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources", "BlogHomeResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources.BlogHomeSpecialWidgetResources#event.gesture.next=west
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources", "BlogHomeSpecialWidgetResources", "event.gesture.next"));
		// com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources.BlogHomeSpecialWidgetResources#event.gesture.previous=east
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources", "BlogHomeSpecialWidgetResources", "event.gesture.previous"));
		// com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources.BlogHomeSpecialWidgetResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources", "BlogHomeSpecialWidgetResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources.BlogHomeWidgetResources#event.gesture.next=west
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources", "BlogHomeWidgetResources", "event.gesture.next"));
		// com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources.BlogHomeWidgetResources#event.gesture.previous=east
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources", "BlogHomeWidgetResources", "event.gesture.previous"));
		// com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources.BlogHomeWidgetResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.blogs.ui.views.home.resources", "BlogHomeWidgetResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.common.ui.resources.PeriodicalGridViewResources#event.gesture.next=west
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.common.ui.resources", "PeriodicalGridViewResources", "event.gesture.next"));
		// com.amazon.ebook.booklet.periodicals.common.ui.resources.PeriodicalGridViewResources#event.gesture.previous=east
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.common.ui.resources", "PeriodicalGridViewResources", "event.gesture.previous"));
		// com.amazon.ebook.booklet.periodicals.common.ui.resources.TOCWidgetResources#event.gesture.flick.east=east
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.common.ui.resources", "TOCWidgetResources", "event.gesture.flick.east"));
		// com.amazon.ebook.booklet.periodicals.common.ui.resources.TOCWidgetResources#event.gesture.flick.north=north
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.common.ui.resources", "TOCWidgetResources", "event.gesture.flick.north"));
		// com.amazon.ebook.booklet.periodicals.common.ui.resources.TOCWidgetResources#event.gesture.flick.south=south
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.common.ui.resources", "TOCWidgetResources", "event.gesture.flick.south"));
		// com.amazon.ebook.booklet.periodicals.common.ui.resources.TOCWidgetResources#event.gesture.flick.west=west
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.common.ui.resources", "TOCWidgetResources", "event.gesture.flick.west"));
		// com.amazon.ebook.booklet.periodicals.common.ui.resources.TOCWidgetResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.common.ui.resources", "TOCWidgetResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.impl.detail.resources.PeriodicalsDetailViewResources#goto.action.content=content
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.impl.detail.resources", "PeriodicalsDetailViewResources", "goto.action.content"));
		// com.amazon.ebook.booklet.periodicals.impl.detail.resources.PeriodicalsDetailViewResources#goto.action.first=first
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.impl.detail.resources", "PeriodicalsDetailViewResources", "goto.action.first"));
		// com.amazon.ebook.booklet.periodicals.impl.detail.resources.PeriodicalsDetailViewResources#goto.action=gotoaction
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.impl.detail.resources", "PeriodicalsDetailViewResources", "goto.action"));
		// com.amazon.ebook.booklet.periodicals.magazines.ui.views.cover.resources.CoverViewResources#event.gesture.next.alternate=north
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.magazines.ui.views.cover.resources", "CoverViewResources", "event.gesture.next.alternate"));
		// com.amazon.ebook.booklet.periodicals.magazines.ui.views.cover.resources.CoverViewResources#event.gesture.next=west
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.magazines.ui.views.cover.resources", "CoverViewResources", "event.gesture.next"));
		// com.amazon.ebook.booklet.periodicals.magazines.ui.views.cover.resources.CoverViewResources#event.gesture.previous=east
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.magazines.ui.views.cover.resources", "CoverViewResources", "event.gesture.previous"));
		// com.amazon.ebook.booklet.periodicals.magazines.ui.views.cover.resources.CoverViewResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.magazines.ui.views.cover.resources", "CoverViewResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources.MagazineHomeResources#event.gesture.next.vertical=north
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources", "MagazineHomeResources", "event.gesture.next.vertical"));
		// com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources.MagazineHomeResources#event.gesture.next=west
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources", "MagazineHomeResources", "event.gesture.next"));
		// com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources.MagazineHomeResources#event.gesture.previous.vertical=south
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources", "MagazineHomeResources", "event.gesture.previous.vertical"));
		// com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources.MagazineHomeResources#event.gesture.previous=east
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources", "MagazineHomeResources", "event.gesture.previous"));
		// com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources.MagazineHomeResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources", "MagazineHomeResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources.MagazineHomeWidgetResources#event.gesture.next=west
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources", "MagazineHomeWidgetResources", "event.gesture.next"));
		// com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources.MagazineHomeWidgetResources#event.gesture.previous=east
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources", "MagazineHomeWidgetResources", "event.gesture.previous"));
		// com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources.MagazineHomeWidgetResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.magazines.ui.views.home.resources", "MagazineHomeWidgetResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ArticlePaneWidgetResources#event.gesture.flick_north=north
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ArticlePaneWidgetResources", "event.gesture.flick_north"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ArticlePaneWidgetResources#event.gesture.flick_south=south
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ArticlePaneWidgetResources", "event.gesture.flick_south"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ArticlePaneWidgetResources#event.gesture.next=east
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ArticlePaneWidgetResources", "event.gesture.next"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ArticlePaneWidgetResources#event.gesture.previous=west
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ArticlePaneWidgetResources", "event.gesture.previous"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ArticlePaneWidgetResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ArticlePaneWidgetResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ArticleTitleWidgetResources#event.gesture.flick_north=north
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ArticleTitleWidgetResources", "event.gesture.flick_north"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ArticleTitleWidgetResources#event.gesture.flick_south=south
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ArticleTitleWidgetResources", "event.gesture.flick_south"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ArticleTitleWidgetResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ArticleTitleWidgetResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ContentViewFooterResources#button.next.image=images/Arrow_Down.png
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ContentViewFooterResources", "button.next.image"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ContentViewFooterResources#button.next.text=∨
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ContentViewFooterResources", "button.next.text"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ContentViewFooterResources#button.previous.image=images/Arrow_Up.png
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ContentViewFooterResources", "button.previous.image"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ContentViewFooterResources#button.previous.text=∧
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ContentViewFooterResources", "button.previous.text"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ContentViewResources#footer.action.front.magazine.label.text=Cover
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ContentViewResources", "footer.action.front.magazine.label.text"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ContentViewResources#this.pane.article.layout=East
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ContentViewResources", "this.pane.article.layout"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.ContentViewResources#this.pane.section.layout=West
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "ContentViewResources", "this.pane.section.layout"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.SectionPaneWidgetResources#event.gesture.flick_north=north
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "SectionPaneWidgetResources", "event.gesture.flick_north"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.SectionPaneWidgetResources#event.gesture.flick_south=south
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "SectionPaneWidgetResources", "event.gesture.flick_south"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.SectionPaneWidgetResources#event.gesture.next=east
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "SectionPaneWidgetResources", "event.gesture.next"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.SectionPaneWidgetResources#event.gesture.previous=west
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "SectionPaneWidgetResources", "event.gesture.previous"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.SectionPaneWidgetResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "SectionPaneWidgetResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources.SectionTitleWidgetResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.contentview.resources", "SectionTitleWidgetResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.home.resources.PeriodicalsHomeWidgetResources#event.gesture.flick.east=east
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.home.resources", "PeriodicalsHomeWidgetResources", "event.gesture.flick.east"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.home.resources.PeriodicalsHomeWidgetResources#event.gesture.flick.west=west
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.home.resources", "PeriodicalsHomeWidgetResources", "event.gesture.flick.west"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.home.resources.PeriodicalsHomeWidgetResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.home.resources", "PeriodicalsHomeWidgetResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.sectionstart.resources.SectionStartResources#event.gesture.flick.east=east
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.sectionstart.resources", "SectionStartResources", "event.gesture.flick.east"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.sectionstart.resources.SectionStartResources#event.gesture.flick.west=west
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.sectionstart.resources", "SectionStartResources", "event.gesture.flick.west"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.sectionstart.resources.SectionStartResources#event.gesture.section.next=north
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.sectionstart.resources", "SectionStartResources", "event.gesture.section.next"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.sectionstart.resources.SectionStartResources#event.gesture.section.previous=south
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.sectionstart.resources", "SectionStartResources", "event.gesture.section.previous"));
		// com.amazon.ebook.booklet.periodicals.newspapers.ui.views.sectionstart.resources.SectionStartResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.newspapers.ui.views.sectionstart.resources", "SectionStartResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.ui.resources.PeriodicalReadingPaneResources#event.gesture.article.footer.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.ui.resources", "PeriodicalReadingPaneResources", "event.gesture.article.footer.tap"));
		// com.amazon.ebook.booklet.periodicals.ui.resources.PeriodicalReadingPaneResources#event.gesture.article.next=north
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.ui.resources", "PeriodicalReadingPaneResources", "event.gesture.article.next"));
		// com.amazon.ebook.booklet.periodicals.ui.resources.PeriodicalReadingPaneResources#event.gesture.article.previous=south
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.ui.resources", "PeriodicalReadingPaneResources", "event.gesture.article.previous"));
		// com.amazon.ebook.booklet.periodicals.ui.resources.ReadingPaneFooterResources#message.default.string=
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.ui.resources", "ReadingPaneFooterResources", "message.default.string"));
		// com.amazon.ebook.booklet.periodicals.ui.resources.ReadingPaneFooterResources#swipe.down.indicator=▲
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.ui.resources", "ReadingPaneFooterResources", "swipe.down.indicator"));
		// com.amazon.ebook.booklet.periodicals.ui.resources.ReadingPaneHeaderResources#event.gesture.tap=tap
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.ui.resources", "ReadingPaneHeaderResources", "event.gesture.tap"));
		// com.amazon.ebook.booklet.periodicals.ui.resources.ReadingPaneHeaderResources#message.default.string=
		set.add(new ResourceEntry("com.amazon.ebook.booklet.periodicals.ui.resources", "ReadingPaneHeaderResources", "message.default.string"));
		// com.amazon.ebook.booklet.reader.impl.ui.resources.HighlightDrawerResources#font.family=SansSerif-aa
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.impl.ui.resources", "HighlightDrawerResources", "font.family"));
		// com.amazon.ebook.booklet.reader.impl.ui.resources.UIResources#annotation.decoration.public.note.marker.text=@
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.impl.ui.resources", "UIResources", "annotation.decoration.public.note.marker.text"));
		// com.amazon.ebook.booklet.reader.impl.ui.resources.UIResources#image.icon.firstOpen.img=tipsAndTricksEN.gif
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.impl.ui.resources", "UIResources", "image.icon.firstOpen.img"));
		// com.amazon.ebook.booklet.reader.plugin.pagenumbers.resources.SidecarPageNumberProviderResources#pagelabel.range.list.delimiter=,
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.pagenumbers.resources", "SidecarPageNumberProviderResources", "pagelabel.range.list.delimiter"));
		// com.amazon.ebook.booklet.reader.plugin.search.resources.SearchResultsPaneResources#snippet.location.font.family=sans
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.search.resources", "SearchResultsPaneResources", "snippet.location.font.family"));
		// com.amazon.ebook.booklet.reader.plugin.sharing.resources.SharingResources#error.dialog.success.message=
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.sharing.resources", "SharingResources", "error.dialog.success.message"));
		// com.amazon.ebook.booklet.reader.plugin.systemcards.resources.SystemsCardsResources#dictionarycard.def.definition.font=sansserif
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.systemcards.resources", "SystemsCardsResources", "dictionarycard.def.definition.font"));
		// com.amazon.ebook.booklet.reader.plugin.systemcards.resources.SystemsCardsResources#dictionarycard.def.markupBegin=
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.systemcards.resources", "SystemsCardsResources", "dictionarycard.def.markupBegin"));
		// com.amazon.ebook.booklet.reader.plugin.systemcards.resources.SystemsCardsResources#dictionarycard.def.markupEnd=
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.systemcards.resources", "SystemsCardsResources", "dictionarycard.def.markupEnd"));
		// com.amazon.ebook.booklet.reader.plugin.systemcards.resources.SystemsCardsResources#dictionarycard.word.allMarkupBegin=<body>
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.systemcards.resources", "SystemsCardsResources", "dictionarycard.word.allMarkupBegin"));
		// com.amazon.ebook.booklet.reader.plugin.systemcards.resources.SystemsCardsResources#dictionarycard.word.allMarkupEnd=</body>
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.systemcards.resources", "SystemsCardsResources", "dictionarycard.word.allMarkupEnd"));
		// com.amazon.ebook.booklet.reader.plugin.systemcards.resources.SystemsCardsResources#dictionarycard.word.markupBegin=<H6>
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.systemcards.resources", "SystemsCardsResources", "dictionarycard.word.markupBegin"));
		// com.amazon.ebook.booklet.reader.plugin.systemcards.resources.SystemsCardsResources#dictionarycard.word.markupEnd=&nbsp;</H6>
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.systemcards.resources", "SystemsCardsResources", "dictionarycard.word.markupEnd"));
		// com.amazon.ebook.booklet.reader.plugin.tts.resources.TTSResources#tts.volume.louder.img=resources/louder.png
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.tts.resources", "TTSResources", "tts.volume.louder.img"));
		// com.amazon.ebook.booklet.reader.plugin.tts.resources.TTSResources#tts.volume.off.img=resources/off.png
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.tts.resources", "TTSResources", "tts.volume.off.img"));
		// com.amazon.ebook.booklet.reader.plugin.tts.resources.TTSResources#tts.volume.pause.img=resources/pause.png
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.tts.resources", "TTSResources", "tts.volume.pause.img"));
		// com.amazon.ebook.booklet.reader.plugin.tts.resources.TTSResources#tts.volume.play.img=resources/play.png
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.tts.resources", "TTSResources", "tts.volume.play.img"));
		// com.amazon.ebook.booklet.reader.plugin.tts.resources.TTSResources#tts.volume.quieter.img=resources/quieter.png
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.plugin.tts.resources", "TTSResources", "tts.volume.quieter.img"));
		// com.amazon.ebook.booklet.reader.resources.DetailViewFactoryResources#pdf.title.template={0}.pdf
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.resources", "DetailViewFactoryResources", "pdf.title.template"));
		// com.amazon.ebook.booklet.reader.resources.ReaderResources#font.menu.family=serif
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.resources", "ReaderResources", "font.menu.family"));
		// com.amazon.ebook.booklet.reader.resources.ReaderResources#font.menu.typeface.altlist[0]=alternate1
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.resources", "ReaderResources", "font.menu.typeface.altlist[0]"));
		// com.amazon.ebook.booklet.reader.resources.ReaderResources#font.menu.typeface.altlist[1]=alternate2
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.resources", "ReaderResources", "font.menu.typeface.altlist[1]"));
		// com.amazon.ebook.booklet.reader.resources.ReaderResources#font.menu.typeface.altlist[2]=alternate3
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.resources", "ReaderResources", "font.menu.typeface.altlist[2]"));
		// com.amazon.ebook.booklet.reader.resources.ReaderResources#font.menu.typeface.list[0]=serif
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.resources", "ReaderResources", "font.menu.typeface.list[0]"));
		// com.amazon.ebook.booklet.reader.resources.ReaderResources#font.menu.typeface.list[1]=condensed
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.resources", "ReaderResources", "font.menu.typeface.list[1]"));
		// com.amazon.ebook.booklet.reader.resources.ReaderResources#font.menu.typeface.list[2]=Sans
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.resources", "ReaderResources", "font.menu.typeface.list[2]"));
		// com.amazon.ebook.booklet.reader.resources.ReaderResources#reader.content.font_family=serif
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.resources", "ReaderResources", "reader.content.font_family"));
		// com.amazon.ebook.booklet.reader.resources.ReaderResources#reader.state.global=reader.glb
		set.add(new ResourceEntry("com.amazon.ebook.booklet.reader.resources", "ReaderResources", "reader.state.global"));
		// com.amazon.ebook.util.resources.TimeZoneUtilResources#zone.display.empty.pattern={0}
		set.add(new ResourceEntry("com.amazon.ebook.util.resources", "TimeZoneUtilResources", "zone.display.empty.pattern"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#image.audible.default=images/audible_default_image.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "image.audible.default"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#image.mp3.default=images/mp3_default_image.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "image.mp3.default"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#nextchapter.button.image.disabled=images/next_chapter_disabled.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "nextchapter.button.image.disabled"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#nextchapter.button.image=images/next_chapter.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "nextchapter.button.image"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#off.button.image=images/off.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "off.button.image"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#pause.button.image=images/pause.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "pause.button.image"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#play.button.image=images/play.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "play.button.image"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#prevchapter.button.image.disabled=images/previous_chapter_disabled.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "prevchapter.button.image.disabled"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#prevchapter.button.image=images/previous_chapter.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "prevchapter.button.image"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#seekbackward.button.image.disabled=images/seek_backward_disabled.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "seekbackward.button.image.disabled"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#seekbackward.button.image=images/seek_backward.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "seekbackward.button.image"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#seekforward.button.image.disabled=images/seek_forward_disabled.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "seekforward.button.image.disabled"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#seekforward.button.image=images/seek_forward.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "seekforward.button.image"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#timesection.timeremaining=-{0}
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "timesection.timeremaining"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#undonavigation.button.image.disabled=images/undo_navigation_disabled.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "undonavigation.button.image.disabled"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#undonavigation.button.image=images/undo_navigation.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "undonavigation.button.image"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#volumedecrease.button.image=images/volume_decrease.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "volumedecrease.button.image"));
		// com.amazon.kindle.audibleplayer.gui.resources.AudibleUIResources#volumeincrease.button.image=images/volume_increase.png
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.gui.resources", "AudibleUIResources", "volumeincrease.button.image"));
		// com.amazon.kindle.audibleplayer.resources.DetailViewFactoryResources#length.template={0}
		set.add(new ResourceEntry("com.amazon.kindle.audibleplayer.resources", "DetailViewFactoryResources", "length.template"));
		// com.amazon.kindle.booklet.ad.resources.AdResources#header=
		set.add(new ResourceEntry("com.amazon.kindle.booklet.ad.resources", "AdResources", "header"));
		// com.amazon.kindle.control.resources.ActionResources#buy.book.action=app\\://com.lab126.store?action\\=buy&asin\\={0}
		set.add(new ResourceEntry("com.amazon.kindle.control.resources", "ActionResources", "buy.book.action"));
		// com.amazon.kindle.control.resources.ActionResources#home.view.request.action=app\\://com.lab126.booklet.home?viewRequest\\={0}
		set.add(new ResourceEntry("com.amazon.kindle.control.resources", "ActionResources", "home.view.request.action"));
		// com.amazon.kindle.control.resources.ActionResources#reader.goto.action={0}?plugin\\=Action&action\\=goto
		set.add(new ResourceEntry("com.amazon.kindle.control.resources", "ActionResources", "reader.goto.action"));
		// com.amazon.kindle.control.resources.ActionResources#reader.notesandmarks.action={0}?plugin\\=Action&action\\=annotation
		set.add(new ResourceEntry("com.amazon.kindle.control.resources", "ActionResources", "reader.notesandmarks.action"));
		// com.amazon.kindle.control.resources.ActionResources#reader.search.action={0}?plugin\\=Action&action\\=search
		set.add(new ResourceEntry("com.amazon.kindle.control.resources", "ActionResources", "reader.search.action"));
		// com.amazon.kindle.control.resources.ActionResources#settings.view.request.action=app\\://com.lab126.booklet.settings?viewRequest\\={0}
		set.add(new ResourceEntry("com.amazon.kindle.control.resources", "ActionResources", "settings.view.request.action"));
		// com.amazon.kindle.control.resources.ActionResources#share.book.action=http\\://www.amazon.com/kindle/sharethisbook?cdekey\\={0}
		set.add(new ResourceEntry("com.amazon.kindle.control.resources", "ActionResources", "share.book.action"));
		// com.amazon.kindle.control.resources.ActionResources#store.description.action=app\\://com.lab126.store?action\\=detail&asin\\={0}
		set.add(new ResourceEntry("com.amazon.kindle.control.resources", "ActionResources", "store.description.action"));
		// com.amazon.kindle.control.resources.ActionResources#store.releasenotes.action=app\\://com.lab126.store?action\\=releaseNotes&asin\\={0}
		set.add(new ResourceEntry("com.amazon.kindle.control.resources", "ActionResources", "store.releasenotes.action"));
		// com.amazon.kindle.control.resources.DialogResources#addcollection.keyboard=\\:Abc\\:
		set.add(new ResourceEntry("com.amazon.kindle.control.resources", "DialogResources", "addcollection.keyboard"));
		// com.amazon.kindle.control.resources.UtilResources#download.status.default=
		set.add(new ResourceEntry("com.amazon.kindle.control.resources", "UtilResources", "download.status.default"));
		// com.amazon.kindle.home.resources.AddToCollectionsResources#addcollection.init.text=
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "AddToCollectionsResources", "addcollection.init.text"));
		// com.amazon.kindle.home.resources.AddToCollectionsResources#default.sort.option=title
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "AddToCollectionsResources", "default.sort.option"));
		// com.amazon.kindle.home.resources.AddToCollectionsResources#sort.options[0]=mostrecent
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "AddToCollectionsResources", "sort.options[0]"));
		// com.amazon.kindle.home.resources.AddToCollectionsResources#sort.options[1]=title
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "AddToCollectionsResources", "sort.options[1]"));
		// com.amazon.kindle.home.resources.ArchiveResources#default.sort.option=title
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "ArchiveResources", "default.sort.option"));
		// com.amazon.kindle.home.resources.ArchiveResources#sort.options[0]=title
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "ArchiveResources", "sort.options[0]"));
		// com.amazon.kindle.home.resources.ArchiveResources#sort.options[1]=author
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "ArchiveResources", "sort.options[1]"));
		// com.amazon.kindle.home.resources.BackIssuesResources#default.sort.option=mostrecent
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "BackIssuesResources", "default.sort.option"));
		// com.amazon.kindle.home.resources.BackIssuesResources#sort.options[0]=mostrecent
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "BackIssuesResources", "sort.options[0]"));
		// com.amazon.kindle.home.resources.BackIssuesResources#sort.options[1]=title
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "BackIssuesResources", "sort.options[1]"));
		// com.amazon.kindle.home.resources.BehaviorResources#error.message.template="{0}"\\n\\n{1}
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "BehaviorResources", "error.message.template"));
		// com.amazon.kindle.home.resources.BrowseResources#default.sort.option=mostrecent
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "BrowseResources", "default.sort.option"));
		// com.amazon.kindle.home.resources.BrowseResources#sort.options[0]=mostrecent
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "BrowseResources", "sort.options[0]"));
		// com.amazon.kindle.home.resources.BrowseResources#sort.options[1]=title
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "BrowseResources", "sort.options[1]"));
		// com.amazon.kindle.home.resources.BrowseResources#sort.options[2]=author
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "BrowseResources", "sort.options[2]"));
		// com.amazon.kindle.home.resources.BrowseResources#sort.options[3]=collection
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "BrowseResources", "sort.options[3]"));
		// com.amazon.kindle.home.resources.CollectionResources#default.sort.option=mostrecent
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "CollectionResources", "default.sort.option"));
		// com.amazon.kindle.home.resources.CollectionResources#sort.options[0]=mostrecent
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "CollectionResources", "sort.options[0]"));
		// com.amazon.kindle.home.resources.CollectionResources#sort.options[1]=title
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "CollectionResources", "sort.options[1]"));
		// com.amazon.kindle.home.resources.CollectionResources#sort.options[2]=author
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "CollectionResources", "sort.options[2]"));
		// com.amazon.kindle.home.resources.ItemsToCollectionResources#contentCount.template=({0,number,integer})
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "ItemsToCollectionResources", "contentCount.template"));
		// com.amazon.kindle.home.resources.ItemsToCollectionResources#default.sort.option=title
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "ItemsToCollectionResources", "default.sort.option"));
		// com.amazon.kindle.home.resources.ItemsToCollectionResources#sort.options[0]=mostrecent
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "ItemsToCollectionResources", "sort.options[0]"));
		// com.amazon.kindle.home.resources.ItemsToCollectionResources#sort.options[1]=title
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "ItemsToCollectionResources", "sort.options[1]"));
		// com.amazon.kindle.home.resources.ItemsToCollectionResources#sort.options[2]=author
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "ItemsToCollectionResources", "sort.options[2]"));
		// com.amazon.kindle.home.resources.SearchResultsResources#default.sort.option=numhits
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "SearchResultsResources", "default.sort.option"));
		// com.amazon.kindle.home.resources.SearchResultsResources#header.template="{0}"
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "SearchResultsResources", "header.template"));
		// com.amazon.kindle.home.resources.SearchResultsResources#sort.options[0]=numhits
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "SearchResultsResources", "sort.options[0]"));
		// com.amazon.kindle.home.resources.SearchResultsResources#sort.options[1]=mostrecent
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "SearchResultsResources", "sort.options[1]"));
		// com.amazon.kindle.home.resources.SearchResultsResources#sort.options[2]=title
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "SearchResultsResources", "sort.options[2]"));
		// com.amazon.kindle.home.resources.SearchResultsResources#sort.options[3]=author
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "SearchResultsResources", "sort.options[3]"));
		// com.amazon.kindle.home.resources.UnindexedResources#default.sort.option=title
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "UnindexedResources", "default.sort.option"));
		// com.amazon.kindle.home.resources.UnindexedResources#sort.options[0]=title
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "UnindexedResources", "sort.options[0]"));
		// com.amazon.kindle.home.resources.UnindexedResources#sort.options[1]=author
		set.add(new ResourceEntry("com.amazon.kindle.home.resources", "UnindexedResources", "sort.options[1]"));
		// com.amazon.kindle.kindlet.internal.Kindlet#dev.kindlet.cert.result.filename.extension=.azw
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal", "Kindlet", "dev.kindlet.cert.result.filename.extension"));
		// com.amazon.kindle.kindlet.internal.Kindlet#dev.kindlet.cert.result.filename.prefix=Test Kindle Installation Result
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal", "Kindlet", "dev.kindlet.cert.result.filename.prefix"));
		// com.amazon.kindle.kindlet.internal.Kindlet#dev.kindlet.keystore.file=/var/local/java/keystore/developer.keystore
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal", "Kindlet", "dev.kindlet.keystore.file"));
		// com.amazon.kindle.kindlet.internal.Kindlet#developer.application.directory=/mnt/us/developer
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal", "Kindlet", "developer.application.directory"));
		// com.amazon.kindle.kindlet.internal.Kindlet#kindlet.application.directory=/mnt/us/.active-content-data
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal", "Kindlet", "kindlet.application.directory"));
		// com.amazon.kindle.kindlet.internal.Kindlet#kindlet.application.metadata.subdirectory=metadata
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal", "Kindlet", "kindlet.application.metadata.subdirectory"));
		// com.amazon.kindle.kindlet.internal.Kindlet#kindlet.application.temp.subdirectory=temp
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal", "Kindlet", "kindlet.application.temp.subdirectory"));
		// com.amazon.kindle.kindlet.internal.Kindlet#kindlet.application.work.subdirectory=work
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal", "Kindlet", "kindlet.application.work.subdirectory"));
		// com.amazon.kindle.kindlet.internal.Kindlet#kindlet.sdk-lib.directory=/opt/amazon/ebook/sdk/lib
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal", "Kindlet", "kindlet.sdk-lib.directory"));
		// com.amazon.kindle.kindlet.internal.home.KindletDetailViewResources#browser.app.uri.pattern=app\\://com.lab126.browser?view\\={0}
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal.home", "KindletDetailViewResources", "browser.app.uri.pattern"));
		// com.amazon.kindle.kindlet.internal.home.KindletDetailViewResources#support.link.label=Support
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal.home", "KindletDetailViewResources", "support.link.label"));
		// com.amazon.kindle.kindlet.internal.home.KindletDetailViewResources#version.area.pattern=Version {0}
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal.home", "KindletDetailViewResources", "version.area.pattern"));
		// com.amazon.kindle.kindlet.internal.resources.KindletBookletResources#appmgrd.path=com.lab126.appmgrd
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal.resources", "KindletBookletResources", "appmgrd.path"));
		// com.amazon.kindle.kindlet.internal.resources.KindletBookletResources#appmgrd.start.cmd=start
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal.resources", "KindletBookletResources", "appmgrd.start.cmd"));
		// com.amazon.kindle.kindlet.internal.resources.KindletBookletResources#kindlet.restart.framework.cancel.button=cancel
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal.resources", "KindletBookletResources", "kindlet.restart.framework.cancel.button"));
		// com.amazon.kindle.kindlet.internal.resources.KindletBookletResources#kindlet.title.subtitle.format={0}\\: {1}
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal.resources", "KindletBookletResources", "kindlet.title.subtitle.format"));
		// com.amazon.kindle.kindlet.internal.resources.KindletBookletResources#store.detail.uri=app\\://com.lab126.store?action\\=detail&asin\\={0}
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal.resources", "KindletBookletResources", "store.detail.uri"));
		// com.amazon.kindle.kindlet.internal.resources.KindletBookletResources#store.open.uri=app\\://com.lab126.store
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal.resources", "KindletBookletResources", "store.open.uri"));
		// com.amazon.kindle.kindlet.internal.resources.KindletBookletResources#store.search.uri=app\\://com.lab126.store?action\\=search&query\\={0}
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.internal.resources", "KindletBookletResources", "store.search.uri"));
		// com.amazon.kindle.kindlet.ui.resources.UIResources#font.family.sansserif=Sans Serif
		set.add(new ResourceEntry("com.amazon.kindle.kindlet.ui.resources", "UIResources", "font.family.sansserif"));
		// com.amazon.kindle.resources.SpeechOutputResources#voice.availablevoices[0]=Tom
		set.add(new ResourceEntry("com.amazon.kindle.resources", "SpeechOutputResources", "voice.availablevoices[0]"));
		// com.amazon.kindle.resources.SpeechOutputResources#voice.availablevoices[1]=Samantha
		set.add(new ResourceEntry("com.amazon.kindle.resources", "SpeechOutputResources", "voice.availablevoices[1]"));
		// com.amazon.kindle.resources.SpeechOutputResources#voice.female=Samantha
		set.add(new ResourceEntry("com.amazon.kindle.resources", "SpeechOutputResources", "voice.female"));
		// com.amazon.kindle.resources.SpeechOutputResources#voice.male=Tom
		set.add(new ResourceEntry("com.amazon.kindle.resources", "SpeechOutputResources", "voice.male"));
		// com.amazon.kindle.resources.StatusBarResources#activity.blank.icon=DownloadingIcon_Blank.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "activity.blank.icon"));
		// com.amazon.kindle.resources.StatusBarResources#activity.sequence[0]=Download_Icon1.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "activity.sequence[0]"));
		// com.amazon.kindle.resources.StatusBarResources#activity.sequence[1]=Download_Icon2.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "activity.sequence[1]"));
		// com.amazon.kindle.resources.StatusBarResources#activity.sequence[2]=Download_Icon3.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "activity.sequence[2]"));
		// com.amazon.kindle.resources.StatusBarResources#activity.sequence[3]=Download_Icon4.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "activity.sequence[3]"));
		// com.amazon.kindle.resources.StatusBarResources#activity.sequence[4]=Download_Icon5.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "activity.sequence[4]"));
		// com.amazon.kindle.resources.StatusBarResources#activity.sequence[5]=Download_Icon6.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "activity.sequence[5]"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.blank=3G_Blank.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.blank"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.fallback.active.cdma=Home_Icon_1X2.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.fallback.active.cdma"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.fallback.active.gsm=Home_Icon_edge2.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.fallback.active.gsm"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.fallback.inactive.cdma=Home_Icon_1X1.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.fallback.inactive.cdma"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.fallback.inactive.gsm=Home_Icon_edge1.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.fallback.inactive.gsm"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.fallback2.active.cdma=Home_Icon_1X2.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.fallback2.active.cdma"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.fallback2.active.gsm=Home_Icon_gprs2.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.fallback2.active.gsm"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.fallback2.inactive.cdma=Home_Icon_1X1.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.fallback2.inactive.cdma"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.fallback2.inactive.gsm=Home_Icon_gprs1.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.fallback2.inactive.gsm"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.fullspeed.active.cdma=Home_Icon_3G2.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.fullspeed.active.cdma"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.fullspeed.active.gsm=Home_Icon_3G2.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.fullspeed.active.gsm"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.fullspeed.inactive.cdma=Home_Icon_3G1.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.fullspeed.inactive.cdma"));
		// com.amazon.kindle.resources.StatusBarResources#asset.wan.icon.fullspeed.inactive.gsm=Home_Icon_3G1.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "asset.wan.icon.fullspeed.inactive.gsm"));
		// com.amazon.kindle.resources.StatusBarResources#battery.fill.tile=Home_Icon_BatteryLevelGraphic.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "battery.fill.tile"));
		// com.amazon.kindle.resources.StatusBarResources#charged.battery.icon=Home_Icon_BatteryCharging.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "charged.battery.icon"));
		// com.amazon.kindle.resources.StatusBarResources#download.bar=Status_DownloadingBar.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "download.bar"));
		// com.amazon.kindle.resources.StatusBarResources#empty.battery.icon=Home_Icon_Battery.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "empty.battery.icon"));
		// com.amazon.kindle.resources.StatusBarResources#low.battery.icon=Home_Icon_BatteryWarning.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "low.battery.icon"));
		// com.amazon.kindle.resources.StatusBarResources#system.bar.cap.left=TopBar_LeftCap.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "system.bar.cap.left"));
		// com.amazon.kindle.resources.StatusBarResources#system.bar.cap.right=TopBar_RightCap.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "system.bar.cap.right"));
		// com.amazon.kindle.resources.StatusBarResources#system.bar.texture=TopBar_MidTexture.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "system.bar.texture"));
		// com.amazon.kindle.resources.StatusBarResources#unknown.battery.icon=Home_Icon_QuestionMark.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "unknown.battery.icon"));
		// com.amazon.kindle.resources.StatusBarResources#wan.off.icon=Home_Icon_Signal0.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "wan.off.icon"));
		// com.amazon.kindle.resources.StatusBarResources#wan.on.icon.0=Home_Icon_Signal1.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "wan.on.icon.0"));
		// com.amazon.kindle.resources.StatusBarResources#wan.on.icon.1=Home_Icon_Signal2.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "wan.on.icon.1"));
		// com.amazon.kindle.resources.StatusBarResources#wan.on.icon.2=Home_Icon_Signal3.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "wan.on.icon.2"));
		// com.amazon.kindle.resources.StatusBarResources#wan.on.icon.3=Home_Icon_Signal4.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "wan.on.icon.3"));
		// com.amazon.kindle.resources.StatusBarResources#wan.on.icon.4=Home_Icon_Signal5.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "wan.on.icon.4"));
		// com.amazon.kindle.resources.StatusBarResources#wan.on.icon.5=Home_Icon_Signal6.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "wan.on.icon.5"));
		// com.amazon.kindle.resources.StatusBarResources#wan.signal.unknown.icon.dim=Home_Icon_SignalUnknown.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "wan.signal.unknown.icon.dim"));
		// com.amazon.kindle.resources.StatusBarResources#wan.signal.unknown.icon=Home_Icon_SignalUnknown.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "wan.signal.unknown.icon"));
		// com.amazon.kindle.resources.StatusBarResources#wifi.connected.icon=icon_wifi.gif
		set.add(new ResourceEntry("com.amazon.kindle.resources", "StatusBarResources", "wifi.connected.icon"));
		// com.amazon.kindle.resources.WebServicesResources#logsupload.http.header.key.anonymous=X-Anonymous-Tag
		set.add(new ResourceEntry("com.amazon.kindle.resources", "WebServicesResources", "logsupload.http.header.key.anonymous"));
		// com.amazon.kindle.resources.WebServicesResources#logsupload.http.header.key.device.dsn=X-DSN
		set.add(new ResourceEntry("com.amazon.kindle.resources", "WebServicesResources", "logsupload.http.header.key.device.dsn"));
		// com.amazon.kindle.resources.WebServicesResources#logsupload.http.header.key.device.fwversion=X-DeviceFirmwareVersion
		set.add(new ResourceEntry("com.amazon.kindle.resources", "WebServicesResources", "logsupload.http.header.key.device.fwversion"));
		// com.amazon.kindle.resources.WebServicesResources#logsupload.http.header.key.device.type=X-DeviceType
		set.add(new ResourceEntry("com.amazon.kindle.resources", "WebServicesResources", "logsupload.http.header.key.device.type"));
		// com.amazon.kindle.resources.WebServicesResources#logsupload.showlog.command=/usr/bin/showlog -s
		set.add(new ResourceEntry("com.amazon.kindle.resources", "WebServicesResources", "logsupload.showlog.command"));
		// com.amazon.kindle.resources.WebServicesResources#logsupload.showlog.param.fsn=--fsn
		set.add(new ResourceEntry("com.amazon.kindle.resources", "WebServicesResources", "logsupload.showlog.param.fsn"));
		// com.amazon.kindle.resources.WebServicesResources#logsupload.showlog.param.full=--full
		set.add(new ResourceEntry("com.amazon.kindle.resources", "WebServicesResources", "logsupload.showlog.param.full"));
		// com.amazon.kindle.settings.resources.DeviceOptionsResources#devicename.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "DeviceOptionsResources", "devicename.property.name"));
		// com.amazon.kindle.settings.resources.DeviceOptionsResources#devicetime.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "DeviceOptionsResources", "devicetime.property.name"));
		// com.amazon.kindle.settings.resources.DeviceOptionsResources#email.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "DeviceOptionsResources", "email.property.name"));
		// com.amazon.kindle.settings.resources.DeviceOptionsResources#passcode.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "DeviceOptionsResources", "passcode.property.name"));
		// com.amazon.kindle.settings.resources.DeviceOptionsResources#personalinfo.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "DeviceOptionsResources", "personalinfo.property.name"));
		// com.amazon.kindle.settings.resources.DialogResources#devicetime.automatic.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "DialogResources", "devicetime.automatic.property.name"));
		// com.amazon.kindle.settings.resources.DictionaryResources#dictionarylanguages.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "DictionaryResources", "dictionarylanguages.property.name"));
		// com.amazon.kindle.settings.resources.InternationalResources#keyboard.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "InternationalResources", "keyboard.property.name"));
		// com.amazon.kindle.settings.resources.InternationalResources#language.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "InternationalResources", "language.property.name"));
		// com.amazon.kindle.settings.resources.LegalResources#legal.image.key=<javaimg>
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "LegalResources", "legal.image.key"));
		// com.amazon.kindle.settings.resources.ReadingOptionsResources#annotations.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "ReadingOptionsResources", "annotations.property.name"));
		// com.amazon.kindle.settings.resources.ReadingOptionsResources#highlights.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "ReadingOptionsResources", "highlights.property.name"));
		// com.amazon.kindle.settings.resources.ReadingOptionsResources#pagerefresh.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "ReadingOptionsResources", "pagerefresh.property.name"));
		// com.amazon.kindle.settings.resources.ReadingOptionsResources#publicnotes.description.value.default=kindle.amazon.com
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "ReadingOptionsResources", "publicnotes.description.value.default"));
		// com.amazon.kindle.settings.resources.ReadingOptionsResources#publicnotes.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "ReadingOptionsResources", "publicnotes.property.name"));
		// com.amazon.kindle.settings.resources.SettingsResources#international.name.label=International
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "SettingsResources", "international.name.label"));
		// com.amazon.kindle.settings.resources.SettingsResources#pagerefresh.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "SettingsResources", "pagerefresh.property.name"));
		// com.amazon.kindle.settings.resources.SettingsResources#passcode.property.name=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "SettingsResources", "passcode.property.name"));
		// com.amazon.kindle.settings.resources.SettingsResources#reg.property.name.no=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "SettingsResources", "reg.property.name.no"));
		// com.amazon.kindle.settings.resources.SettingsResources#wifi.property.name.connected.no=
		set.add(new ResourceEntry("com.amazon.kindle.settings.resources", "SettingsResources", "wifi.property.name.connected.no"));		
		return set;
	}
	
	public static boolean containsEntry(String packageName, String className, String entryName) {
		return BLACKLISTED_ENTRIES.contains(new ResourceEntry(packageName, className, entryName));
	}
	
	public static boolean containsBundle(KTResourceBundle bundle) {
		String canonicalName = bundle.descriptor.toString();
		for (String blacklist: BLACKLISTED_CLASSNAMES) {
			if (blacklist.endsWith("$")) {
				if (canonicalName.equals(blacklist.substring(0, blacklist.length()-1))) {
					System.out.println(canonicalName+" is blacklisted for translation ("+blacklist+").");
					return true;
				}
			}
			if (canonicalName.startsWith(blacklist)) {
				System.out.println(canonicalName+" is blacklisted for translation ("+blacklist+").");
				return true;
			}
		}
		return false;
	}

}
