package ixtab.ktlocale;

import ixtab.ktlocale.ShellCommand.Result;
import ixtab.ktlocale.util.DirectoryFinder;

import java.io.File;
import java.io.IOException;

public class JSResourceTool {

	private static final String DEFAULT_EXECUTABLE = "python";
	private static final String DEFAULT_SCRIPT_PARENT_DIR = "tool";
	private static final String DEFAULT_SCRIPT_DIR = "js_resources";
	public static final String DEFAULT_COMMAND = determineDefaultCommand();
	private final String executable;
	
	private static String determineDefaultCommand() {
		try {
			File base = DirectoryFinder.determineRootDirectory();
			while (base != null) {
				if (base.getName().equalsIgnoreCase(DEFAULT_SCRIPT_PARENT_DIR)) {
					break;
				}
				base = base.getParentFile();
			}
			if (base == null) {
				return null;
			}
			File scriptDir= findScriptDirIn(base);
			if (scriptDir != null) {
				return DEFAULT_EXECUTABLE + " " + scriptDir.getCanonicalPath();
			}
		} catch (Exception e) {
		}
		return null;
	}
	
	private static File findScriptDirIn(File base) {
		if (base.isDirectory()) {
			for (File candidate: base.listFiles()) {
				if (candidate.getName().equalsIgnoreCase(DEFAULT_SCRIPT_DIR)) {
					return candidate;
				}
			}
		}
		return null;
	}

	public static boolean canBeAutoConfigured() {
		return DEFAULT_COMMAND != null;
	}
	
	public JSResourceTool(String exec) {
		this.executable = exec == null ? DEFAULT_COMMAND : exec;
		try {
			Result r = ShellCommand.exec(this.executable + " --help");
			if (r.code != 0) {
				throw new Exception();
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("\""+this.executable+"\" does not seem to be a valid executable. Please specify the correct executable using the appropriate option.");
		}
	}

	public void compile(File fromDir, File toDir, String targetFromLocale) throws IOException { 
		String cmd =executable.toString();
		cmd += " compile";
		cmd += " --source-dir ";
		cmd += fromDir.getCanonicalPath();
		cmd += " --dest-dir ";
		cmd += toDir.getCanonicalPath();
		cmd += " --locale ";
		cmd += targetFromLocale;
		try {
//			System.err.println("EXECUTING: "+cmd);
			Result r = ShellCommand.exec(cmd);
			if (r.code != 0) {
				throw new Exception("Something went wrong running "+cmd+ ",error below "+r.err);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
