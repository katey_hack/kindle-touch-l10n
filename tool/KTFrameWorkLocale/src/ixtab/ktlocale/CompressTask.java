package ixtab.ktlocale;

import ixtab.ktlocale.resources.ResourceLoader;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.jar.Attributes;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;

import serp.bytecode.BCClass;

public class CompressTask implements Runnable {

	private final File sourceDirectory;
	private final String[] relevantLocales;
	private final File targetFile;
	
	public CompressTask(String sourceDirectory, String targetFile, boolean overwrite, String... locales) {
		this.sourceDirectory = new File(sourceDirectory);
		if (!this.sourceDirectory.exists() || !this.sourceDirectory.isDirectory()) {
			throw new IllegalArgumentException(sourceDirectory+" does not exist or is not a directory.");
		}
		MessageFormatResources.read(this.sourceDirectory);
		this.relevantLocales = locales;
		this.targetFile = new File(targetFile);
		if (this.targetFile.exists() && !overwrite) {
			throw new IllegalStateException("Not overwriting "+targetFile+". To force overwrite, use --force option.");
		}
		
		if (locales == null || locales.length == 0) {
			System.err.println("This functionality expects one or more locale strings, but you provided none. This is probably not what you wanted, and the result will be empty.");
		}
	}

	@Override
	public void run() {
		try {
			List<BundleDescriptor> bundles = BundleDescriptor.find(sourceDirectory, relevantLocales);
			JarOutputStream jarFile = openOutputFile();
			writeTo(jarFile, bundles);
			addOffButtons(jarFile);
			jarFile.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private JarOutputStream openOutputFile() throws IOException {
		FileOutputStream fos = new FileOutputStream(targetFile);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		return new JarOutputStream(bos, getManifest());
	}

	private Manifest getManifest() {
		Manifest m = new Manifest();
		m.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
		m.getMainAttributes().put(Attributes.Name.IMPLEMENTATION_VENDOR, "Kindle Touch localization volunteers");
		m.getMainAttributes().put(Attributes.Name.IMPLEMENTATION_URL, "https://www.transifex.net/projects/p/kindle-touch/");
		return m;
	}

	private void writeTo(JarOutputStream jarFile, List<BundleDescriptor> bundles) throws IOException {
		int count = 0;
		for (BundleDescriptor bundle: bundles) {
			count += writeTo(jarFile, bundle);
		}
		System.out.println("Wrote "+count+" resource bundles.");
	}

	private int writeTo(JarOutputStream jarFile, BundleDescriptor descriptor) throws IOException {
		KTResourceBundle bundle = KTResourceBundle.fromDescriptor(descriptor);
		
		if (bundle.getContents().length == 0) {
			return 0;
		}
		byte[] binary = getByteCodeFor(bundle);
		byte[] text = getTextFor(bundle);
		String extension = "properties";
		byte[] chosen = text;
		
		/* prefer .properties files both for debuggability
		 * and performance reasons.
		 */
		if (text == null ) { // || text.length > binary.length) {
			chosen = binary;
			extension = "class";
		}
		
		String entryName = getFullLogicalPath(bundle, extension);
        ZipEntry entry = new ZipEntry(entryName);
        jarFile.putNextEntry(entry);
        jarFile.write(chosen);
        jarFile.closeEntry();
        return 1;
	}

	private byte[] getByteCodeFor(KTResourceBundle bundle) throws IOException {
		BCClass bytecode = ListResourceBundleAssembler.assembleFrom(bundle);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bytecode.write(bos);
		bos.close();
		return bos.toByteArray();
	}

	/* this may return null because not every KTResourceBundle
	 * can be correctly represented as a .properties file
	 * (most notably not the ones with arrays).
	 */
	private byte[] getTextFor(KTResourceBundle bundle) {
		try {
			Properties props = bundle.toProperties();
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			props.store(bos,null);
			bos.close();
			return bos.toByteArray();
		} catch (Exception e) {
			return null;
		}
	}

	private String getFullLogicalPath(KTResourceBundle bundle, String extension) {
		String path = bundle.descriptor.packageName;
		path = path.replace('.', '/');
		path += '/' + bundle.descriptor.suggestClassName();
		path += "." + extension;
		return path;
	}

	private void addOffButtons(JarOutputStream jarFile) throws IOException {
		addOffButton(jarFile, "com/amazon/ebook/booklet/reader/plugin/tts/resources/off_ln.png", ResourceLoader.OFF_50x40);
		addOffButton(jarFile, "com/amazon/kindle/audibleplayer/gui/images/off_ln.png", ResourceLoader.OFF_70x40);
	}

	public void addOffButton(JarOutputStream jarFile, String outName,
			String resName) throws IOException {
		byte[] bytes = ResourceLoader.getBytes(resName);
        ZipEntry entry = new ZipEntry(outName);
        jarFile.putNextEntry(entry);
        jarFile.write(bytes);
        jarFile.closeEntry();
	}


}
