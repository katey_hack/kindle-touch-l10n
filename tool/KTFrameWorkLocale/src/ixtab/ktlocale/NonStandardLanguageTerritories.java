package ixtab.ktlocale;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class NonStandardLanguageTerritories {
	private static final Map<Locale, Locale> MAP = initMappings();
	private static Map<Locale, Locale> initMappings() {
		Map<Locale, Locale> map = new HashMap<Locale, Locale>();
		// non-qualified locale => qualified locale
		map.put(new Locale("eu"), new Locale("eu", "ES"));
		return map;
	}
	
	public static Locale get(Locale localeWithoutTerritory) {
		return MAP.get(localeWithoutTerritory);
	}
}
