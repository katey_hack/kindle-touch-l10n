package ixtab.ktlocale.modification;

import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;

public class EmptyValuesRemoverMapModificationStrategy implements
		MapModificationStrategy {

	@Override
	public void updateResourceBundleMap(String packageName, String className, SortedMap<String, Object> map) {
		Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> entry = it.next();
			if (entry.getValue().equals("")) {
				it.remove();
			}
		}
	}
}
