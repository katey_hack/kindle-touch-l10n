package ixtab.ktlocale.modification;

import ixtab.ktlocale.arrays.NDimensionalStringArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class ArrayJoinerMapModificationStrategy implements MapModificationStrategy {

	@Override
	public void updateResourceBundleMap(String packageName, String className, SortedMap<String, Object> map) {
		Map<String, List<String>> arrayEntries = findArrayEntries(map);
		for (Map.Entry<String, List<String>> entry: arrayEntries.entrySet()) {
			int dimension = findDimensionality(entry.getValue());
			NDimensionalStringArray array = NDimensionalStringArray.instantiate(dimension);
			for (String oldKey: entry.getValue()) {
				String spec = oldKey.substring(entry.getKey().length());
				array.setEntry(spec, (String) map.get(oldKey));
				map.remove(oldKey);
			}
			map.put(entry.getKey(), array.getObject());
		}
	}
	
	private Map<String, List<String>> findArrayEntries(
			SortedMap<String, Object> map) {
		Map<String, List<String>> arrayEntries = new TreeMap<String, List<String>>();
		for (String key: map.keySet()) {
			if (key.endsWith("]")) {
				String prefix = key.substring(0, key.indexOf("["));
				List<String> instances = arrayEntries.get(prefix);
				if (instances == null) {
					instances = new ArrayList<String>();
					arrayEntries.put(prefix, instances);
				}
				instances.add(key);
				if (!(map.get(key) instanceof String)) {
					throw new IllegalArgumentException(key+" is not a String");
				}
			}
		}
		return arrayEntries;
	}

	private int findDimensionality(List<String> values) {
		int dim = -1;
		for (String value: values) {
			char[] chars = value.toCharArray();
			int count = count('[', chars);
			if (count(']', chars) != count) {
				throw new IllegalArgumentException(value+" does not look like a valid array specification");
			}
			if (dim == -1) {
				dim = count;
			} else if (count != dim) {
				throw new IllegalArgumentException("Conflicting array sizes for "+value);
			}
		}
		if (dim > NDimensionalStringArray.MAX_SUPPORTED_DIMENSIONS) {
			throw new UnsupportedOperationException("The dimensionality ("+dim+") is larger than the greatest supported value ("+NDimensionalStringArray.MAX_SUPPORTED_DIMENSIONS+")");
		}
		return dim;
	}

	// there probably is a slightly better way with regexes, but hey,
	// it's not like this is time-critical or called billions of times;
	private int count(char needle, char[] haystack) {
		int c = 0;
		for (int i=0; i < haystack.length; ++i) {
			if (haystack[i] == needle) {
				++c;
			}
		}
		return c;
	}

}
