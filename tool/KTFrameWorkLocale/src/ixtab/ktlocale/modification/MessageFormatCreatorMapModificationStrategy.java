package ixtab.ktlocale.modification;

import ixtab.ktlocale.MessageFormatResources;

import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;

public class MessageFormatCreatorMapModificationStrategy implements
		MapModificationStrategy {

	@Override
	public void updateResourceBundleMap(String packageName, String className, SortedMap<String, Object> map) {
		Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> entry = it.next();
			if (MessageFormatResources.contains(packageName, className, entry.getKey())) {
				entry.setValue(new StringBuffer((String)entry.getValue()));
			}
		}
	}
}
