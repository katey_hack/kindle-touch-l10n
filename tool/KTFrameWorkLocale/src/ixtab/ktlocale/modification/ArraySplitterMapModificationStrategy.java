package ixtab.ktlocale.modification;


import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class ArraySplitterMapModificationStrategy implements
		MapModificationStrategy {

	@Override
	public void updateResourceBundleMap(String packageName, String className, SortedMap<String, Object> map) {
		Map<String, Object> splitted = new TreeMap<String, Object>();
		
		Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> entry = it.next();
			Object v = entry.getValue();
			if (isArray1(v)) {
				splitOneDimension(splitted, entry.getKey(), (String[]) v);
				it.remove();
			}
			else if (isArray2(v)) {
				splitTwoDimension(splitted, entry.getKey(), (String[][]) v);
				it.remove();
			}
			else if (isArray3(v)) {
				splitThreeDimension(splitted, entry.getKey(), (String[][][]) v);
				it.remove();
			}
		}
		map.putAll(splitted);
	}
	
	private void splitOneDimension(Map<String, Object> splitted, String key, String[] value) {
		for (int i=0; i < value.length; ++i) {
			splitted.put(key+"["+i+"]", value[i]);
		}
	}

	private void splitTwoDimension(Map<String, Object> splitted, String key, String[][] value) {
		for (int i=0; i < value.length; ++i) {
			if (value[i] == null) continue;
			for (int j=0; j < value[i].length; ++j) {
				splitted.put(key+"["+i+"]["+j+"]", value[i][j]);
			}
		}
	}

	private void splitThreeDimension(Map<String, Object> splitted, String key, String[][][] value) {
		for (int i=0; i < value.length; ++i) {
			if (value[i] == null) continue;
			for (int j=0; j < value[i].length; ++j) {
				if (value[j] == null) continue;
				for (int k=0; k < value[i][j].length; ++k) {
					splitted.put(key+"["+i+"]["+j+"]["+k+"]", value[i][j][k]);
				}
			}
		}
	}

	private boolean isArray1(Object o) {
		return String[].class.isAssignableFrom(o.getClass());
	}
	private boolean isArray2(Object o) {
		return String[][].class.isAssignableFrom(o.getClass());
	}
	private boolean isArray3(Object o) {
		return String[][][].class.isAssignableFrom(o.getClass());
	}

}
