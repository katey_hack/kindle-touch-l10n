package ixtab.ktlocale.modification;


import ixtab.ktlocale.KTLocalizationBlackList;

import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;

public class BlacklistedEntryRemoverMapModificationStrategy implements
		MapModificationStrategy {

	@Override
	public void updateResourceBundleMap(String packageName, String className, SortedMap<String, Object> map) {
		Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> entry = it.next();
			if (KTLocalizationBlackList.containsEntry(packageName, className, entry.getKey())) {
				//System.err.println("BLACKLISTED \""+entry.getValue() + "\" in "+packageName+"."+className+"#"+entry.getKey());
				it.remove();
			}
		}
	}
}
