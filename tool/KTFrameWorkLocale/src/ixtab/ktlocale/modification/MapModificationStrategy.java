package ixtab.ktlocale.modification;

import java.util.SortedMap;

public interface MapModificationStrategy {
	void updateResourceBundleMap(String packageName, String className, SortedMap<String, Object> map);
}
