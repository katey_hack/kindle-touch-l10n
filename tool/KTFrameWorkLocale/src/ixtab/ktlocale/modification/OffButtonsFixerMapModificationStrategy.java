package ixtab.ktlocale.modification;

import java.util.SortedMap;

public class OffButtonsFixerMapModificationStrategy implements
		MapModificationStrategy {

	private static final String PACKAGE1 = "com.amazon.ebook.booklet.reader.plugin.tts.resources";
	private static final String CLASS1 = "TTSResources";
	private static final String KEY1 = "tts.volume.off.img";
	private static final String VALUE1 = "resources/off_ln.png";

	private static final String PACKAGE2 = "com.amazon.kindle.audibleplayer.gui.resources";
	private static final String CLASS2 = "AudibleUIResources";
	private static final String KEY2 = "off.button.image";
	private static final String VALUE2 = "images/off_ln.png";

	@Override
	public void updateResourceBundleMap(String packageName, String className,
			SortedMap<String, Object> map) {
		if (packageName.equals(PACKAGE1) && className.equals(CLASS1)) {
			map.put(KEY1, VALUE1);
		}
		else if (packageName.equals(PACKAGE2) && className.equals(CLASS2)) {
			map.put(KEY2, VALUE2);
		}
	}
}
