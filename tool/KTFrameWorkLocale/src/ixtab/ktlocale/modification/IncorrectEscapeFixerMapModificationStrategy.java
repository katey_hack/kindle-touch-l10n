package ixtab.ktlocale.modification;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.regex.Pattern;

public class IncorrectEscapeFixerMapModificationStrategy implements
		MapModificationStrategy {
	
	private static class Fix {
		Pattern pattern;
		String replacement;

		public Fix(String pattern, String replacement) {
			super();
			this.pattern = Pattern.compile(pattern);
			this.replacement = replacement;
		}
		
	}
	
	private static final List<Fix> fixes = initializeFixes();

	private static List<Fix> initializeFixes() {
		List<Fix> list = new ArrayList<Fix>();

		/* these fixes are applied in order, so the output of one
		 * might serve as input for the next
		 */
		list.add(new Fix("(\\d)(\\\\+)#","$1#"));
		list.add(new Fix("(\\\\+):",":"));
		list.add(new Fix("(\\\\+)=","="));
		list.add(new Fix("(\\\\+)n","\n"));
		
		return list;
	}

	@Override
	public void updateResourceBundleMap(String packageName, String className, SortedMap<String, Object> map) {
		Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> entry = it.next();
			String value = (String) entry.getValue();
			String fixed = value;
			for (Fix fix: fixes) {
				fixed = fix.pattern.matcher(fixed).replaceAll(fix.replacement);
			}
	        if (!value.equals(fixed)) {
	        	//System.err.println(value +" => "+ fixed);
	        	entry.setValue(fixed);
	        }
		}
	}

}
