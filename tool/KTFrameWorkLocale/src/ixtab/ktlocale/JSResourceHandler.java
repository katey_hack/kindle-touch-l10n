package ixtab.ktlocale;

import ixtab.ktlocale.util.LocaleUtil;
import ixtab.ktlocale.util.TemporaryDirectory;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

public class JSResourceHandler {
	public static final String DIR_LOCALES = "locales";

	public static enum Type {
		WAF {
			@Override
			protected File getDirectory(Workspace ws) {
				return ws.wafDir;
			}
			
		}
		, PILLOW {
			@Override
			protected File getDirectory(Workspace ws) {
				return ws.pillowDir;
			}
			
		};
		
		protected File getDirectory(Workspace ws) {
			return null;
		}
		
	}

	public final String localeFrom;
	public final String localeToAsGiven;
	public final String localeToFullyQualified;

	private final Type jsType;
	private final Workspace workspace;
	private final File sourceDirectory;
	private final String jsToolCmd;

	public JSResourceHandler(String jsToolCmd, Type type, Workspace workspace, Locale source) {
		this.jsToolCmd = jsToolCmd;
		localeFrom = source.toString();
		localeToAsGiven = LocaleUtil.jsLocaleStringFromTransifexLocale(source, false);
		localeToFullyQualified = LocaleUtil.jsLocaleStringFromTransifexLocale(source, true);
		jsType = type;
		this.workspace = workspace;
		sourceDirectory = getLocaleSubDirectory();
	}

	private File getLocaleSubDirectory() {
		File typeDir = jsType.getDirectory(workspace);
		if (!typeDir.exists() || !typeDir.isDirectory()) {
			return null;
		}
		for (File candidate: typeDir.listFiles()) {
			if (candidate.getName().equals(localeFrom)) {
				return candidate;
			}
		}
		return null;
	}

	public TemporaryDirectory fillInstallFiles(Map<String, File> install) throws IOException {
		if (sourceDirectory == null) {
			//System.err.println("No files to translate for "+jsType+"/"+localeFrom);
			return null;
		}
		TemporaryDirectory temp = new TemporaryDirectory("kindle-loc_"+ localeToFullyQualified+"_"+jsType.toString()+"-");
		try {
			JSResourceTool js = new JSResourceTool(jsToolCmd);
			js.compile(jsType.getDirectory(workspace), temp, localeFrom);
			File sourceDirectory = findSourceDirectory(temp);
			fillInstallAndUninstallFiles(sourceDirectory, install);
		} catch (Throwable t) {
			t.printStackTrace();
			temp.deleteRecursively();
			return null;
		}
		//temp.listRecursively(System.err);
		return temp;
	}

	private File findSourceDirectory(TemporaryDirectory temp) {
		for (File candidate: temp.listFiles()) {
			if (candidate.getName().equals(DIR_LOCALES)) {
				return candidate;
			}
		}
		throw new IllegalStateException("Expected to find directory \""+DIR_LOCALES+" in directory "+temp);
	}
	
	private void fillInstallAndUninstallFiles(File base,
			Map<String, File> install) throws IOException {
		Map<String, File> newlyInstalled = new TreeMap<String, File>();
		for (File considered: base.listFiles()) {
			populateInstallFiles(base, considered, newlyInstalled);
		}
		for (Map.Entry<String, File> entry: newlyInstalled.entrySet()) {
			String fileName = entry.getKey();
			String kindleName = getKindleFileName(fileName);
			// debugging...			
//			System.err.print(fileName + " => ");
//			System.err.println(kindleName);
			
			if (kindleName.equals(fileName)) {
				continue;
			}
			
			// more debugging...			
//			System.err.print(fileName + " => ");
//			System.err.println(kindleName);
			
			if (!entry.getValue().isDirectory()) {
				install.put(kindleName, entry.getValue());
			}
		}
	}

	private String getKindleFileName(String key) {
		/* There is no reasonable way to refactor these into the enums,
		 * because they depend on instance variables (localeFrom/To).
		 */
		String from = null;
		String to = null;
		
		if (jsType == Type.WAF) {
			// e.g. /var/local/waf/adviewer/locales/de-de/scripts/resources.js
			from = "^/("+localeToAsGiven+")/(.*?)/(.*)";
			to = "/var/local/waf/$2/locales/"+localeToFullyQualified+"/$3";
		} else if (jsType == Type.PILLOW) {
			// e.g. /usr/share/webkit-1.0/pillow/strings/locales/de-de/search_bar_strings.js
			from = "^/("+localeToAsGiven+")/(.*?)/(.*)";
			to = "/usr/share/webkit-1.0/pillow/$2/locales/"+localeToFullyQualified+"/$3";
		}
		Pattern p = Pattern.compile(from);
		return p.matcher(key).replaceAll(to);
	}

	private void populateInstallFiles(File base, File current,
			Map<String, File> install) throws IOException {
		String localName = getRelativeName(base, current);
		if (current.isDirectory()) {
			for (File f: current.listFiles()) {
				populateInstallFiles(base, f, install);
			}
		}
		install.put(localName, current);
	}

	private String getRelativeName(File base, File current) throws IOException {
		String baseName = base.getCanonicalPath().replace(File.separatorChar, '/');
		String currentName = current.getCanonicalPath().replace(File.separatorChar, '/');
		if (!currentName.startsWith(baseName)) {
			throw new RuntimeException("Unexpected: " + currentName+" does not start with "+baseName);
		}
		return currentName.substring(baseName.length());
	}

}
