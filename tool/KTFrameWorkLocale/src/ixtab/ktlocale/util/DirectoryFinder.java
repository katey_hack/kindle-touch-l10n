package ixtab.ktlocale.util;

import java.io.File;
import java.net.URL;

public class DirectoryFinder {
	private static final String MARKER_FILENAME = ".IXTAB_MARKER_DO_NOT_REMOVE.txt";
	private static final String DEFAULT_DIRNAME = "glibc-locales";

	private DirectoryFinder() {};
	public static File determineGlibcLocaleDir() {
		/* This is a bit of a hack. Try to figure out, from the classloader,
		 * a potential directory.
		 */
		try {
			File base = determineRootDirectory();
			return verifiedLocaleSubdirectory(base);
		} catch (Throwable t) {
			return null;
		}
	}
	
	public static File determineRootDirectory() throws Exception {
		URL self = DirectoryFinder.class.getResource(DirectoryFinder.class.getSimpleName()+".class");
		File base = self == null ? null: determineBaseFromUrl(self.toString());
		return base;
	}
	
	private static File determineBaseFromUrl(String url) throws Exception {
		if (url.startsWith("jar:")) {
			int excl = url.lastIndexOf("!");
			String jarName = url.substring(4, excl);
			File jarFile = null;
			try {
				jarFile = new File(new URL(jarName).toURI());
			} catch (Throwable t) {
				jarFile = new File(jarName);
			}
			if (jarFile.exists()) {
				return jarFile.getParentFile();
			}
		} else if (url.startsWith("file:")) {
			String suffix = DirectoryFinder.class.getCanonicalName().replace('.', File.separatorChar);
			suffix += ".class";
			if (!url.endsWith(suffix)) {
				return null;
			}
			String base = url.replace(suffix, "");
			File codeDir = new File(new URL(base).toURI());
			if (!codeDir.isDirectory()) {
				return null;
			}
			return codeDir.getParentFile();
		}
		return null;
	}
	
	private static File verifiedLocaleSubdirectory(File base) {
		if (base == null) return null;
		for (File candidate: base.listFiles()) {
			if (candidate.isDirectory() && candidate.getName().equalsIgnoreCase(DEFAULT_DIRNAME)) {
				if (isLocaleSubdirectory(candidate)) {
					return candidate;
				}
			}
		}
		return null;
	}

	public static boolean isLocaleSubdirectory(File candidate) {
		if (candidate == null) return false;
		if (!candidate.isDirectory()) {
			return false;
		}
		for (String file: candidate.list()) {
			if (file.equalsIgnoreCase(MARKER_FILENAME)) {
				return true;
			}
		}
		return false;
	}

}
