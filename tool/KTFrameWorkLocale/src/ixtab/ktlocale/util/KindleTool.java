package ixtab.ktlocale.util;

import java.io.File;
import java.io.IOException;

import ixtab.ktlocale.ShellCommand;
import ixtab.ktlocale.ShellCommand.Result;

public class KindleTool {

	public static final String DEFAULT_EXECUTABLE = "kindletool";
	public static final String DEFAULT_ARGS = "create ota2 -d k5w -d k5g";
	private final String executable;
	
	public KindleTool(String exec) {
		this.executable = exec == null ? DEFAULT_EXECUTABLE : exec;
		try {
			Result r = ShellCommand.exec(this.executable);
			if (r.code != 0) {
				throw new Exception();
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("\""+this.executable+"\" is not a valid executable. Please specify the correct executable using the appopriate option.");
		}
	}

	public void pack(File source, File output) throws IOException {
		String cmd = executable+" "+DEFAULT_ARGS+" "+source.getCanonicalPath()+ " "+ output.getCanonicalPath();
		try {
			Result r = ShellCommand.exec(cmd);
			if (r.code != 0) {
				throw new Exception("Something went wrong running "+cmd+ ",error below "+r.err);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
