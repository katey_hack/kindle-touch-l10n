package ixtab.ktlocale.util;

import ixtab.ktlocale.cldr.CLDRLocale;

import java.util.Locale;

public class LocaleUtil {
	public static Locale localeFromString(String description) {
		int underscore = description.indexOf('_');
		if (underscore != -1) {
			String language = description.substring(0, underscore);
			String territory = description.substring(underscore+1);
			return new Locale(language, territory);
		}
		return new Locale(description);
	}
	
	public static String jsLocaleStringFromTransifexLocale(Locale source, boolean fullyqualify) {
		String ret = source.toString();
		if (fullyqualify) {
			ret = CLDRLocale.load(source)[CLDRLocale.LANGUAGE_AND_TERRITORY].locale.toString();
		}
		ret = ret.toLowerCase().replace("_", "-");
		return ret;

	}
}
