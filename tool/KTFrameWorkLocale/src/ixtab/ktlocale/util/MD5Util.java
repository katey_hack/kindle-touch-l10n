package ixtab.ktlocale.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {

	public static String getMD5Sum(File file) throws IOException {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			InputStream is = new BufferedInputStream(new FileInputStream(file));

			byte[] buffer = new byte[8192];
			int read = 0;
			while ((read = is.read(buffer)) > 0)
				md.update(buffer, 0, read);
			byte[] md5 = md.digest();

			BigInteger bi = new BigInteger(1, md5);
			String output = bi.toString(16).toLowerCase();
			while (output.length() < 32) {
				output = "0" + output;
			}
			return output;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

}
