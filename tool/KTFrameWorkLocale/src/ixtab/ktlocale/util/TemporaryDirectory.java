package ixtab.ktlocale.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

@SuppressWarnings("serial")
public class TemporaryDirectory extends File {

	public TemporaryDirectory(String prefix) throws IOException {
		super(getAbsoluteName(prefix));
	}

	private static String getAbsoluteName(String prefix) throws IOException {
		File tmp = File.createTempFile(prefix, null);
		tmp.delete();
		if (!tmp.mkdirs()) {
			throw new RuntimeException("Unable to create directory "+tmp);
		}
		return tmp.getCanonicalPath();
	}

	public boolean deleteRecursively() {
		deleteRecursively(this);
		return true;
	}

	private void deleteRecursively(File dir) {
		for (File f: dir.listFiles()) {
			if (f.isDirectory()) {
				deleteRecursively(f);
			} else {
				f.delete();
			}
		}
		dir.delete();
	}

	/*
	 * only for debugging.
	 */
	public void listRecursively(PrintStream out) {
		listRecursively(this, out);
	}

	/*
	 * only for debugging.
	 */
	private void listRecursively(File dir,
			PrintStream out) {
		out.println(dir);
		for (File f: dir.listFiles()) {
			if (f.isDirectory()) {
				listRecursively(f, out);
			} else {
				out.println(f);
			}
		}
	}

}
