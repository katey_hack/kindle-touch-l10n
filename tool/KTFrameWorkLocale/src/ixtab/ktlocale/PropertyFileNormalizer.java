package ixtab.ktlocale;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class PropertyFileNormalizer {
	// This is an artifact from refactoring code out ot ExtractTask.
	// In any case, it designates the comment line which should be removed.
	private static final int REMOVE_NTH_COMMENT_LINE = 1;

	private PropertyFileNormalizer(){}
	
	public static void normalizeFile(File file) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(file));
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		int state = 0; // 0=copy, 1 = discard until end of line
		boolean lastWasNewLine = true;
		int commentCount = 0;
		while (true) {
			int b = is.read();
			if (b == -1) {
				break;
			}
			if (b == '\r') {
				continue;
			} else if (b == '\n') {
				lastWasNewLine = true;
			} else if (b== '#' && lastWasNewLine) {
				lastWasNewLine = false;
				if (++commentCount == REMOVE_NTH_COMMENT_LINE) {
					state = 1;
				}
			} else {
				lastWasNewLine = false;
			}
			if (state == 0) {
				bos.write(b);
			}
			if (state == 1 && lastWasNewLine) {
				state = 0;
			}
		}
		is.close();
		bos.close();
		byte[] contents = bos.toByteArray();
		OutputStream fos = new BufferedOutputStream(new FileOutputStream(file));
		fos.write(contents);
		fos.close();
	}


}
