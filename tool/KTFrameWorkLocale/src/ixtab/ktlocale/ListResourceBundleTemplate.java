package ixtab.ktlocale;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.ListResourceBundle;

import com.amazon.ebook.util.text.Base64;

/**
 * This class (rather: its compiled form) is used as a template
 * for producing localized resource bundles.
 * 
 * In other words, all localized resource bundle classes will have
 * the same structure as this class (differing only in package and
 * class name, and of course the "contents").
 * 
 * Modify this class only if you are sure that you know what you're doing.
 * @author ixtab
 *
 */
public class ListResourceBundleTemplate extends ListResourceBundle {

	private static final Object[][] CONTENTS = decodeContents();
	
	@Override
	protected Object[][] getContents() {
		return CONTENTS;
	}

	private static Object[][] decodeContents()  {
		Object[][] result = null;
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(Base64.decode("H4sIAAAAAAAAAFvzloG1tIhBJDraJyuxLFEvJzEvXc8/KSs1ucRaYv/v4CfZt08xMTBUFDAwMDACFQpjUTfhXMR8gWLNHJg6phIG1rT8opLKEgbmkvJ8ANdAvlljAAAA"));
			//Amazon Base64 *already includes* GZip.
			//GZIPInputStream zis = new GZIPInputStream(bis);
			ObjectInputStream ois = new ObjectInputStream(bis);
			result = (Object[][]) ois.readObject();
			fixupMessageFormats(result);
			ois.close();
		} catch (Throwable t) {
			System.err.println("Something which should never have happened actually did happen.");
			try {
				PrintWriter w = new PrintWriter(new FileWriter(new File("/tmp/ixtab.err"), true));
				t.printStackTrace(w);
				w.close();
			} catch (Throwable t2) {}
			result = new Object[][] {};
		}
		return result;
	}
	
	private static void fixupMessageFormats(Object[][] result) {
		for (int i=0; i < result.length; ++i) {
			if (result[i][1] instanceof StringBuffer) {
				result[i][1] = new MessageFormat(result[i][1].toString());
			}
		}
	}
}
