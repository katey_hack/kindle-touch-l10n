package ixtab.ktlocale;

import ixtab.ktlocale.modification.ArraySplitterMapModificationStrategy;
import ixtab.ktlocale.modification.BlacklistedEntryRemoverMapModificationStrategy;
import ixtab.ktlocale.modification.DNTRemoverMapModificationStrategy;
import ixtab.ktlocale.modification.MapModificationStrategy;
import ixtab.ktlocale.modification.NonStringRemoverMapModificationStrategy;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.jar.JarFile;

public class ExtractTask implements Runnable {

	private final String inputDirectory;
	private final File outputDirectory;
	
	public ExtractTask(String inputDirectory, String outputDirectory, boolean force) {
		this.inputDirectory = inputDirectory;
		this.outputDirectory = getVerifiedOutputFile(outputDirectory, force);
	}
	
	@Override
	public void run() {
		try {
			List<JarFile> jars = findJars();
			MessageFormatResources.openWrite(outputDirectory);
			List<ResourceBundle> inBundles = extractRelevantBundles(jars);
			List<KTResourceBundle> outBundles = convertAndFilter(inBundles);
			writeProperties(outBundles);
			MessageFormatResources.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private List<JarFile> findJars() throws IOException {
		List<JarFile> jars = new ArrayList<JarFile>();
		File directory = new File(inputDirectory);
		findJars(directory, jars);
		return jars;
	}

	private void findJars(File directory, List<JarFile> jars) throws IOException {
		for (File file: directory.listFiles()) {
			if (file.isDirectory()) {
				findJars(file, jars);
			} else if (file.getName().endsWith(".jar")) {
				jars.add(new JarFile(file));
			}
		}
	}

	private List<ResourceBundle> extractRelevantBundles(List<JarFile> jars) throws IOException {
		List<ResourceBundle> bundles = new ArrayList<ResourceBundle>();
		new PropertyResourceBundleFinder().findBundlesInJars(bundles, jars);
		new CompiledResourceBundleFinder().findBundlesInJars(bundles, jars);
		return bundles;
	}
	
	private List<KTResourceBundle> convertAndFilter(List<ResourceBundle> bundles) {
		List<KTResourceBundle> out = new ArrayList<KTResourceBundle>(bundles.size());
		// order matters!
		MapModificationStrategy[] modifications = new MapModificationStrategy[] {
				new BlacklistedEntryRemoverMapModificationStrategy(),
				new DNTRemoverMapModificationStrategy(),
				new NonStringRemoverMapModificationStrategy(),
				new ArraySplitterMapModificationStrategy(),
		};
		for (ResourceBundle in: bundles) {
			out.add(new KTResourceBundle(in, modifications));
		}
		removeUninterestingBundles(out);
		return out;
	}

	private void removeUninterestingBundles(List<KTResourceBundle> out) {
		Iterator<KTResourceBundle> it = out.iterator();
		while (it.hasNext()) {
			KTResourceBundle bundle = it.next();
			if (KTLocalizationBlackList.containsBundle(bundle)) {
				it.remove();
				continue;
			}
			if (bundle.getContents().length > 0) {
				if (bundle.descriptor.suggestClassName().endsWith("DNT")) {
					System.out.println("Bundle name suggests not to translate it: Not including "+bundle);
					it.remove();
				}
			}
		}
	}

	private void writeProperties(List<KTResourceBundle> bundles) throws IOException {
		for (KTResourceBundle bundle: bundles) {
			if (bundle.getContents().length == 0) {
//				System.err.println("Skipping "+bundle+": no contents");
				continue;
			}
			String directory = outputDirectory.getCanonicalPath()+File.separator+bundle.descriptor.packageName.replace('.', File.separatorChar);
			new File(directory).mkdirs();
//			String file = directory + File.separator + bundle.descriptor.suggestClassName() + ".properties";
			Locale loc = bundle.getLocale();
			
			//FIXME: ugly hack.
			String suffix = "";
			if (loc != null && loc.toString().equals("en") && bundle.descriptor.toString().contains("SimpleTextListFormatResources")) {
				suffix ="_US";
			} else if (loc == null) {
				suffix = "_en_US";
			}
			
			String file = directory + File.separator + bundle.descriptor.suggestClassName() + suffix + ".properties";
			store(bundle, file);
		}
	}

	private void store(KTResourceBundle bundle, String filename) throws IOException {
		File file = new File(filename);
		Properties props = bundle.toProperties();
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
		Writer ow = new OutputStreamWriter(out, "UTF-8");
		props.store(ow, null);
		ow.close();
		PropertyFileNormalizer.normalizeFile(file);
	}

	private File getVerifiedOutputFile(String outputDirectory, boolean force) {
		File output = new File(outputDirectory);
		if (!output.exists() || !output.isDirectory()) {
			throw new IllegalStateException("The output directory "+outputDirectory+" does not exist. Create this directory first (running with --force will NOT automatically create it).");
		}
		if (output.list().length != 0) {
			if (!force) {
				throw new IllegalStateException("The output directory "+outputDirectory+" is not empty. Refusing to write there unless --force is used.");
			} else {
				deleteRecursively(output);
			}
		}
		return output;
	}

	private void deleteRecursively(File directory) {
		for (File file: directory.listFiles()) {
			if (file.isDirectory()) {
				deleteRecursively(file);
			}
			if (!file.delete()) {
				throw new RuntimeException("Unable to delete: "+file);
			}
		}
	}
	
}
