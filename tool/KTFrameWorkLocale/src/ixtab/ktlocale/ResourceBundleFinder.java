package ixtab.ktlocale;

import java.io.IOException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.jar.JarFile;

public interface ResourceBundleFinder {
	public void findBundlesInJars(List<ResourceBundle> output, List<JarFile> jars) throws IOException;
}
