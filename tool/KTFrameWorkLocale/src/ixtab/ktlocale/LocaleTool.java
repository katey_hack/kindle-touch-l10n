package ixtab.ktlocale;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.TimeZone;

import ixtab.ktlocale.kindletool.KindleTool;
import ixtab.ktlocale.util.DirectoryFinder;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;
import com.martiansoftware.jsap.UnflaggedOption;

public class LocaleTool {

	public static final String PARAM_LOCALE = "locale";
	public static final String PARAM_GLIBC_LOCALES = "glibc-locales";
	public static final String PARAM_TARGET = "target";
	public static final String PARAM_SOURCE = "source";
	public static final String PARAM_FORCE = "force";
	public static final String PARAM_MODE = "mode";
	public static final String PARAM_MSGFMT = "msgfmt";
	public static final String PARAM_KINDLETOOL = "kindletool";
	public static final String PARAM_JSTOOL = "jstool";

	public static final String MODE_COMPILE = "compile";
	public static final String MODE_EXTRACT = "extract";
	public static final String MODE_CONVERT = "iso2utf";
	public static final String MODE_DIST = "dist";
	public static final String MODE_KINDLETOOL = "kindletool";

	@SuppressWarnings("unused")
	private static String[] sampleExtractArgs = new String[] {
			MODE_EXTRACT,
			"-f",
			"-s",
			System.getProperty("user.home")
					+ "/kindle-touch/fs.510/opt/amazon/ebook/", "-t",
			"/tmp/loc", };

	@SuppressWarnings("unused")
	private static String[] sampleCompileArgs = new String[] { MODE_COMPILE,
			"-f", "-s", System.getProperty("user.home")
			+ "/kindle-touch/localization/kindle-touch-l10n-tx-full/src/5.0.3/framework", "-t", "/tmp/locale-es.jar", "es", };

	@SuppressWarnings("unused")
	private static String[] sampleDistArgs = new String[] {
			MODE_DIST,
			"-f",
			"-s",
			System.getProperty("user.home")
					+ "/kindle-touch/localization/kindle-touch-l10n-tx-full/src/5.0.3",
			"-t", "/tmp/update_locale-LOCALE_TYPE.bin", "hi_IN", };

	@SuppressWarnings("unused")
	private static String[] sampleKindleToolArgs = new String[] {
			MODE_KINDLETOOL,
			"-f",
			"-s",
			System.getProperty("user.home")
					+ "/workspaces/kindle-touch/Launcher-WPA/install",
			"-t", "/tmp/update_launcher_install.bin"};

	public static void main(String[] args) throws JSAPException {
		// only enable for quick stdargs testing. DO NOT enable or depend on
		// them when checking in.
		// args =sampleCompileArgs;
		// args =sampleExtractArgs;
		// args = sampleDistArgs;
		// args = sampleKindleToolArgs;

		// required for correct MessageFormat handling
		Locale.setDefault(Locale.US);
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

		
		JSAP jsap = initializeArgumentParser();
		JSAPResult config = validateArguments(args, jsap);
		String source = config.getString(PARAM_SOURCE);
		String target = config.getString(PARAM_TARGET);
		String msgfmt = config.getString(PARAM_MSGFMT);
		String kindletool = config.getString(PARAM_KINDLETOOL);
		String jstool = config.getString(PARAM_JSTOOL);
		String glibcLocales = config.getString(PARAM_GLIBC_LOCALES);
		boolean force = config.getBoolean(PARAM_FORCE);
		String[] locales = config.getStringArray(PARAM_LOCALE);

		String mode = config.getString(PARAM_MODE);
		if (MODE_EXTRACT.equals(mode)) {
			new ExtractTask(source, target, force).run();
		} else if (MODE_COMPILE.equals(mode)) {
			new CompressTask(source, target, force, locales).run();
		} else if (MODE_CONVERT.equals(mode)) {
			new ConvertTask(source, target, force).run();
		} else if (MODE_DIST.equals(mode)) {
			new DistributionTask(source, target, glibcLocales, msgfmt,
					kindletool, jstool, force, locales).run();
		} else if (MODE_KINDLETOOL.equals(mode)) {
			new KindleToolTask(source, target, kindletool, force).run();
		} else {
			throw new IllegalArgumentException("Unknown mode: " + mode);
		}
	}

	private static JSAPResult validateArguments(String[] args, JSAP jsap) {
		JSAPResult config = jsap.parse(args);
		if (!config.success()) {
			displayUsage(jsap, config);
		}
		return config;
	}

	private static void displayUsage(JSAP jsap, JSAPResult config) {
		for (@SuppressWarnings("rawtypes")
		java.util.Iterator errs = config.getErrorMessageIterator(); errs
				.hasNext();) {
			System.err.println("Error: " + errs.next());
		}

		System.err.println();
		System.err.println("Usage: java -jar kt-l10n.jar " + jsap.getUsage());
		System.err.println();
		System.err.println(jsap.getHelp());
		System.exit(1);
	}

	private static JSAP initializeArgumentParser() throws JSAPException {
		JSAP jsap = new JSAP();

		registerModeParameter(jsap);
		registerForceParameter(jsap);
		registerSourceParameter(jsap);
		registerTargetParameter(jsap);
		registerGlibcLocaleParameter(jsap);
		registerJsToolParameter(jsap);
		registerMsgFmtParameter(jsap);
		registerKindletoolParameter(jsap);
		registerLocaleParameter(jsap);

		return jsap;
	}

	private static void registerModeParameter(JSAP jsap) throws JSAPException {
		UnflaggedOption mode = new UnflaggedOption(PARAM_MODE).setStringParser(
				JSAP.STRING_PARSER).setRequired(true);
		mode.setHelp("One of \""
				+ MODE_EXTRACT
				+ "\", \""
				+ MODE_COMPILE
				+ "\", \""
				+ MODE_DIST
				+ "\", \""
				+ MODE_KINDLETOOL
				+ "\", or \""
				+ MODE_CONVERT
				+ "\".\n * "
				+ MODE_EXTRACT
				+ ": finds jars in source directory and extracts relevant resource bundles into target directory.\n * "
				+ MODE_COMPILE
				+ ": compiles all relevant resource files in source into a single file.\n * "
				+ MODE_DIST
				+ ": creates binary update files, combining resources into a format suitable for installation on Kindles.\n * "
				+ MODE_KINDLETOOL
				+ " (experimental): hack to partially replace kindletool functionality. This mode is not officially supported.\n * "
				+ MODE_CONVERT
				+ " (obsolete): converts all existing .properties files from ISO-8859-1 to UTF-8 format.\n"

				+ "");
		jsap.registerParameter(mode);
	}

	private static void registerForceParameter(JSAP jsap) throws JSAPException {
		Switch force = new Switch(PARAM_FORCE).setShortFlag('f').setLongFlag(
				PARAM_FORCE);
		force.setHelp("Forces execution of operations which would otherwise be deemed unsafe, like directory creation/deletion or overwriting files. Use at own risk.");
		jsap.registerParameter(force);
	}

	private static void registerSourceParameter(JSAP jsap) throws JSAPException {
		FlaggedOption source = new FlaggedOption(PARAM_SOURCE)
				.setShortFlag('s').setLongFlag(PARAM_SOURCE)
				.setStringParser(JSAP.STRING_PARSER).setRequired(true);
		source.setHelp("Source directory to look for input files.\nIn extract mode, directory to search for jar files.\nIn compile mode, directory to search for properties files relevant to the given locales.\nIn dist mode, directory containing translations.");
		jsap.registerParameter(source);
	}

	private static void registerTargetParameter(JSAP jsap) throws JSAPException {
		FlaggedOption target = new FlaggedOption(PARAM_TARGET)
				.setShortFlag('t').setLongFlag(PARAM_TARGET)
				.setStringParser(JSAP.STRING_PARSER).setRequired(true);
		target.setHelp("Target file or directory.\nIn extract mode, directory to write properties files to.\nIn compile mode, jar file to compile resources into.\nIn dist mode, binary file (template) name.");
		jsap.registerParameter(target);
	}

	private static void registerGlibcLocaleParameter(JSAP jsap)
			throws JSAPException {
		FlaggedOption glibclocale = new FlaggedOption(PARAM_GLIBC_LOCALES)
				.setShortFlag('g').setLongFlag(PARAM_GLIBC_LOCALES)
				.setStringParser(JSAP.STRING_PARSER).setRequired(false);
		String help = "directory with glibc locale bundles. Relevant for dist mode only. ";
		File auto = DirectoryFinder.determineGlibcLocaleDir();
		if (auto != null) {
			try {
				help += "If not set, \"" + auto.getCanonicalPath()
						+ "\" is used.";
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else {
			help += "No sensible default could be determined, so you must provide a correct directory.";
		}
		glibclocale.setHelp(help);
		jsap.registerParameter(glibclocale);
	}

	private static void registerJsToolParameter(JSAP jsap)
			throws JSAPException {
		FlaggedOption jsTool = new FlaggedOption(PARAM_JSTOOL)
				.setShortFlag('j').setLongFlag(PARAM_JSTOOL)
				.setStringParser(JSAP.STRING_PARSER).setRequired(false);
		String help = "Command to invoke js resources tool. Relevant for dist mode only. ";
		boolean ok = false;
		if (JSResourceTool.canBeAutoConfigured()) {
			try {
				new JSResourceTool(null);
				help += "If not set, \"" + JSResourceTool.DEFAULT_COMMAND
						+ "\" is used.";
				ok = true;
			} catch (Throwable e) {
			}
		}
		if (!ok) {
			help += "No sensible default could be determined, so you must provide a correct command. Be sure to surround the command with double quotes, because it will almost certainly contain whitespaces.";
		}
		jsTool.setHelp(help);
		jsap.registerParameter(jsTool);
	}

	private static void registerMsgFmtParameter(JSAP jsap) throws JSAPException {
		FlaggedOption msgfmt = new FlaggedOption(PARAM_MSGFMT)
				.setShortFlag('m').setLongFlag(PARAM_MSGFMT)
				.setStringParser(JSAP.STRING_PARSER).setRequired(false);
		msgfmt.setHelp("msgfmt executable to use. Relevant for dist mode only. If not set, \""
				+ MsgFmt.DEFAULT_EXECUTABLE + "\" is used.");
		jsap.registerParameter(msgfmt);
	}

	private static void registerKindletoolParameter(JSAP jsap)
			throws JSAPException {
		FlaggedOption kindletool = new FlaggedOption(PARAM_KINDLETOOL)
				.setShortFlag('k').setLongFlag(PARAM_KINDLETOOL)
				.setStringParser(JSAP.STRING_PARSER).setRequired(false);
		kindletool
				.setHelp("kindletool executable to use. Relevant for dist mode only. If not set, \""
						+ KindleTool.DEFAULT_EXECUTABLE + "\" is used. ");
		jsap.registerParameter(kindletool);
	}

	private static void registerLocaleParameter(JSAP jsap) throws JSAPException {
		UnflaggedOption locale = new UnflaggedOption(PARAM_LOCALE)
				.setStringParser(JSAP.STRING_PARSER).setRequired(false)
				.setGreedy(true);
		locale.setHelp("Locales to consider - relevant in compile or dist mode only.\nIn compile mode, not providing a locale results in an output file containing no localizations.\n In dist mode, at least one locale is required. \"ALL\" will create distribution archives for all currently registered locales.");
		jsap.registerParameter(locale);
	}

}
