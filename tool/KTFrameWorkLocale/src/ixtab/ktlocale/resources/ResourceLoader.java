package ixtab.ktlocale.resources;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ResourceLoader {
	public static final String OFF_50x40 = "off_ln_50x40.png";
	public static final String OFF_70x40 = "off_ln_70x40.png";
	
	private ResourceLoader() {}
	
	public static InputStream load(String resourceName) {
		return ResourceLoader.class.getResourceAsStream(resourceName);
	}
	
	public static byte[] getBytes(String resourceName) throws IOException {
		InputStream in = load(resourceName);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		for (int b=in.read(); b != -1; b = in.read()) {
			out.write(b);
		}
		return out.toByteArray();
	}
}
