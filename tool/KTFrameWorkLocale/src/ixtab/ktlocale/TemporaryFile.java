package ixtab.ktlocale;

import java.io.File;
import java.io.IOException;

public class TemporaryFile {
	private TemporaryFile() {
	}

	public static File makeTemporaryFile(String prefix, String suffix)
			throws IOException {
		File f;
		f = File.createTempFile(prefix + "-", suffix);
		f.deleteOnExit();
		return f;
	}

}
