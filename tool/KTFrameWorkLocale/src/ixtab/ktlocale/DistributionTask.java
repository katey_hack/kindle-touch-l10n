package ixtab.ktlocale;

import static ixtab.ktlocale.TemporaryFile.makeTemporaryFile;
import ixtab.ktlocale.cldr.CLDRLocale;
import ixtab.ktlocale.kindletool.KindleTool;
import ixtab.ktlocale.resources.ResourceLoader;
import ixtab.ktlocale.util.DirectoryFinder;
import ixtab.ktlocale.util.LocaleUtil;
import ixtab.ktlocale.util.TGZCreator;
import ixtab.ktlocale.util.TemporaryDirectory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class DistributionTask implements Runnable {

	public static final String PATTERN_TYPE = "TYPE";
	public static final String PATTERN_LOCALE = "LOCALE";
	public static final String TYPE_INSTALL = "install";
	public static final String TYPE_UNINSTALL = "uninstall";
	
	private final Workspace workspace;
	private final List<String> locales;
	private final String target;
	private final boolean force;
	private final MsgFmt msgfmt;
	private final KindleTool kindletool;
	private final File glibcLocales;
	private final String jsToolCmd;

	public DistributionTask(String source, String target, String glibcLocales, String msgfmt,
			String kindletool, String jstool, boolean force, String... locales) {
		workspace = new Workspace(new File(source));
		this.glibcLocales = validateGlibcLocales(glibcLocales);
		this.locales = evaluateLocaleArray(locales);
		this.target = target;
		this.force = force;
		this.msgfmt = new MsgFmt(msgfmt);
		this.kindletool = new KindleTool(kindletool);
		this.jsToolCmd = validateJSToolCommand(jstool);
		validateTarget();
	}

	private String validateJSToolCommand(String jstool) {
		if (jstool != null) {
			new JSResourceTool(jstool);
		} else {
			if (!JSResourceTool.canBeAutoConfigured()) {
				throw new IllegalArgumentException("The correct parameters for the jstool integration cannot be autodetermined. You must manually specify the full command line (e.g. \"/opt/bin/python /path/to/tool/js_resources\") using the --"+LocaleTool.PARAM_JSTOOL+" parameter.");
			}
		}
		return jstool;
	}

	private File validateGlibcLocales(String glibc) {
		if (glibc == null) {
			File defaultFile = DirectoryFinder.determineGlibcLocaleDir();
			if (defaultFile != null) {
				return defaultFile;
			}
		} else {
			File file = new File(glibc);
			if (DirectoryFinder.isLocaleSubdirectory(file)) {
				return file;
			}
		}
		throw new IllegalArgumentException("You must provide a valid directory containing glibc locale files.");
	}

	private List<String> evaluateLocaleArray(String[] in) {
		if (in.length == 0)
			throw new IllegalArgumentException(
					"Must provide at least one locale");

		List<String> all = workspace.listAvailableLocales();
		if (in.length == 1 && in[0].equals("ALL")) {
			return all;
		}
		List<String> out = new ArrayList<String>();
		for (String locale : in) {
			if (!all.contains(locale)) {
				throw new IllegalArgumentException("No translation for \""
						+ locale + "\" found. Available translations are: "
						+ all);
			}
			out.add(locale);
		}
		return out;
	}

	private void validateTarget() {
		if (!target.contains(PATTERN_TYPE)) {
			throw new IllegalArgumentException("Invalid target name: " + target
					+ ". Target name must contain substring \"" + PATTERN_TYPE
					+ "\", which will be replaced by \"" + TYPE_INSTALL
					+ "\"/\"" + TYPE_UNINSTALL + "\".");
		}
		if (locales.size() > 1 && !target.contains(PATTERN_LOCALE)) {
			throw new IllegalArgumentException(
					"Invalid target name: "
							+ target
							+ ". For batch distribution creation, target name must contain substring \""
							+ PATTERN_LOCALE
							+ "\", which will be replaced by the locale code.");
		}
	}

	@Override
	public void run() {
		for (String locale : locales) {
			if (locales.size() > 1) {
				System.out.println("Creating distribution: "+locale);
			}
			try {
				createDistribution(locale);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private void createDistribution(String localeString) throws IOException {
		Map<String, File> content = new TreeMap<String, File>();
		Locale source = LocaleUtil.localeFromString(localeString);
		CLDRLocale[] locales = CLDRLocale.load(source);
		addBlanketFiles(source, locales, content);
		addJavaFiles(source, content);
		
		List<TemporaryDirectory> tempDirs = new ArrayList<TemporaryDirectory>();
		
		tempDirs.add(addJavaScriptFiles(new JSResourceHandler(jsToolCmd, JSResourceHandler.Type.WAF, workspace, source), content));
		tempDirs.add(addJavaScriptFiles(new JSResourceHandler(jsToolCmd, JSResourceHandler.Type.PILLOW, workspace, source), content));
		addMediaPlayerOffButton(source, content);
		
		addManifest(source, content);
		createKindlePackages(source, content);
		for (TemporaryDirectory tmpDir: tempDirs) {
			if (tmpDir != null) {
				tmpDir.deleteRecursively();
			}
		}
	}


	private void addBlanketFiles(Locale source, CLDRLocale[] locales, Map<String, File> content)
			throws IOException {
		Locale territory = locales[CLDRLocale.LANGUAGE_AND_TERRITORY].locale;
		String prefix = workspace.blanketDir.getCanonicalPath()
				+ File.separator + source.toString() + File.separator;
		for (File po : workspace.findBlanketFiles(source)) {
			String kindleFilename = po.getCanonicalPath();
			if (!kindleFilename.startsWith(prefix)) {
				throw new IllegalStateException();
			}
			kindleFilename = kindleFilename.substring(prefix.length());
			kindleFilename = "/usr/share/locale/" + territory.toString() + "/"
					+ kindleFilename;
			// .po -> .mo
			kindleFilename = kindleFilename.substring(0,
					kindleFilename.length() - 3)
					+ ".mo";
			File mo = makeTemporaryFile(po.getName().replace(".po", ""), ".mo");
			msgfmt.format(po, mo);
			content.put(kindleFilename, mo);
		}
	}

	private void addJavaFiles(Locale locale, Map<String, File> content)
			throws IOException {
		File jar = makeTemporaryFile("locale-" + locale, ".jar");
		new CompressTask(workspace.javaDir.getCanonicalPath(),
				jar.getCanonicalPath(), true, locale.toString()).run();
		content.put("/opt/amazon/ebook/lib/locale-" + locale + ".jar", jar);
	}

	private TemporaryDirectory addJavaScriptFiles(JSResourceHandler js, Map<String, File> output) throws IOException {
		return js.fillInstallFiles(output);
	}
	
	private void addMediaPlayerOffButton(Locale source,
			Map<String, File> content) throws IOException {
		String locale = LocaleUtil.jsLocaleStringFromTransifexLocale(source, true);
		String filename = "/usr/share/webkit-1.0/pillow/assets/locales/"+locale+"/off.png";
		byte[] bytes = ResourceLoader.getBytes(ResourceLoader.OFF_70x40);
		File file = makeTemporaryFile("off", ".png");
		FileOutputStream os = new FileOutputStream(file);
		os.write(bytes);
		os.close();
		content.put(filename, file);
	}


	private void addManifest(Locale locale, Map<String, File> content)
			throws IOException {
		String filename = locale.toString() + ".properties";
		File manifest = makeTemporaryFile(locale.toString(), ".properties");
		Properties props = CLDRLocale.getProperties(locale);
		FileOutputStream os = new FileOutputStream(manifest);
		props.store(os, null);
		os.close();
		PropertyFileNormalizer.normalizeFile(manifest);
		content.put("/opt/amazon/ebook/config/locales/" + filename, manifest);
	}

	private void createKindlePackages(Locale locale, Map<String, File> content)
			throws IOException {
		String deleteInstructions = generateDeleteInstructions(content);
		Map<String, File> installFiles = createKindleInstallFiles(content, locale);
		Map<String, File> uninstallFiles = createKindleUninstallFiles(locale, deleteInstructions);
		KindleToolTask task = new KindleToolTask(kindletool, force);
		createKindlePackage(task, TYPE_INSTALL, locale, installFiles);
		createKindlePackage(task, TYPE_UNINSTALL, locale, uninstallFiles);

	}

	private String generateDeleteInstructions(Map<String, File> installed) {
		StringBuffer cmd = new StringBuffer();
		SortedSet<String> allFilesOrDirs = new TreeSet<String>();
		for (String fileOrDir: installed.keySet()) {
			addCompletePathOf(new File(fileOrDir), allFilesOrDirs);
		}
		List<String> all = new ArrayList<String>(allFilesOrDirs);
		Collections.sort(all);
		for (int i= all.size()-1; i >= 0; --i) {
			String file = all.get(i);
			if (!file.contains("/"+JSResourceHandler.DIR_LOCALES)) {
				continue;
			}
			if (file.startsWith("/opt/amazon")) {
				// "false positive"
				continue;
			}
			File check = installed.get(file);
			if (check == null || check.isDirectory()) {
				cmd.append("test -d " +file+" && rmdir "+file+"\n");
			} else {
				cmd.append("test -f " +file+" && rm "+file+"\n");
			}
		}
		String result = cmd.toString();
		//System.err.println(result);
		return result;
	}
	
	

	private void addCompletePathOf(File fileOrDir,
			SortedSet<String> allFilesOrDirs) {
		if (fileOrDir == null) {
			return;
		}
		allFilesOrDirs.add(fileOrDir.getAbsolutePath());
		addCompletePathOf(fileOrDir.getParentFile(), allFilesOrDirs);
	}

	private Map<String, File> createKindleInstallFiles(Map<String, File> content,
			Locale locale) throws IOException {
		Map<String, File> map = new HashMap<String, File>();
		map.put("glibc-locale.tgz", findGlibcLocaleFile(locale));
		File archive = makeTemporaryFile("locale-"+locale, ".tgz");
		map.put("locale.tgz", archive);
		TGZCreator.createTGZFile(archive, content);
		File shell = makeTemporaryFile("run", ".ffs");
		map.put("run.ffs", shell);
		createInstallerShellFile(shell, locale);
		return map;
	}

	private File findGlibcLocaleFile(Locale locale) {
		Locale systemLocale = locale;
		if (locale.getCountry().equals("")) {
			systemLocale = CLDRLocale.load(locale)[CLDRLocale.LANGUAGE_AND_TERRITORY].locale;
		}
		String pattern = systemLocale.toString()+".tgz";
		for (File candidate: glibcLocales.listFiles()) {
			if (candidate.getName().equals(pattern)) {
				return candidate;
			}
		}
		throw new IllegalStateException("FATAL ERROR: For locale "+locale+", the required file "+glibcLocales+File.separator+pattern+" was not found.");
	}

	private void createInstallerShellFile(File file, Locale locale) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("kindletool/install_template.sh")));
		PrintStream out = new PrintStream(file);
		Locale systemLocale = CLDRLocale.load(locale)[CLDRLocale.LANGUAGE_AND_TERRITORY].locale;
		for (String line = in.readLine(); line != null; line = in.readLine()) {
			line = replaceLocalePatterns(line, locale, systemLocale);
			out.println(line);
		}
		in.close();
		out.close();
	}

	private Map<String, File> createKindleUninstallFiles(Locale locale, String deleteCommands) throws IOException {
		Map<String, File> map = new HashMap<String, File>();
		File shell = makeTemporaryFile("run", ".ffs");
		map.put("run.ffs", shell);
		BufferedReader in = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("kindletool/uninstall_template.sh")));
		PrintStream out = new PrintStream(shell);
		Locale systemLocale = CLDRLocale.load(locale)[CLDRLocale.LANGUAGE_AND_TERRITORY].locale;
		for (String line = in.readLine(); line != null; line = in.readLine()) {
			if (line.equals("# _RUN_DELETE_COMMANDS_ #")) {
				out.println(deleteCommands);
				continue;
			}
			line = replaceLocalePatterns(line, locale, systemLocale);
			out.println(line);
		}
		in.close();
		out.close();
		return map;
	}

	private String replaceLocalePatterns(String line, Locale locale, Locale systemLocale) {
		// this is simple and stupid, without regexes
		line = line.replace("<LOC_SHORT>", locale.getLanguage());
		line = line.replace("<LOC_FULL>", locale.toString());
		line = line.replace("<LOC_SYSTEM>", systemLocale.toString());
		return line;
	}

	private void createKindlePackage(KindleToolTask kt, String type, Locale locale,
			Map<String, File> sources) throws IOException {
		File bin = getInstallerFile(locale, type);
		kt.createKindlePackage(sources, bin);
	}

	private File getInstallerFile(Locale locale, String type) {
		String output = target;
		output = output.replace(PATTERN_LOCALE, locale.toString());
		output = output.replace(PATTERN_TYPE, type);
		File bin = new File(output);
		return bin;
	}


}
