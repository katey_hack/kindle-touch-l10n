package ixtab.ktlocale;

import static ixtab.ktlocale.TemporaryFile.makeTemporaryFile;
import ixtab.ktlocale.kindletool.KindleTool;
import ixtab.ktlocale.util.MD5Util;
import ixtab.ktlocale.util.TGZCreator;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.ssl.PKCS8Key;

public class KindleToolTask {
	private static final long BLOCK_SIZE = 64;
	private final KindleTool kindletool;
	private final PrivateKey key;
	private final boolean force;
	private final File source;
	private final File target;

	public KindleToolTask(KindleTool kindletool, boolean force) {
		this(null, null, kindletool, force);
	}
	
	public KindleToolTask(String source, String target, String kindletool,
			boolean force) {
		this (source, target, new KindleTool(kindletool), force);
	}

	public KindleToolTask(String source, String target, KindleTool kindletool,
			boolean force) {
		this.source = verifySource(source);
		this.target = target == null ? null : new File(target);
		this.kindletool = kindletool;
		this.force = force;
		key = loadPrivateKey();
	}

	private File verifySource(String directory) {
		if (directory == null) {
			return null;
		}
		File result = new File(directory);
		if (!result.isDirectory()) {
			throw new IllegalArgumentException(directory+" is not a directory");
		}
		return result;
	}

	private PrivateKey loadPrivateKey() {
		InputStream is = KindleTool.class.getResourceAsStream("key.pem");
		try {
			return new PKCS8Key(is, null).getPrivateKey();
		} catch (GeneralSecurityException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	public void run() {
		Map<String, File> sources = new TreeMap<String, File>();
		for (File file: source.listFiles()) {
			sources.put(file.getName(), file);
		}
		try {
			createKindlePackage(sources, target);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}


	public void createKindlePackage(Map<String, File> sources,
			File bin) throws IOException {
		File tgz = makeTemporaryFile(bin.getName(), ".tgz");
		Map<String, File> finalContents = new HashMap<String, File>();
		StringBuilder manifest = new StringBuilder();
		for (Map.Entry<String, File> entry: sources.entrySet()) {
			addSignatureAndManifest(entry.getKey(), entry.getValue(), key, finalContents, manifest);
		}
		File manifestFile = makeTemporaryFile("update-filelist", ".dat");
		FileWriter fw = new FileWriter(manifestFile);
		fw.append(manifest.toString());
		fw.close();
		addSignatureAndManifest("update-filelist.dat", manifestFile, key, finalContents, null);
		
		TGZCreator.createTGZFile(tgz, finalContents);
		invokeKindleTool(tgz, bin);
	}
	
	private void addSignatureAndManifest(String filename, File source,
			PrivateKey key, Map<String, File> target,
			StringBuilder manifest) throws IOException {
		if (manifest != null) {
			addManifestEntry(filename, source, manifest);
		}
		String sigName = filename+".sig";
		File sigFile = makeTemporaryFile(filename, ".sig");
		target.put(filename, source);
		target.put(sigName, sigFile);
		signFile(key, source, sigFile);
	}

	private void addManifestEntry(String filename, File source,
			StringBuilder manifest) throws IOException {
		manifest.append(filename.endsWith(".ffs") ? "129":"128");
		manifest.append(" ");
		manifest.append(MD5Util.getMD5Sum(source));
		manifest.append(" ");
		manifest.append(filename);
		manifest.append(" ");
		manifest.append(source.length() / BLOCK_SIZE);
		manifest.append(" ");
		manifest.append(filename);
		manifest.append("\n");
	}

	private void signFile(PrivateKey key, File source, File target) throws IOException {
		try {
			Signature signer = Signature.getInstance("SHA256WithRSA");
			signer.initSign(key);
			BufferedInputStream is = new BufferedInputStream(new FileInputStream(source));
			byte[] buffer = new byte[1024];
			int len;
			while ((len = is.read(buffer)) >= 0) {
			    signer.update(buffer, 0, len);
			};
			is.close();
			byte[] signature = signer.sign();
			BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(target));
			os.write(signature);
			os.close();
		} catch (SignatureException se) {
			throw new IllegalStateException(se);
		} catch (InvalidKeyException ke) {
			throw new IllegalStateException(ke);
		} catch (NoSuchAlgorithmException ne) {
			throw new IllegalStateException(ne);
		}
	}

	private void invokeKindleTool(File tgz, File bin) throws IOException {
		if (bin.exists() && !force) {
			throw new IllegalStateException("File " + bin+ " exists. Refusing to overwrite unless forced.");
		}
		kindletool.pack(tgz, bin);
		// useful for debugging, i.e., inspecting the files; uncomment next line and set breakpoint after it.
//		System.err.println("packed "+tgz);
		System.out.println("Successfully created "+bin.getCanonicalPath());
	}

}
