package ixtab.ktlocale;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import java.util.TreeSet;

public class MessageFormatResources {
	private static final String FILENAME ="msgfmt.dat";
	
	private static PrintWriter out;
	private static Set<String> entries = new TreeSet<String>();
	
	public static void close() {
		if (out != null) {
			out.close();
			out = null;
		}
	}
	
	public static void read(File directory) {
		try {
			File file = new File(directory.getCanonicalPath()+File.separator+FILENAME);
			BufferedReader r = new BufferedReader(new FileReader(file));
			for (String l = r.readLine(); l != null; l = r.readLine()) {
				entries.add(l);
			}
			r.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void openWrite(File directory) {
		try {
			File file = new File(directory.getCanonicalPath()+File.separator+FILENAME);
			out = new PrintWriter(file);
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void add(String packageName, String className, String resourceName) {
		out.println(join(packageName, className, resourceName));
	}

	private static String join(String packageName, String className,
			String resourceName) {
		return packageName+"."+className+"#"+resourceName;
	}
	
	public static boolean contains(String packageName, String className, String resourceName) {
		return entries.contains(join(packageName, className, resourceName));
	}
}
