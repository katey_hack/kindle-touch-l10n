package ixtab.ktlocale;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class BundleDescriptor {
	
	public final String bundleName;
	public final String locale;
	public final String packageName;
	public final File file;
	
	public BundleDescriptor (File file, String packageName, String bundleAndLocale) {
		this.file = file;
		this.packageName = packageName;
		int separator = bundleAndLocale.indexOf('_');
		if (separator == -1) {
			bundleName = bundleAndLocale;
			locale = "";
		} else {
			bundleName = bundleAndLocale.substring(0, separator);
			String loc = bundleAndLocale.substring(separator);
			if (loc.startsWith("_")) {
				locale = loc.substring(1);
			} else {
				locale = loc;
			}
		}
		//System.out.println(bundle+"#"+locale);
	}

	public static List<BundleDescriptor> find (File rootDirectory, String... localeFilter) {
		List<BundleDescriptor> results = new ArrayList<BundleDescriptor>();
		find(results, rootDirectory, rootDirectory, localeFilter);
		return results;
	}
	
	private static void find(List<BundleDescriptor> results, File root, File directory, String[] localeFilter) {
		String packageName = extractPackageName(root, directory);
		File[] files = directory.listFiles();
		Arrays.sort(files, new Comparator<File>() {

			@Override
			public int compare(File o1, File o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		for (File entry: files) {
			if (entry.isDirectory()) {
				find (results, root, entry, localeFilter);
			} else {
				if (entry.getName().endsWith(".properties")) {
					BundleDescriptor descriptor = new BundleDescriptor(entry, packageName, entry.getName().replace(".properties", ""));
					if (descriptor.matchesFilter(localeFilter)) {
						results.add(descriptor);
					}
				}
			}
		}
	}

	private static String extractPackageName(File root, File directory) {
		String pkg = null;
		try {
			pkg = directory.getCanonicalPath().replace(root.getCanonicalPath(), "");
		} catch (IOException e) {
			// won't happen
			throw new RuntimeException(e);
		}
		pkg = pkg.replace('\\', '/');
		while (pkg.startsWith("/")) {
			pkg = pkg.substring(1);
		}
		pkg = pkg.replace('/', '.');
		return pkg;
	}
	
	private boolean matchesFilter(String[] localeFilter) {
		for (String candidate: localeFilter) {
			if (candidate.equals(locale)) {
				return true;
			}
		}
		return false;
	}


}
