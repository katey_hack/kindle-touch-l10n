package ixtab.ktlocale.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import ixtab.ktlocale.cldr.CLDRLoader;

import org.junit.Test;



public class TestCLDRLoader {
	@Test
	public void testExistingLocales() throws Exception {
		assertNotNull(CLDRLoader.getCLDRFileAsStream("de"));
		assertNotNull(CLDRLoader.getCLDRFileAsURL("ru_RU"));
		assertNull(CLDRLoader.getCLDRFileAsURL("de_RU"));
		assertNull(CLDRLoader.getCLDRFileAsStream("ru_DE"));
	}
}
