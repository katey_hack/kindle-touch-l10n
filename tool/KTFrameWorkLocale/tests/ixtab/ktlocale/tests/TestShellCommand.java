package ixtab.ktlocale.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import ixtab.ktlocale.ShellCommand;

import org.junit.Test;


public class TestShellCommand {
	@Test
	public void testValidCommand() throws Exception {
		ShellCommand.Result result = ShellCommand.exec("uname -a");
		assertEquals(0, result.code);
		assertTrue(result.out.startsWith("Linux "));
		assertEquals("", result.err);
	}
	
	@Test(expected=IOException.class)
	public void testInvalidCommand() throws Exception {
		ShellCommand.exec("thiscommanddoesntexistonanysanesystem");
	}
	
	@Test
	public void testIncompleteCommand() throws Exception {
		ShellCommand.Result result = ShellCommand.exec("touch");
		assertEquals(1, result.code);
		assertEquals("", result.out);
		assertTrue(result.err.startsWith("touch:"));
	}
}
