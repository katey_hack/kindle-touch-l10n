package ixtab.ktlocale.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import ixtab.ktlocale.Workspace;

import org.junit.Test;


public class TestWorkspace {
	
	String VALIDWORKSPACEDIR = System.getProperty("user.home")+"/kindle-touch/localization/kindle-touch-l10n-tx-full/src/5.0.3";
	
	@Test
	public void testInvalidDir() throws Exception {
		try {
			new Workspace(new File("/INEXISTENT"));
			fail();
		} catch (IllegalArgumentException e) {}
		try {
			new Workspace(new File("/tmp"));
			fail();
		} catch (IllegalArgumentException e) {}
	}
	
	@Test
	public void testLocales() throws Exception {
		Workspace w = new Workspace(new File(VALIDWORKSPACEDIR));
		List<String> locales = w.listAvailableLocales();
		assertNotNull(locales);
		assertTrue(locales.contains("de"));
		System.out.println(locales);
	}
}
