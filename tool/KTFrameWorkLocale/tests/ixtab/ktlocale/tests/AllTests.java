package ixtab.ktlocale.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	TestBundleDescriptor.class,
	TestCLDRLoader.class,
	TestCLDRLocale.class,
	TestKTResourceBundle.class,
	TestListResourceBundleAssembler.class,
	TestMsgFmt.class,
	TestNDimensionalStringArray.class,
	TestShellCommand.class,
	TestWorkspace.class,
	})
public class AllTests {
}
