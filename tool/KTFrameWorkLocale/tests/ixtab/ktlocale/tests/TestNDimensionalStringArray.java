package ixtab.ktlocale.tests;

import static org.junit.Assert.*;
import ixtab.ktlocale.arrays.NDimensionalStringArray;

import org.junit.Test;

public class TestNDimensionalStringArray {
	
	private void assertEquality(Object expected, Object actual) {
		assertArrayEquals(new Object[] {expected}, new Object[] {actual});
		
	}
	
	@Test
	public void oneDimensional() throws Exception {
		NDimensionalStringArray a = NDimensionalStringArray.instantiate(1);
		String[] expected = new String[0];
		assertEquality(expected, a.getObject());
		expected = new String[] {null, "second"};
		a.setEntry("[1]", "second");
		assertEquality(expected, a.getObject());
	}
	
	@Test
	public void twoDimensional() throws Exception {
		NDimensionalStringArray a = NDimensionalStringArray.instantiate(2);
		String[][] expected = new String[0][];
		assertEquality(expected, a.getObject());
		expected = new String[][] {null, new String[]{null, "second"}};
		a.setEntry("[1][1]", "second");
		assertEquality(expected, a.getObject());
	}
	
}
