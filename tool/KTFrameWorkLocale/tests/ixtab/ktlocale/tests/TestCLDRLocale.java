package ixtab.ktlocale.tests;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import ixtab.ktlocale.cldr.CLDRLocale;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.junit.Ignore;
import org.junit.Test;


public class TestCLDRLocale {
	
	Locale be = new Locale("be");
	Locale be_BY = new Locale("be","BY");
	Locale de = Locale.GERMAN;
	Locale de_CH = new Locale("de","CH");
	Locale zh_CN = new Locale("zh","CN");
	Locale zh_TW = new Locale("zh","TW");
	Locale bn_IN = new Locale("bn","IN");
	Locale hi_IN = new Locale("hi","IN");
	Locale ca = new Locale("ca");
	
	@Test
	public void testChineseDescriptions() throws Exception {
		CLDRLocale[] cn = CLDRLocale.load(zh_CN);
		CLDRLocale[] tw = CLDRLocale.load(zh_TW);
		System.err.println("zh_CN@zh_CN:"+ CLDRLocale.describe(zh_CN, cn));
//		System.err.println("zh_CN@zh_TW:"+ CLDRLocale.describe(zh_CN, tw));
//		System.err.println("zh_TW@zh_CN:"+ CLDRLocale.describe(zh_TW, cn));
		System.err.println("zh_TW@zh_TW:"+ CLDRLocale.describe(zh_TW, tw));
	}
	
	@Test
	public void testIndianDescriptions() throws Exception {
		System.err.println("bn_IN@bn_IN:"+ CLDRLocale.describe(bn_IN, CLDRLocale.load(bn_IN)));
		System.err.println("hi_IN@hi_IN:"+ CLDRLocale.describe(hi_IN, CLDRLocale.load(hi_IN)));
	}
	
	@Test
	public void testCatalan() throws Exception {
		System.err.println("ca@ca:"+ CLDRLocale.describe(ca, CLDRLocale.load(ca)));
	}
	
	@Test
	public void testCompletionAndOrderingWork() throws Exception {
		CLDRLocale[] both = CLDRLocale.load(Locale.GERMAN);
		assertEquals(Locale.GERMAN, both[CLDRLocale.LANGUAGE_ONLY].locale);
		assertEquals(Locale.GERMANY, both[CLDRLocale.LANGUAGE_AND_TERRITORY].locale);
		
		both = CLDRLocale.load(Locale.GERMANY);
		assertEquals(Locale.GERMAN, both[CLDRLocale.LANGUAGE_ONLY].locale);
		assertEquals(Locale.GERMANY, both[CLDRLocale.LANGUAGE_AND_TERRITORY].locale);
	}
	
	@Test
	public void testDescriptions() throws Exception {
		CLDRLocale[] swiss = CLDRLocale.load(de_CH);
		
//		assertEquals("Belarusian", CLDRLocale.describe(be, CLDRLocale.ENGLISH));
		assertEquals("Belarusian (Belarus)", CLDRLocale.describe(be, CLDRLocale.ENGLISH));
		assertEquals("Belarusian (Belarus)", CLDRLocale.describe(be_BY, CLDRLocale.ENGLISH));
		
//		assertEquals("Weißrussisch", CLDRLocale.describe(be, swiss[0]));
		assertEquals("Weißrussisch (Belarus)", CLDRLocale.describe(be, swiss[0]));
		assertEquals("Weißrussisch (Belarus)", CLDRLocale.describe(be_BY, swiss[0]));

//		assertEquals("Weissrussisch", CLDRLocale.describe(be, swiss));
		assertEquals("Weissrussisch (Weissrussland)", CLDRLocale.describe(be, swiss));
		assertEquals("Weissrussisch (Weissrussland)", CLDRLocale.describe(be_BY, swiss));
	}
	
	@Test
	@Ignore
	public void testSelfDescription() throws Exception {
		/* This is not really a test, but more a verification. It includes all locales
		 * that we currently have a translation team for on Transifex. (Hasn't been updated in a while)
		 */
		System.out.println(CLDRLocale.getProperties(new Locale("ru","RU")));
		System.out.println(CLDRLocale.getProperties(new Locale("cs")));
		System.out.println(CLDRLocale.getProperties(new Locale("hu","")));
		System.out.println(CLDRLocale.getProperties(new Locale("de","")));
		System.out.println(CLDRLocale.getProperties(new Locale("eu","")));
		System.out.println(CLDRLocale.getProperties(new Locale("hr","HR")));
		System.out.println(CLDRLocale.getProperties(new Locale("it","")));
		System.out.println(CLDRLocale.getProperties(new Locale("es","")));
		System.out.println(CLDRLocale.getProperties(new Locale("uk","UA")));
	}
	
	@Test
	public void testOutput() throws Exception {
		Properties props = CLDRLocale.getProperties(new Locale("ru","RU"));
		props.store(new FileOutputStream("/tmp/ru_RU.properties"), null);
	}
	
	@Test
	public void testThings() throws Exception {
		URL url = CLDRLocale.class.getResource(CLDRLocale.class.getSimpleName()+".class");
		File dir = new File(url.toURI()).getParentFile();
		List<String> basics = new ArrayList<String>();
		for (File xml: dir.listFiles()) {
			if ((!xml.getName().endsWith(".xml")) || xml.getName().contains("_")) {
				continue;
			}
			String basic = xml.getName().replace(".xml", "");
			basics.add(basic);
		}
		Collections.sort(basics);
		int warnings = 0;
		for (String basic: basics) {
			Locale lang = new Locale(basic);
			if (!basic.equals(lang.toString())) {
				System.err.println("WEIRD: language code "+basic+" produces locale "+lang);
				continue;
			}
			Locale full = CLDRLocale.getLocaleWithTerritory(lang);
			if (basic.equals("pl")) {
				assertEquals(new Locale("pl","PL"), full);
			} else if (basic.equals("de")) {
				assertEquals(new Locale("de","DE"), full);
			} else if (basic.equals("ru")) {
				assertEquals(new Locale("ru","RU"), full);
			} else if (basic.equals("cs")) {
				assertEquals(new Locale("cs","CZ"), full);
			} else if (basic.equals("ca")) {
				assertEquals(new Locale("ca","ES"), full);
			} else if (basic.equals("it")) {
				assertEquals(new Locale("it","IT"), full);
			}
			if (full == null) {
				//System.err.println("WARNING: " + basic);
				++warnings;
			}
		}
		if (warnings != 0) {
			System.err.println("There were "+warnings+" warnings.");
		}
	}
}
