package poc;

import java.text.DateFormat;
import java.text.Format;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.TimeZone;

import org.junit.Test;

import com.amazon.ebook.util.text.DateUtils;


public class MessageFormatTests {
	@Test
	public void testname() throws Exception {
		Locale.setDefault(Locale.ENGLISH);
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		String fmt = "{0}: {1,date,EEE}, {1,date,medium}";
		MessageFormat msg = new MessageFormat(fmt);
		System.err.println(msg.toPattern());
		msg = DateUtils.floatMessageFormat(msg);
		System.err.println(msg.toPattern());
		msg = new MessageFormat(fmt);
		msg = floatMessageFormat(msg);
		System.err.println(msg.toPattern());
	}
	
	MessageFormat floatMessageFormat(MessageFormat msg) {
		Format[] formats = msg.getFormats();
		for (int i=0; i < formats.length; ++i) {
			if (formats[i] instanceof DateFormat) {
				((DateFormat)formats[i]).setTimeZone(TimeZone.getTimeZone("UTC"));
				msg.setFormat(i, formats[i]);
			}
		}
		return msg;
	}
}
