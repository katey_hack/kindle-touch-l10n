package poc;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.zip.GZIPOutputStream;

import org.junit.Test;

import serp.bytecode.BCClass;
import serp.bytecode.BCMethod;
import serp.bytecode.Project;
import serp.bytecode.lowlevel.ConstantPool;
import serp.bytecode.lowlevel.Entry;
import serp.bytecode.lowlevel.StringEntry;
import serp.bytecode.lowlevel.UTF8Entry;

import com.amazon.ebook.util.text.Base64;

/**
 * This is only used for proof-of-concept testing (i.e. as a quick way to learn how to do
 * things. Don't worry if these tests fail on your machine. As long as they work
 * on mine, we're ok.
 * 
 * @author ixtab
 * 
 */
public class POCTests {

	private static enum Compression {
		NONE, GZIP
	};

	@Test
	public void testPropertiesCanContainArray() throws Exception {
		Properties p = new Properties();
		p.load(this.getClass().getResourceAsStream(
				"resources/withArray.properties"));
		assertEquals("works", p.get("simple"));
		assertEquals("works[0]", p.get("one.dimensional[0]"));
		assertEquals("works[1]", p.get("one.dimensional[1]"));
		assertEquals("works[0][0]", p.get("two.dimensional[0][0]"));
		assertEquals("works[0][1]", p.get("two.dimensional[0][1]"));
		assertEquals("works[1][0]", p.get("two.dimensional[1][0]"));
	}

	@Test
	public void testTemplateIsWorking() throws Exception {
		ResourceBundle bundle = ResourceBundle
				.getBundle("poc.resources.TemplateResources");
		assertEquals("two", bundle.getString("forty"));
	}

	@Test
	public void learnSerp() throws Exception {
		ResourceBundle bundle = ResourceBundle
				.getBundle("poc.resources.TemplateResources");
		assertEquals("two", bundle.getString("forty"));

		String fortytwo = "rO0ABXVyABRbW0xqYXZhLmxhbmcuT2JqZWN0Oxi/+1Pka9vKAgAAeHAAAAABdXIAE1tMamF2YS5sYW5nLk9iamVjdDuQzlifEHMpbAIAAHhwAAAAAnQABWZvcnR5dAADdHdv";
//                         H4sIAAAAAAAAAFvzloG1tIhBJDraJyuxLFEvJzEvXc8/KSs1ucRaYv/v4CfZt08xMTBUFDAwMDACFQpjUTfhXMR8gWLNHJg6phIG1rT8opLKEgbmkvJ8ANdAvlljAAAA
		Project proj = new Project();
		BCClass clazz = proj.loadClass(bundle.getClass());

		BCMethod m = clazz.getDeclaredMethod("getContents");
		assertNotNull(m);

		ConstantPool pool = clazz.getPool();

		Entry[] entries = pool.getEntries();
		assertNotNull(entries);
		for (Entry entry : entries) {
			if (entry instanceof UTF8Entry) {
				UTF8Entry e = (UTF8Entry) entry;
				if (fortytwo.equals(e.getValue())) {
					e.setValue("HACKED :-)");
				}
			} else if (entry instanceof StringEntry) {
				StringEntry e = (StringEntry) entry;
				System.out.println(e.getIndex()+" => " + e.getStringIndex());
			}
		}
		clazz.setName("ixtab.patched.TemplateResources_sq_AF");
		clazz.write(new File("/tmp/out.class"));
	}

	@Test
	public void testSerializedObject() throws Exception {
		Object[][] multi = new Object[][] { new Object[] { "forty", "two" }, };

		String serialized = toBase64(multi, Compression.GZIP);
		System.out.println(serialized);
		ByteArrayInputStream bis = new ByteArrayInputStream(
				Base64.decode(serialized));
		ObjectInputStream ois = new ObjectInputStream(bis);
		Object[][] clone = (Object[][]) ois.readObject();
		assertArrayEquals(multi, clone);
	}

	private String toBase64(Object[][] contents, Compression comp)
			throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		OutputStream zos = bos;
		if (comp == Compression.GZIP) {
			zos = new GZIPOutputStream(bos);
		}
		ObjectOutputStream oos = new ObjectOutputStream(zos);
		oos.writeObject(contents);
		oos.close();
		String serialized = Base64.encodeBytes(bos.toByteArray());
		return serialized;
	}

	@Test
	public void testRatherLargeFile() throws Exception {
		String file = System.getProperty("user.home");
		file += "/kindle-touch/localization/kindle-touch-l10n/";
		file += "src/5.0.0/framework/com/amazon/ebook/booklet/reader/sdk/exception/resources/ReaderExceptionResources_de.properties";
		Properties props = new Properties();
		props.load(new FileInputStream(file));
		List<Object[]> objs = new ArrayList<Object[]>();
		for (Map.Entry<Object, Object> entry : props.entrySet()) {
			objs.add(new Object[] { entry.getKey(), entry.getValue() });
		}
		Object[][] contents = new Object[objs.size()][];
		for (int i = 0; i < contents.length; ++i) {
			contents[i] = objs.get(i);
		}

		String unzipped = toBase64(contents, Compression.NONE);
		System.out.println("unzipped: " + unzipped.length());

		String gzipped = toBase64(contents, Compression.GZIP);
		System.out.println("gzipped: " + gzipped.length());
		
	}
	
	@Test
	public void testUTF8Properties() throws Exception {
		String utf8 = "/tmp/ixtab.utf-8.properties";
		String iso = "/tmp/ixtab.iso-8859-1.properties";
		Properties props = new Properties();
		String four = "four";
		String five = "five";
		String four_l = "четыpе";
		String five_l = "fünf";
		props.put(four, four_l);
		props.put(five, five_l);
		
		FileWriter fw = new FileWriter(utf8);
		props.store(fw, null);
		fw.close();
		
		FileReader fr = new FileReader(utf8);
		props.load(fr);
		fr.close();
		
		assertEquals(four_l, props.get(four));
		assertEquals(five_l, props.get(five));
		
		OutputStream os = new FileOutputStream(iso);
		props.store(os, null);
		os.close();
		
		InputStream is = new FileInputStream(iso);
		props.load(is);
		is.close();
		
		assertEquals(four_l, props.get(four));
		assertEquals(five_l, props.get(five));
		
	}
}
